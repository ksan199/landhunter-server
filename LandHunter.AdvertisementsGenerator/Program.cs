﻿using System;
using Autofac;
using LandHunter.DataAccess.Context;
using LandHunter.DataAccess.Repository;
using LandHunter.Services;
using SharpEngine.Repository;
using LandHunter.Common.Helpers;
using System.Xml;
using System.Web;
using System.IO;
using System.Text;

namespace LandHunter.AdvertisementsGenerator
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            RegisterDependencies();
            
            try
            {
                var webSitePath = AppConfigHelper.GetSetting<string>("WebSitePath");
                var webSitePhysicalPath = AppConfigHelper.GetSetting<string>("WebSitePhysicalPath");

                var irrUserId = AppConfigHelper.GetSetting<string>("IrrUserId");
                var savePath = args.Length > 0 ? args[0] : "";

                FixMd5Hash(webSitePhysicalPath);

                GenerateAvitoAdvertisements(webSitePath, savePath);
                GenerateCianAdvertisements(webSitePath, savePath);
                GenerateIrrAdvertisements(webSitePath, savePath, irrUserId);

                Console.WriteLine("Advertisements was successfully generated.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while generating Advertisements: " + e.Message + " - " + e.StackTrace);
            }
#if DEBUG
            Console.WriteLine("Press any key to close console.");
            Console.ReadKey();
#endif
        }

        private static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<LandHunterContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(LandHunterRepository<,>)).As(typeof(IRepository<,>)).InstancePerLifetimeScope();
            builder.RegisterType<OrdersService>().As<IOrdersService>().InstancePerLifetimeScope();
            builder.RegisterType<FilesService>().As<IFilesService>().InstancePerLifetimeScope();
            
            Container = builder.Build();
        }

        private static string GenerateAvitoAdvertisements(string webSitePath, string savePath)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var ordersService = scope.Resolve<IOrdersService>();

                var xmlString = ordersService.GenerateAvitoAdvertisements(webSitePath);

                var doc = new XmlDocument();

                doc.LoadXml(xmlString);

                doc.Save(string.Format("{0}{1}.xml", savePath, "avito"));

                return xmlString;
            }
        }

        private static void FixMd5Hash(string webSitePhysicalPath)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var filesService = scope.Resolve<IFilesService>();

                filesService.FixMd5(webSitePhysicalPath);
            }
        }

        private static string GenerateCianAdvertisements(string webSitePath, string savePath)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var ordersService = scope.Resolve<IOrdersService>();
                
                var xmlString = ordersService.GenerateCianAdvertisements(webSitePath);
                var xmlSavePath = string.Format("{0}{1}.xml", savePath, "cian");

                var doc = new XmlDocument();

                doc.LoadXml(HttpUtility.HtmlDecode(xmlString));

                using (TextWriter sw = new StreamWriter(xmlSavePath, false, Encoding.GetEncoding("windows-1251")))
                {
                    doc.Save(sw);  
                }

                return xmlString;
            }
        }

        private static string GenerateIrrAdvertisements(string webSitePath, string savePath, string userId)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var ordersService = scope.Resolve<IOrdersService>();

                var xmlString = ordersService.GenerateIrrAdvertisements(webSitePath, userId);
                var xmlSavePath = string.Format("{0}{1}.xml", savePath, "irr");

                var doc = new XmlDocument();

                doc.LoadXml(HttpUtility.HtmlDecode(xmlString));

                using (TextWriter sw = new StreamWriter(xmlSavePath, false, Encoding.UTF8))
                {
                    doc.Save(sw);
                }

                return xmlString;
            }
        }
    }
}
