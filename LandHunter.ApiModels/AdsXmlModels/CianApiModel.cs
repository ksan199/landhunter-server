﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace LandHunter.ApiModels
{
    public sealed class StringWriterWindows1251 : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.GetEncoding("windows-1251"); }
        }
    }

    [XmlRoot(ElementName="suburbian", Namespace="")]
    public class CianRootModel
    {
        [XmlElement(ElementName = "offer")]
        public CianOfferModel[] Offers;

        public CianRootModel()
        { 
        }

        public CianRootModel(CianOfferModel[] offers)
        {
            this.Offers = offers;
        }

        public string ToXmlString()
        {
            //string xmlVersion = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>";
            XmlSerializer serializer = new XmlSerializer(this.GetType());

            StringWriterWindows1251 stringWriter = new StringWriterWindows1251();
            
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            serializer.Serialize(stringWriter, this, ns);

            return stringWriter.ToString();
        }
    }

    public class CianArea
    {
        [XmlAttribute(AttributeName="region")]
        public decimal Region {get;set;}
    }

    public class CianPrice
    {
        [XmlAttribute("currency")]
        public string Currency { get; set; }

        [XmlAttribute("for_day")]
        public string RentLength { get; set; }

        [XmlText]
        public decimal Price {get;set;}

        public CianPrice()
        {
            this.RentLength = "0";

            this.Currency = "RUB"; //todo USD, RUB, EUR
        }
    }

    public class CianCommision
    {
        [XmlAttribute(AttributeName = "agent")]
        public int Agent { get; set; }
        [XmlAttribute(AttributeName = "client")]
        public int Client { get; set; }

        public CianCommision()
        {
            this.Agent = 0;
            this.Client = 0;
        }
    }

    public class CianAddress
    {
        [XmlAttribute(AttributeName="admin_area")]
        public int AdminArea { get; set; }
        [XmlAttribute(AttributeName = "locality")]
        public string Locality { get; set; }
        [XmlAttribute(AttributeName = "street")]
        public string Street { get; set; }
        [XmlAttribute(AttributeName = "house_str")]
        public string House { get; set; }
        [XmlAttribute(AttributeName = "mcad")]
        public int MCAD { get; set; }
        [XmlAttribute(AttributeName = "route")]
        public int Route { get; set; }
    }
    
    [XmlRoot(ElementName="offer")]
    public class CianOfferModel
    {
        [XmlElement(ElementName="id")]
        public int Id { get; set; }
        [XmlElement(ElementName = "realty_type")]
        public string RealtyType { get; set; }

        [XmlElement(ElementName = "deal_type")]        
        public string DealType { get; set; }
        
        [XmlElement(ElementName="area")]
        public CianArea Area { get; set; }

        [XmlElement(ElementName="price")]
        public CianPrice Price { get; set; }

        [XmlElement(ElementName="land_type")]
        public int LandType { get; set; }

        [XmlElement(ElementName="com")]
        public CianCommision Commision {get; set;}

        [XmlElement(ElementName="address")]
        public CianAddress Address { get; set; }

        [XmlElement(ElementName = "note")]
        public string Note { get; set; }

        [XmlElement(ElementName="photo")]
        public string[] Images { get; set; }

        [XmlElement(ElementName = "phone")]
        public string Phone { get; set; }

        [XmlElement(ElementName="premium")]
        public int Premium { get; set; }

        public CianOfferModel()
        {
            this.Area = new CianArea();
            this.Price = new CianPrice();

            this.Commision = new CianCommision();

            this.Address = new CianAddress();
        }

        public void SetArea(decimal area)
        {
            decimal squareMetesToSotka = 0.01m;

            this.Area.Region = Math.Round(area * squareMetesToSotka, 0);
        }

        public void SetPrice(decimal price)
        {
            this.Price.Price = Math.Round(price, 0);
        }

        public void SetAddress(int areaId, string locality, string street, int mcad = 0, int routeId = 280)
        {
            this.Address.AdminArea = areaId;
            this.Address.Locality = System.Web.HttpUtility.HtmlEncode(locality);
            this.Address.Street = street;

            this.Address.MCAD = mcad;
            this.Address.Route = routeId;
        }
        
    }
}
