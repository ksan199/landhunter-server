﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace LandHunter.ApiModels
{

    public sealed class StringWriterUtf8: StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
    [XmlRoot(ElementName="users")]
    public class IrrRootModel
    {
        [XmlElement(ElementName = "user")]
        public IrrUser[] Users { get; set; }


        public string ToXmlString()
        {
            //string xmlVersion = "<?xml version=\"1.0\" encoding=\"windows-1251\"?>";
            XmlSerializer serializer = new XmlSerializer(this.GetType());

            StringWriterUtf8 stringWriter = new StringWriterUtf8();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            serializer.Serialize(stringWriter, this, ns);

            return stringWriter.ToString();
        }
    }

    public class IrrUserCredentials
    {
        [XmlElement(ElementName="user-id")]
        public string Id { get; set; }
    }

    public class IrrPrice
    {
        [XmlAttribute(AttributeName="currency")]
        public string Currency { get; set; }

        [XmlAttribute(AttributeName = "value")]
        public decimal Value{ get; set; }
    }

    public class IrrPhoto
    {
        [XmlAttribute(AttributeName="url")]
        public string Url { get; set; }


        [XmlAttribute(AttributeName = "md5")]
        public string Md5 { get; set; }

        public IrrPhoto()
        {
        }

        public IrrPhoto(string url)
        {
            this.Url = url;
        }

        public IrrPhoto(string url, string md5Hash)
        {
            this.Url = url;
            this.Md5 = md5Hash;
        }
    }

    public class IrrCustomField
    {
        [XmlAttribute(AttributeName="name")]
        public string Name { get; set; }
        [XmlText]
        public string FieldValue { get; set; }

        public IrrCustomField()
        {
        }

        public IrrCustomField(string name, string fieldValue)
        {
            this.Name = name;
            this.FieldValue = fieldValue;
        }
    }

    public class IrrOffer
    {
        private DateTime _dateBegin;        
        [XmlAttribute(AttributeName="validfrom")]
        public string DateBegin
        {
            get { return _dateBegin.ToString("yyyy-MM-ddThh:mm:ss"); }
            set { _dateBegin = DateTime.Parse(value); }
        }


        private DateTime _dateEnd;
        [XmlAttribute(AttributeName = "validtill")]
        public string DateEnd
        {
            get { return _dateEnd.ToString("yyyy-MM-ddThh:mm:ss"); }
            set { _dateEnd = DateTime.Parse(value); }
        }

        [XmlAttribute(AttributeName="power-ad")]
        public int PowerAd
        {
            get { return 1; }
            set { }

        }

        [XmlAttribute(AttributeName="source-id")]
        public int Id { get; set; }

        [XmlAttribute(AttributeName = "category")]
        public string Category
        {
            get { return "/real-estate/out-of-town/lands"; }
            set { }
        }
        [XmlAttribute(AttributeName = "adverttype")]
        public string AdverType
        {
            get { return "realty_new"; }
            set { }
        }

        [XmlElement(ElementName="price")]
        public IrrPrice Price { get; set; }

        [XmlElement(ElementName="title")]
        public string Title { get; set; }
        
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        [XmlArray("fotos")]
        [XmlArrayItem("foto-remote")]
        public IrrPhoto[] Photos { get; set; }

        [XmlArray("custom-fields")]
        [XmlArrayItem("field")]
        public IrrCustomField[] CustomFields { get; set; }

        public IrrOffer()
        {
            this.Price = new IrrPrice();

        }

        public void SetPrice(decimal price, string currency = "RUR")
        {
            this.Price.Value = Math.Round(price, 0);
            this.Price.Currency = currency;
        }
    }

    public class IrrUser
    {
        [XmlAttribute(AttributeName="deactivate-untouched")]
        public bool Deactivate { get; set; }

        public IrrUser()
        {
            this.Deactivate = false;

            this.Credentials = new IrrUserCredentials();
        }
        [XmlElement(ElementName="match")]
        public IrrUserCredentials Credentials; 

        [XmlElement(ElementName = "store-ad")]
        public IrrOffer[] Offers;

    }
}
