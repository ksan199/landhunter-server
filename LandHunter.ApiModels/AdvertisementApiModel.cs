﻿using System;
using System.Collections.Generic;
using LandHunter.Common.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LandHunter.ApiModels
{
    public class AdvertisementApiModel : LandBaseData
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public LandStatus? LandStatus { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public AdvertisementState? State { get; set; }

        public int? Id { get; set; }

        public bool? IsOnTop { get; set; }

        public RealtorApiModel Realtor { get; set; }

        public IEnumerable<FileApiModel> Documents { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}