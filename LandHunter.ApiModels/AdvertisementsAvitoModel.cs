﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LandHunter.ApiModels
{
    [XmlRoot(ElementName="Ads")]
    public class AdvertisementsAvitoModel
    {
        [XmlAttribute(AttributeName = "target")]
        public string Target = "Avito.ru";
        [XmlAttribute(AttributeName="formatVersion")]
        public string FormatVersion = "2";
        
        [XmlElement(ElementName="Ad")]
        public AdvertisementAvitoModel[] Ads { get; set; }

        public AdvertisementsAvitoModel()
        {
            
        }

        public AdvertisementsAvitoModel(AdvertisementAvitoModel[] ads)
        {
            this.Ads = ads;
        }

        public string ToXmlString()
        {
            //string xmlVersion = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            XmlSerializer serializer = new XmlSerializer(this.GetType());
            StringWriter stringWriter = new StringWriter();

            serializer.Serialize(stringWriter, this);

            return stringWriter.ToString();
        }
    }

    public class AdvertisementAvitoBaseModel
    {

        public string Id { get; set; }

        public string Category { get; set; }

        public string Region { get; set; }

        public string City { get; set; }

        public string ContactPhone { get; set; }
        /*required end*/

        private DateTime _dateBegin;
        public string DateBegin
        {
            get { return _dateBegin.ToString("yyyy-MM-dd"); }
            set { _dateBegin = DateTime.Parse(value); }
        }


        private DateTime _dateEnd;
        public string DateEnd
        {
            get { return _dateEnd.ToString("yyyy-MM-dd"); }
            set { _dateEnd = DateTime.Parse(value); }
        }

        public string Subway { get; set; }

        public string District { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public string CompanyName { get; set; }

        public string ManagerName { get; set; }

        public string EMail { get; set; }

        public string AdStatus { get; set; }

    }

    public class AdvertisementAvitoModel : AdvertisementAvitoBaseModel
    {
        public AdvertisementAvitoModel()
        {
            
        }

        public string OperationType { get; set; }

        public decimal LandArea { get; set; }

        public string ObjectType { get; set; }

        public int DistanceToCity { get; set; }
                
        [XmlArrayItem(ElementName="Image")]
        public ImageAvitoModel[] Images { get; set; }
    }

    [XmlRoot(ElementName="Images")]
    public class ImageAvitoModel
    {
        public ImageAvitoModel()
        {
        }
        public ImageAvitoModel(string url)
        {
            this.Url = url;
        }

        [XmlAttribute(AttributeName="url")]
        public string Url { get; set; }
    }
}
