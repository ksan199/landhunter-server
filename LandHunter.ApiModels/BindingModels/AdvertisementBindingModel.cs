﻿using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ApiModels
{
    public class AdvertisementBindingModel
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string CadNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public int CurrencyId { get; set; }

        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string Address { get; set; }

        public string Description { get; set; }

        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string Utilization { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string CategoryCode { get; set; }

        public decimal? Area { get; set; }

        public bool GasPipeline { get; set; }

        public bool WaterPipeline { get; set; }

        public bool PowerLine { get; set; }

        public bool Sand { get; set; }

        public bool ShowCreateDate { get; set; }

        public int[] DocumentsIds { get; set; }

        public int[] ImagesIds { get; set; }
    }
}