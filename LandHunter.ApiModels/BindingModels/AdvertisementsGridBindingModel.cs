﻿using LandHunter.Common.Enums;

namespace LandHunter.ApiModels
{
    public class AdvertisementsGridBindingModel
    {
        public string RealtorId { get; set; }

        public AdvertisementState State { get; set; }

        public GridSettingsBindingModel GridSettings { get; set; }
    }
}