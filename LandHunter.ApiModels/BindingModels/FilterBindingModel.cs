﻿namespace LandHunter.ApiModels
{
    public class FilterBindingModel
    {
        public string Key { get; set; }
        public string[] Values { get; set; }
    }
}