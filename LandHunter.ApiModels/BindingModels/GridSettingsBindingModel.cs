﻿using System.Collections.Generic;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.ApiModels
{
    public class GridSettingsBindingModel
    {
        public GridSortOptions Sorting { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public IEnumerable<FilterBindingModel> Filters { get; set; }
    }
}