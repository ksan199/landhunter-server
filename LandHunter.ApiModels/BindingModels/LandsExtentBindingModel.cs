﻿using System.Collections.Generic;

namespace LandHunter.ApiModels
{
    public class LandsExtentBindingModel
    {
        public string XMin { get; set; }
        public string YMin { get; set; }
        public string XMax { get; set; }
        public string YMax { get; set; }
        public int Resolution { get; set; }

        public int? CurrencyId { get; set; }
        public IEnumerable<FilterBindingModel> Filters { get; set; }
    }
}