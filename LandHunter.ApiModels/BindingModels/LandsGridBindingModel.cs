﻿using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ApiModels
{
    public class LandsGridBindingModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public string[] CadNums { get; set; }

        public bool GetOnlyCadNums { get; set; }

        public GridSettingsBindingModel GridSettings { get; set; }
    }
}