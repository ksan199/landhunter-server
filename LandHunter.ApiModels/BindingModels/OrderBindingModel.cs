﻿using System;
using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ApiModels
{
    public class OrderBindingModel
    {
        
        public int OrderType { get; set; }

        public decimal OrderSumAmount { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string CadNum { get; set; }

        public string PaymentType { get; set; }

        public DateTime? AdStartDate { get; set; }

        public DateTime? AdEndDate { get; set; }

    }
}
