﻿using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ApiModels
{
    public class RealtorBindingModel
    {
        public string Surname { get; set; }

        //[Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public string Name { get; set; }

        public string Patronymic { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Website { get; set; }

        public int? ImageId { get; set; }

        public int? AgencyId { get; set; }
    }
}