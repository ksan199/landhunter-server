﻿using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ApiModels
{
    public class ChangePasswordBindingModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public string NewPassword { get; set; }
    }
}