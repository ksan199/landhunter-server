﻿using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Resources;
using SharpEngine.Web.Mvc.Validation;

namespace LandHunter.ApiModels
{
    public class ResetPasswordBindingModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [RegularExpression(ValidationRegex.Phone, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PhoneIncorrect")]
        public string PhoneNumber { get; set; }
    }
}