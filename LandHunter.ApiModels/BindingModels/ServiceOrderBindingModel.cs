﻿using System.ComponentModel.DataAnnotations;
using LandHunter.Common;
using LandHunter.Common.Enums;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ApiModels
{
    public class ServiceOrderBindingModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public int ServiceId { get; set; }

        public ServiceOrderUserData UserData { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        public ReceptionType ReceptionType { get; set; }
    }
}