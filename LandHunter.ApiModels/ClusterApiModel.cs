﻿namespace LandHunter.ApiModels
{
    public class ClusterApiModel
    {
        public double X { get; set; }
        public double Y { get; set; }
        public int Size { get; set; }
    }
}