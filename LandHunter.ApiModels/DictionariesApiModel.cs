﻿using System.Collections.Generic;
using LandHunter.Models;

namespace LandHunter.ApiModels
{
    public class DictionariesApiModel
    {
        public IEnumerable<Region> Regions { get; set; }
        public IEnumerable<LandCategory> LandCategories { get; set; }
        public IEnumerable<AdvertisementStatus> AdvertisementStatuses { get; set; }
        public IEnumerable<Currency> Currencies { get; set; }
    }
}