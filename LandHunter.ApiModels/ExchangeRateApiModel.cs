﻿using LandHunter.Common.Helpers;
using LandHunter.Common.JsonConverters;
using Newtonsoft.Json;

namespace LandHunter.ApiModels
{
    public class ExchangeRateApiModel : IExchangeRate
    {
        [JsonConverter(typeof(ToStringConverter))]
        public int From { get; set; }

        [JsonConverter(typeof(ToStringConverter))]
        public int To { get; set; }

        public decimal Value { get; set; }
    }
}