﻿namespace LandHunter.ApiModels
{
    public class FileApiModel
    {
        public int? Id { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string ThumbnailPath { get; set; }
    }
}
