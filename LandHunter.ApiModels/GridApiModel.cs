﻿using System.Collections.Generic;

namespace LandHunter.ApiModels
{
    public class GridApiModel<TApiModel>
        where TApiModel: class
    {
        public IReadOnlyCollection<TApiModel> List { get; set; }

        public int? TotalItems { get; set; }
        public int? TotalPages { get; set; }
        public bool? HasPreviousPage { get; set; }
        public bool? HasNextPage { get; set; }
    }
}