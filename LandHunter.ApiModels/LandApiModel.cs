﻿using System.Collections.Generic;

namespace LandHunter.ApiModels
{
    public class LandApiModel : LandBaseData
    {
        public IEnumerable<AdvertisementApiModel> Advertisements { get; set; }
    }
}