﻿using System.Collections.Generic;
using LandHunter.Common.JsonConverters;
using Newtonsoft.Json;

namespace LandHunter.ApiModels
{
    public class LandBaseData
    {
        public string CadNum { get; set; }

        public int RegionId { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public string Utilization { get; set; }

        public string CategoryCode { get; set; }

        public bool? GasPipeline { get; set; }

        public bool? WaterPipeline { get; set; }

        public bool? ShowCreateDate { get; set; }

        public bool? PowerLine { get; set; }

        public bool? Sand { get; set; }

        public decimal? Area { get; set; }

        public decimal? CadastrePrice { get; set; }

        public decimal? Price { get; set; }

        [JsonConverter(typeof(ToStringConverter))]
        public int? CurrencyId { get; set; }

        public IEnumerable<FileApiModel> Images { get; set; }

        public int? ViewsCount { get; set; }
    }
}
