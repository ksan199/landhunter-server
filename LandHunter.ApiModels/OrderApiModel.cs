﻿using System;
using System.Collections.Generic;

namespace LandHunter.ApiModels
{
    public class OrderApiModel
    {
        public int Id { get; set; }

        public int OrderTypeId { get; set; }

        public string OrderType { get; set; }

        public Dictionary<int, string> OrderTypes { get; set; }
        
        public string CadNum { get; set; }

        public decimal OrderSumAmount { get; set; }

        public string PaymentType { get; set; }

        public decimal BalanceBefore { get; set; }

        public DateTime UpdateDate { get; set; }

        public string Message { get; set; }

        public bool IsCreated { get; set; }
    }
}
