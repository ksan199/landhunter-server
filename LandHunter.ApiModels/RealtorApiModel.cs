﻿using System.Collections.Generic;
using LandHunter.Common.Enums;

namespace LandHunter.ApiModels
{
    public class RealtorApiModel
    {
        public string Id { get; set; }

        public string Surname { get; set; }

        public string Name { get; set; }

        public string Patronymic { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Website { get; set; }

        public AgencyApiModel Agency { get; set; }

        public FileApiModel Image { get; set; }

        public IDictionary<AdvertisementState, int> AdvertisementsCount { get; set; }

        public int? ViewsCount { get; set; }

        public int? Rating { get; set; }

        public decimal? Balance { get; set; }

        public IReadOnlyCollection<int> RegionsIds { get; set; }

        public IReadOnlyCollection<int> OwnedAgenciesIds { get; set; }
    }
}