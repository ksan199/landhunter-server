﻿namespace LandHunter.ApiModels
{
    public class RealtorsGridApiModel : GridApiModel<RealtorApiModel>
    {
        public int? AllAdvertisementsCount { get; set; }
    }
}