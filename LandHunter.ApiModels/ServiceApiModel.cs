﻿namespace LandHunter.ApiModels
{
    public class ServiceApiModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal? Price { get; set; }
    }
}
