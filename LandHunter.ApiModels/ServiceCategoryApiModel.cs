﻿using System.Collections.Generic;

namespace LandHunter.ApiModels
{
    public class ServiceCategoryApiModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IReadOnlyCollection<ServiceApiModel> Services { get; set; }
    }
}
