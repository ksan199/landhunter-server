﻿namespace LandHunter.ApiModels
{
    public class SitePageApiModel
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Content { get; set; }
    }
}