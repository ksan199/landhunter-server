﻿using System;

namespace LandHunter.ApiModels
{
    public class YaKassaRequestApiModel
    {
        public DateTime RequestDateTime { get; set; }
        //Тип запроса. Значение: «checkOrder» или "paymentAviso"
        public string Action { get; set; }
        //MD5-хэш параметров платежной формы
        public string Md5 { get; set; }
        //Идентификатор Контрагента
        public long ShopId { get; set; }
        //Идентификатор товара
        public long ShopArticleId { get; set; }
        //Уникальный номер транзакции в ИС Оператора.
        public long InvoiceId { get; set; }
        //Номер заказа в ИС Контрагента
        public string OrderNumber { get; set; }
        //Идентификатор плательщика (присланный в платежной форме) на стороне Контрагента: номер договора, мобильного телефона и т.п
        public string CustomerNumber { get; set; }
        //Момент регистрации заказа в ИС Оператора
        public DateTime OrderCreatedDatetime { get; set; }
        //Стоимость заказа. Может отличаться от суммы платежа, если пользователь платил в валюте, которая отличается от указанной в платежной форме. В этом случае Оператор берет на себя все конвертации.
        public decimal OrderSumAmount { get; set; }
        //Код валюты для суммы заказа
        public long OrderSumCurrencyPaycash { get; set; }
        //Код процессингового центра Оператора для суммы заказа.
        public long OrderSumBankPaycash { get; set; }
        //Сумма к выплате Контрагенту на р/с (стоимость заказа минус комиссия Оператора).
        public decimal ShopSumAmount { get; set; }
        //Код валюты для shopSumAmount.
        public long ShopSumCurrencyPaycash { get; set; }
        //Код процессингового центра Оператора для shopSumAmount.
        public long ShopSumBankPaycash { get; set; }
        //Номер счета в ИС Оператора, с которого производится оплата.
        public long PaymentPayerCode { get; set; }
        //Способ оплаты заказа.
        public string PaymentType { get; set; }
    }
}
