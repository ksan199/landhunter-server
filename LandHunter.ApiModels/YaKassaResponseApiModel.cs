﻿using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LandHunter.ApiModels
{    
    [XmlRoot(Namespace = "",
     ElementName = "__actionType__Response"
     )]
    public class YaKassaResponseApiModel
    {
        public YaKassaResponseApiModel(){
            this.PerformedDatetime = DateTime.Now;
        }

        public YaKassaResponseApiModel(YaKassaRequestApiModel request)
        {
            this.PerformedDatetime = DateTime.Now;

            this.Action = request.Action;

            this.ShopId = request.ShopId;
            this.InvoiceId = request.InvoiceId;
            this.OrderSumAmount = request.OrderSumAmount;
        }

        [XmlIgnore]
        public string Action { get; set; }

        [XmlAttribute(AttributeName = "performedDatetime")]
        public DateTime PerformedDatetime { get; set; }
        [XmlAttribute(AttributeName = "code")]
        public int Code { get; set; }
        [XmlAttribute(AttributeName = "shopId")]
        public long ShopId { get; set; }
        [XmlAttribute(AttributeName = "invoiceId")]
        public long InvoiceId { get; set; }
        [XmlIgnore]
        public decimal OrderSumAmount { get; set; }
        [XmlAttribute(AttributeName = "message")]
        public string Message { get; set; }
        [XmlAttribute(AttributeName = "techMessage")]
        public string TechMessage { get; set; }


        public string ToXmlString()
        {
            string result = string.Format("<?xml version=\"1.0\" encoding=\"UTF-8\"?> <{0}Response performedDatetime=\"{1}\" code=\"{2}\" invoiceId=\"{3}\" shopId=\"{4}\"/>",
                this.Action,
                DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss.fzzzz"),
                this.Code,
                this.InvoiceId,
                this.ShopId);

            return result;
        }
    }
}
