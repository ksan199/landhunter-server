﻿using System.Threading.Tasks;
using Microsoft.Owin;

namespace LandHunter.AspNetIdentity
{
    public class ApiAuthenticationMiddleware : OwinMiddleware
    {
        public ApiAuthenticationMiddleware(OwinMiddleware next)
            : base(next)
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            await Next.Invoke(context);

            if (context.Response.StatusCode == 400 && context.Response.Headers.ContainsKey("AuthorizationResponse"))
            {
                context.Response.Headers.Remove("AuthorizationResponse");
                context.Response.StatusCode = 200;
            }
        }
    }
}