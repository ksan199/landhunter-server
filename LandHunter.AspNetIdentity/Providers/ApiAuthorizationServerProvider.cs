﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using LandHunter.Common.Helpers;
using Microsoft.Owin.Security.OAuth;
using SharpEngine.Web.Mvc.Autofac;

namespace LandHunter.AspNetIdentity
{
    public class ApiAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            string userId;
            string userName;
            string userRole;

            if (string.Equals(context.UserName, "annonymous", StringComparison.InvariantCultureIgnoreCase)
                && string.Equals(context.Password, "annonymous", StringComparison.InvariantCultureIgnoreCase))
            {
                userId = Guid.NewGuid().ToString();
                userName = "Annonymous";
                userRole = "Annonymous";
            }
            else
            {
                var userManager = AutofacLifetimeScope.GetInstance<ApplicationUserManager>();

                var realtorUserName = RealtorsHelper.GetUserNameFromPhone(context.UserName);

                var user = await userManager.FindAsync(realtorUserName, context.Password);

                if (user == null || !(await userManager.IsInRoleAsync(user.Id, "Realtor")))
                {
                    context.SetError("invalid_grant", "Неправильный логин или пароль.");
                    context.Response.Headers.Add("AuthorizationResponse", new[] { "Failed" });

                    return;
                }

                if (await userManager.GetLockoutEnabledAsync(user.Id))
                {
                    context.SetError("invalid_grant", "Ваш аккаунт заблокирован, обратитесь к администратору.");
                    context.Response.Headers.Add("AuthorizationResponse", new[] { "Failed" });

                    return;
                }

                userId = user.Id;
                userName = user.UserName;
                userRole = "Realtor";
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", userId));
            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", userName));
            identity.AddClaim(new Claim("http://schemas.microsoft.com/ws/2008/06/identity/claims/role", userRole));

            context.Validated(identity);
        }
    }
}