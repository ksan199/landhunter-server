﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SmsRu;

namespace LandHunter.AspNetIdentity.Services
{
    public class SmsService : IIdentityMessageService
    {
        private readonly ISmsProvider _smsProvider;

        public SmsService(ISmsProvider smsProvider)
        {
            _smsProvider = smsProvider;
        }

        public Task SendAsync(IdentityMessage message)
        {
            _smsProvider.Send(message.Destination, message.Body);

            return Task.FromResult(0);
        }
    }
}