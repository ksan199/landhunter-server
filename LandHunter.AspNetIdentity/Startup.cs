﻿using System;
using System.Web;
using System.Web.Configuration;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartupAttribute(typeof(LandHunter.AspNetIdentity.Startup))]
namespace LandHunter.AspNetIdentity
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var loginPathSettings = WebConfigurationManager.AppSettings["LoginPath"];

            app.UseCookieAuthentication(new CookieAuthenticationOptions
                {
                    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                    LoginPath = new PathString(loginPathSettings ?? "/Admin/Security/SignIn"),
                    Provider = new CookieAuthenticationProvider
                        {
                            OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                                TimeSpan.FromMinutes(30),
                                (manager, user) => manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie)),
                            OnApplyRedirect = ctx =>
                                {
                                    if (!IsApiRequest(ctx.Request))
                                        ctx.Response.Redirect(ctx.RedirectUri);
                                }
                        }
                });

            app.Use<ApiAuthenticationMiddleware>();

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
                {
                    TokenEndpointPath = new PathString("/api/Security/GetToken"),
                    Provider = new ApiAuthorizationServerProvider(),
                    AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                    AllowInsecureHttp = true
                });

            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        private static bool IsApiRequest(IOwinRequest request)
        {
            var apiPath = VirtualPathUtility.ToAbsolute("~/api/");

            return request.Uri.LocalPath.StartsWith(apiPath);
        }
    }
}