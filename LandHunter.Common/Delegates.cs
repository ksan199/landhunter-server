﻿namespace LandHunter.Common
{
    public delegate void RefAction<T>(ref T obj);
    public delegate void RefAction<T1, T2>(ref T1 obj1, ref T2 obj2);
}