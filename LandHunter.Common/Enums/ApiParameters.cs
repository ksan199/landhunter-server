﻿namespace LandHunter.Common.Enums
{
    public enum LandDataComposition
    {
        Short,
        Additional,
        Full
    }

    public enum LandStatus
    {
        NotExist,
        Exist,
        HasAdvertisement
    }

    public enum AdvertisementState : byte
    {
        All = 0,
        Active = 1,
        SoldOut = 2,
        Deleted = 3
    }

    public enum ResizeImage
    {
        Default,
        Realtor,
        Agency
    }

    public enum YaKassaResponseStatus
    {
        Success = 0,
        AuthError = 1,
        Reject = 100,
        ParsingError = 200
    }
}