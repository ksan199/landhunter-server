﻿using System.ComponentModel;

namespace LandHunter.Common.Enums
{
    public enum AdvertisementStatusKey : byte
    {
        New = 0,
        Modified = 1,
        Approved = 2,
    }

    public enum FileTypeKey : byte
    {
        Image = 0,
        Video = 1,
        Document = 2,
        Other = 3
    }

    public enum FileCategoryKey : byte
    {
        Image = 0,
        Document = 1
    }

    public enum LogActionType : byte
    {
        [Description("Создано")]
        Created = 0,

        [Description("Обновлено")]
        Updated = 1,

        [Description("Продано")]
        SoldOut = 2,

        [Description("Удалено")]
        Deleted = 3
    }

    public enum OrderStatusKey : byte
    {
        New = 0,
        Approved = 1,
        PaymentReceived = 2,
        Rejected = 3
    }

    public enum OrderTypeKey : byte
    {
        Publication = 1,
        Recharging = 2,
        Service = 3
    }

    public enum ReceptionType : byte
    {
        [Description("E-mail")]
        Email = 0,

        [Description("Почта")]
        Post = 1
    }

    public enum ServiceOrderStatus : byte
    {
        [Description("Новый")]
        New = 0,

        [Description("В обработке")]
        InProcessing = 1,

        [Description("Отменен")]
        Canceled = 2,

        [Description("Завершен")]
        Finished = 3,

        [Description("Не оплачен")]
        Suspended = 4
    }
}