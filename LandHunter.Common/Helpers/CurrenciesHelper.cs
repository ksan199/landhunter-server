﻿using System.Collections.Generic;
using System.Linq;

namespace LandHunter.Common.Helpers
{
    public class CurrenciesHelper
    {
        public static decimal Convert(decimal price, int fromCurrencyId, int toCurrencyId, IEnumerable<IExchangeRate> exchangeRates)
        {
            if (fromCurrencyId != toCurrencyId)
            {
                var exchangeRate = exchangeRates
                    .Where(r => r.From == fromCurrencyId && r.To == toCurrencyId).Select(r => r.Value).FirstOrDefault();

                if (exchangeRate != 0)
                    price = price * exchangeRate;
            }

            return price;
        }
    }

    public interface IExchangeRate
    {
        int From { get; set; }
        int To { get; set; }
        decimal Value { get; set; }
    }
}