﻿using System.IO;
using Newtonsoft.Json;

namespace LandHunter.Common.Helpers
{
    public static class JsonHelper
    {
        public static string Serialize(object value)
        {
            var serializer = new JsonSerializer { NullValueHandling = NullValueHandling.Ignore };

            using (var stringWriter = new StringWriter())
            using (var writer = new JsonTextWriter(stringWriter))
            {
                serializer.Serialize(writer, value);

                return stringWriter.ToString();
            }
        }

        public static T Deserialize<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }
    }
}