﻿using System.Text.RegularExpressions;

namespace LandHunter.Common.Helpers
{
    public class LandsHelper
    {
        public static void NormalizeCadNum(ref string cadNum)
        {
            cadNum = NormalizeCadNum(cadNum);
        }

        public static string NormalizeCadNum(string cadNum)
        {
            var regex = new Regex(RegExps.CadNum);

            if (regex.IsMatch(cadNum))
            {
                var cadNumPieces = cadNum.Split(':');

                cadNumPieces[0] = cadNumPieces[0].PadLeft(2, '0');
                cadNumPieces[1] = cadNumPieces[1].PadLeft(2, '0');
                cadNumPieces[2] = cadNumPieces[2].PadLeft(7, '0');
                cadNumPieces[3] = cadNumPieces[3].TrimStart('0');

                cadNum = string.Join(":", cadNumPieces);
            }
            else
            {
                cadNum = null;
            }

            return cadNum;
        }

        public static decimal ConvertAreaToMeters(decimal area, string areaUnit)
        {
            switch (areaUnit)
            {
                case "050":
                    return area * (decimal)0.0000001;
                case "051":
                    return area * (decimal)0.0001;
                case "053":
                    return area * (decimal)0.001;
                case "058":
                    return area * 1000;
                case "059":
                    return area * 10000;
                case "061":
                    return area * 1000000;
                case "055":
                default:
                    return area;
            }
        }
    }
}
