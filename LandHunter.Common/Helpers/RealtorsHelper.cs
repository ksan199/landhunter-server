﻿using System.Text.RegularExpressions;

namespace LandHunter.Common.Helpers
{
    public class RealtorsHelper
    {
        public static string GetUserNameFromPhone(string phoneNumber)
        {
            return Regex.Replace(phoneNumber, RegExps.NotNumber, "");
        }

        public static string GetNameFirstFullName(string surname, string name, string patronymic)
        {
            return name
                + (!string.IsNullOrEmpty(patronymic) ? " " + patronymic : null)
                + (!string.IsNullOrEmpty(surname) ? " " + surname : null);
        }

        public static string GetFullName(string surname, string name, string patronymic)
        {
            return (!string.IsNullOrEmpty(surname) ? surname + " " : null)
                + name
                + (!string.IsNullOrEmpty(patronymic) ? " " + patronymic : null);
        }
    }
}