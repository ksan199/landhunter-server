﻿namespace LandHunter.Common
{
    public static class RegExps
    {
        public const string CadNum = @"^\d{1,2}[:]\d{1,2}[:]\d{1,7}[:]\d{1,7}$";
        public const string NotNumber = @"[^\d]";
    }
}