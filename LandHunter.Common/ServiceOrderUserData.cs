﻿using System.ComponentModel.DataAnnotations;

namespace LandHunter.Common
{
    public class ServiceOrderUserData
    {
        [Display(Name = "В объеме разделов")]
        public string Completeness { get; set; }

        [Display(Name = "Кадастровый номер")]
        public string CadNum { get; set; }

        [Display(Name = "Адрес")]
        public string Address { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        [Display(Name = "Тип объекта")]
        public string Type { get; set; }

        [Display(Name = "Ориентиры территории")]
        public string Landmarks { get; set; }

        [Display(Name = "Пожелания по разделу")]
        public string Wishes { get; set; }

        [Display(Name = "Причина (основание) для уточнения")]
        public string Reason { get; set; }

        [Display(Name = "Желаемый вид разрешенного использования")]
        public string Utilization { get; set; }

        [Display(Name = "Желаемая категория")]
        public string CategoryCode { get; set; }

        [Display(Name = "Цель разработки")]
        public string DevelopmentGoal { get; set; }

        [Display(Name = "Наличие топографической съемки")]
        public bool? HasSurveying { get; set; }

        [Display(Name = "ФИО")]
        public string FullName { get; set; }

        [Display(Name = "Паспортные данные заявителя")]
        public string Passport { get; set; }

        [Display(Name = "Почтовый адрес")]
        public string PostAddress { get; set; }

        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        [Display(Name = "Желаемая стоимость")]
        public string UserPrice { get; set; }
    }
}