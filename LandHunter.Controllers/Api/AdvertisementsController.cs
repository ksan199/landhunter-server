﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;

namespace LandHunter.Controllers
{
    [Authorize(Roles = "Realtor")]
    public class AdvertisementsController : ApiController
    {
        private readonly IAdvertisementsService _advertisementsService;

        public AdvertisementsController(IAdvertisementsService advertisementsService)
        {
            _advertisementsService = advertisementsService;
        }

        [AllowAnonymous]
        [HttpPost]
        public GridApiModel<AdvertisementApiModel> GetList(AdvertisementsGridBindingModel advertisementsGrid)
        {
            return _advertisementsService.GetList(advertisementsGrid, User);
        }

        [HttpGet]
        public AdvertisementApiModel Get(string cadNum = null)
        {
            return _advertisementsService.Get(cadNum, User);
        }

        [HttpPost]
        public HttpResponseMessage Update(AdvertisementBindingModel advertisement)
        {
            if (advertisement == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("advertisement"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                _advertisementsService.Update(advertisement, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }

        [HttpPost]
        public HttpResponseMessage Create(AdvertisementBindingModel advertisement)
        {
            if (advertisement == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("advertisement"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                _advertisementsService.Create(advertisement, User);

                return new HttpResponseMessage(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }

        [HttpPost]
        public HttpResponseMessage Sell([FromBody]int id)
        {
            try
            {
                _advertisementsService.Sell(id, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }

        [HttpDelete]
        public HttpResponseMessage Delete([FromBody]int id)
        {
            try
            {
                _advertisementsService.Delete(id, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }
    }
}