﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;

namespace LandHunter.Controllers
{
    [Authorize(Roles = "Realtor")]
    public class AgenciesController : ApiController
    {
        private readonly IAgenciesService _agenciesService;

        public AgenciesController(IAgenciesService agenciesService)
        {
            _agenciesService = agenciesService;
        }

        [AllowAnonymous]
        [HttpGet]
        public AgencyApiModel Get(int id)
        {
            return _agenciesService.Get(id, User);
        }

        [AllowAnonymous]
        [HttpGet]
        public IReadOnlyCollection<AgencyApiModel> GetList()
        {
            return _agenciesService.GetList(User);
        }

        [AllowAnonymous]
        [HttpGet]
        public IReadOnlyCollection<AgencyApiModel> Autocomplete(string startsWith)
        {
            return _agenciesService.Autocomplete(startsWith, User);
        }

        [HttpPost]
        public AgencyApiModel Create(AgencyBindingModel agency)
        {
            if (agency == null)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("agency")));

            if (!ModelState.IsValid)
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));

            try
            {
                return _agenciesService.Create(agency, User);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e));
            }
        }

        [HttpPost]
        public HttpResponseMessage Update(EditAgencyBindingModel agency)
        {
            if (agency == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("agency"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                _agenciesService.Update(agency, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }
    }
}