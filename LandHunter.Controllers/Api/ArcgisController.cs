﻿using LandHunter.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LandHunter.Controllers
{
    public class ArcgisController : ApiController
    {
        private readonly IArcgisService arcgisService;

        public ArcgisController(IArcgisService _arcgisService)
        {
            arcgisService = _arcgisService;
        }

        [HttpGet]
        public JObject Query()
        {
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            nvc.Remove("callback");
            return arcgisService.Query(nvc.ToString());
        }

        [HttpGet]
        public JObject Get()
        {
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            nvc.Remove("callback");
            if (Request.RequestUri.LocalPath.IndexOf("get/query") != -1)
            {
                return arcgisService.Query(nvc.ToString());
            }
            else
            {
                return arcgisService.Get(nvc.ToString());
            }
        }

    }
}
