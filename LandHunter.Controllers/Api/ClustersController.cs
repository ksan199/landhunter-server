﻿using System.Collections.Generic;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;

namespace LandHunter.Controllers
{
    public class ClustersController : ApiController
    {
        private readonly IClustersService _clustersService;

        public ClustersController(IClustersService clustersService)
        {
            _clustersService = clustersService;
        }

        [HttpPost]
        public IEnumerable<ClusterApiModel> GetLandsClusters(LandsExtentBindingModel landsExtent)
        {
            return _clustersService.GetLandsClusters(landsExtent);
        }
    }
}