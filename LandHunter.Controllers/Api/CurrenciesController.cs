﻿using System.Collections.Generic;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;
using WebApi.OutputCache.V2.TimeAttributes;

namespace LandHunter.Controllers
{
    public class CurrenciesController : ApiController
    {
        private readonly ICurrenciesService _currenciesService;

        public CurrenciesController(ICurrenciesService currenciesService)
        {
            _currenciesService = currenciesService;
        }

        [CacheOutputUntilToday(18, 5, 0)]
        [HttpGet]
        public IEnumerable<ExchangeRateApiModel> GetExchangeRates()
        {
            return _currenciesService.GetExchangeRates();
        }
    }
}