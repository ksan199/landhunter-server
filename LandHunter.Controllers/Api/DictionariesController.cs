﻿using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;
using WebApi.OutputCache.V2.TimeAttributes;

namespace LandHunter.Controllers
{
    public class DictionariesController : ApiController
    {
        private readonly IDictionariesService _dictionariesService;

        public DictionariesController(IDictionariesService dictionariesService)
        {
            _dictionariesService = dictionariesService;
        }

        [CacheOutputUntilToday]
        [HttpGet]
        public DictionariesApiModel Get()
        {
            return _dictionariesService.Get();
        }
    }
}