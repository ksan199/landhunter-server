﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.Services;

namespace LandHunter.Controllers
{
    [Authorize(Roles = "Realtor")]
    public class FavouritesController : ApiController
    {
        private readonly ILandsService _landsService;

        public FavouritesController(ILandsService landsService)
        {
            _landsService = landsService;
        }

        [HttpGet]
        public IReadOnlyCollection<string> Get()
        {
            return _landsService.GetFavourites(User);
        }

        [HttpPost]
        public HttpResponseMessage Add([FromBody]string cadNum)
        {
            try
            {
                _landsService.AddToFavourites(cadNum, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }

        [HttpDelete]
        public HttpResponseMessage Remove([FromBody]string cadNum)
        {
            try
            {
                _landsService.RemoveFromFavourites(cadNum, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }
    }
}