﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;

namespace LandHunter.Controllers
{
    [Authorize(Roles = "Realtor")]
    public class FilesController : ApiController
    {
        private readonly IFilesService _filesService;

        public FilesController(IFilesService filesService)
        {
            _filesService = filesService;
        }

        [HttpPost]
        public IEnumerable<FileApiModel> Upload()
        {
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            try
            {
                return _filesService.Save(HttpContext.Current.Request, User, isTemporary: true);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.OK, e));
            }
        }
    }
}