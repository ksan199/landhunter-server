﻿using System.Collections.Generic;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;
using System.Net.Http;
using System.Text;
using System;
using System.Net;

namespace LandHunter.Controllers
{
    public class KassaController : ApiController
    {
        private readonly IOrdersService _ordersService;

        public KassaController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        [Authorize(Roles = "Realtor")]
        [HttpPost]
        public HttpResponseMessage CreateOrder(OrderBindingModel order)
        {
            if (order == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("order"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                var result =  _ordersService.Create(order, User);

                return Request.CreateResponse(result.IsCreated ? HttpStatusCode.Created : HttpStatusCode.OK, result);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }

        [HttpPost]
        public IReadOnlyCollection<OrderApiModel> GetLast()
        {
            return _ordersService.GetLast(10, User);
        }
        
        [HttpPost]
        public HttpResponseMessage CheckOrder(YaKassaRequestApiModel request)
        {                       
            var response = _ordersService.CheckOrder(request);

            return new HttpResponseMessage
                {
                    Content = new StringContent(
                        response.ToXmlString(),
                        Encoding.UTF8, 
                        "application/xml")
                };
        }

        [HttpPost]
        public HttpResponseMessage PaymentAviso(YaKassaRequestApiModel request)
        {
            var response = _ordersService.PaymentAviso(request);

            return new HttpResponseMessage
            {
                Content = new StringContent(
                    response.ToXmlString(),
                    Encoding.UTF8,
                    "application/xml")
            };
        }
    }
}
