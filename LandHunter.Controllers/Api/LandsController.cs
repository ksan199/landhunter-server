﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Services;
using Newtonsoft.Json;
using System.Diagnostics;

namespace LandHunter.Controllers
{
    public class LandsController : ApiController
    {
        private readonly ILandsService _landsService;
        private readonly IAdvertisementsService _advertisementsService;
        private readonly string ADDRESS_ERROR_1 = "Нет информации в Росреестре.<br> Вероятно, участок изъят из учета.";
        private readonly string ADDRESS_ERROR_2 = "Адрес неизвестен.";

        public LandsController(ILandsService landsService, IAdvertisementsService advertisementsService)
        {
            _landsService = landsService;
            _advertisementsService = advertisementsService;
        }

        [HttpGet]
        public LandApiModel Get(string cadNum, LandDataComposition dataComposition)
        {
            var land = _landsService.Get(cadNum, dataComposition);

            if (land == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(
                    HttpStatusCode.OK, string.Format("Участок с кадастровым номером {0} не найден.", cadNum)));
            }

            return land;
        }

        [HttpGet]
        public LandApiModel ById(int id, LandDataComposition dataComposition)
        {

            var tmp = _advertisementsService.ById(id);
            string cadNum = "";
            if (tmp != null)
            {
                cadNum = tmp.CadNum;
            }
            else
            {
                throw new HttpResponseException(Request.CreateErrorResponse(
                    HttpStatusCode.OK, string.Format("Участок с идентификатором {0} не найден.", id)));
            }
            return Get(cadNum, dataComposition);
        }

        [HttpGet]
        public Dictionary<string, object> GetFilterSettings()
        {
            return _landsService.GetFilterSettings();
        }

        [HttpPost]
        public IReadOnlyCollection<LandApiModel> Filter(LandsGridBindingModel landsGrid)
        {
            if (landsGrid == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, new ArgumentNullException("landsGrid")));
            }

            if (!ModelState.IsValid)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(
                    HttpStatusCode.OK, ModelState));
            }

            return _landsService.Filter(landsGrid);
        }

        [HttpGet]
        public object GetMeow()
        {
            var requestUrl = "http://pkk5.rosreestr.ru/api/features/1/";
            var cadnum = Request.RequestUri.ParseQueryString().Get("cadNum");
            requestUrl += fixCadNum(cadnum);
            WebClient request = new WebClient();
            string response;
            try
            {
                response = request.DownloadString(requestUrl);
            }
            catch (Exception e)
            {
                response = "";
            }
            dynamic stuff = JsonConvert.DeserializeObject(response);
            double object_id, //= fixNum(stuff.feature._id),
                area_value, //= fixNum(stuff.feature.attrs.area_value),
                area_unit, //= fixNum(stuff.feature.attrs.area_unit),
                cad_cost, //= fixNum(stuff.feature.attrs.cad_cost),
                reg, //= fixNum(stuff.feature.attrs.reg),
                cad_unit, //= fixNum(stuff.feature.attrs.cad_unit);
                xmin,
                xmax,
                ymin,
                ymax,
                xc,
                yc;

            try
            {
                object_id = Convert.ToDouble(stuff.feature._id);
            }
            catch
            {
                object_id = 0;
            }
            try
            {
                area_value = Convert.ToDouble(stuff.feature.attrs.area_value ?? (stuff.feature.attrs ?? (stuff.feature ?? (stuff ?? 0))));
            }
            catch
            {
                area_value = 0;
            }
            try
            {
                area_unit = Convert.ToDouble(stuff.feature.attrs.area_unit ?? (stuff.feature.attrs ?? (stuff.feature ?? (stuff ?? 0))));
            }
            catch
            {
                area_unit = 0;
            }
            try
            {
                cad_cost = Convert.ToDouble(stuff.feature.attrs.cad_cost ?? (stuff.feature.attrs ?? (stuff.feature ?? (stuff ?? 0))));
            }
            catch
            {
                cad_cost = 0;
            }
            try
            {
                reg = Convert.ToDouble(stuff.feature.attrs.reg ?? (stuff.feature.attrs ?? (stuff.feature ?? (stuff ?? 0))));
            }
            catch
            {
                reg = 0;
            }
            try
            {
                cad_unit = Convert.ToDouble(stuff.feature.attrs.cad_unit ?? (stuff.feature.attrs ?? (stuff.feature ?? (stuff ?? 0))));
            }
            catch
            {
                cad_unit = 0;
            }
            try
            {
                xmin = Convert.ToDouble(stuff.feature.extent.xmin);
                xmax = Convert.ToDouble(stuff.feature.extent.xmax);
                ymin = Convert.ToDouble(stuff.feature.extent.ymin);
                ymax = Convert.ToDouble(stuff.feature.extent.ymax);
                xc = Convert.ToDouble(stuff.feature.center.x);
                yc = Convert.ToDouble(stuff.feature.center.x);
            }
            catch
            {
                var quarter = cadnum.Split(':');
                var cadnumQuarter = $"{quarter[0]}:{quarter[1]}:{quarter[2]}";
                requestUrl = "http://pkk5.rosreestr.ru/api/features/2?text=" + cadnumQuarter;
                try
                {
                    response = request.DownloadString(requestUrl);
                }
                catch
                {
                    response = "";
                }
                dynamic stuffQuarter = JsonConvert.DeserializeObject(response);
                try
                {
                    xmin = stuffQuarter.features[0].extent.xmin;
                    xmax = stuffQuarter.features[0].extent.xmax;
                    ymin = stuffQuarter.features[0].extent.ymin;
                    ymax = stuffQuarter.features[0].extent.ymax;
                    xc = stuffQuarter.features[0].center.x;
                    yc = stuffQuarter.features[0].center.y;
                }
                catch (Exception)
                {
                    xmin = xmax = ymin = ymax = xc = yc = 0;
                }
            }

            string
                address1,
                category_type, //= fix(stuff.feature.attrs.category_type),
                util_by_doc, //= fix(stuff.feature.attrs.util_by_doc),

                date_cost, //= fix(stuff.feature.attrs.date_cost),
                date_create, //= fix(stuff.feature.attrs.date_create),
                cad_record_date, //= fix(stuff.feature.attrs.cad_record_date),
                cn, //= fix(stuff.feature.attrs.cn),
                area_type, //= fix(stuff.feature.attrs.area_type),
                contours; //= fix(stuff.feature.attrs.contours);

            try
            {
                address1 = stuff.feature.attrs.address.Value == null ? ADDRESS_ERROR_2 : fix(stuff.feature.attrs.address);
            }
            catch
            {
                address1 = ADDRESS_ERROR_1;
            }
            try
            {
                category_type = fix(stuff.feature.attrs.category_type);
            }
            catch
            {
                category_type = "";
            }
            try
            {
                util_by_doc = fix(stuff.feature.attrs.util_by_doc);
            }
            catch
            {
                util_by_doc = "";
            }
            try
            {
                date_cost = fix(stuff.feature.attrs.date_cost);
            }
            catch
            {
                date_cost = "";
            }
            try
            {
                date_create = fix(stuff.feature.attrs.date_create);
            }
            catch
            {
                date_create = "";
            }
            try
            {
                cad_record_date = fix(stuff.feature.attrs.cad_record_date);
            }
            catch
            {
                cad_record_date = "";
            }
            try
            {
                cn = fix(stuff.feature.attrs.cn);
            }
            catch
            {
                cn = "";
            }
            try
            {
                area_type = fix(stuff.feature.attrs.area_type);
            }
            catch
            {
                area_type = "";
            }
            try
            {
                contours = fix(stuff.feature.attrs.contours);
            }
            catch
            {
                contours = "";
            }
            try
            {
                var callback = new
                {
                    features = new[]
                        {
                    new {
                        attributes = new
                            {
                                OBJECT_ID = object_id,
                                CAD_NUM = cadnum,
                                OBJECT_ADDRESS = address1,
                                CATEGORY_TYPE = category_type,
                                UTIL_BY_DOC = util_by_doc,
                                AREA_VALUE = area_value,
                                AREA_UNIT = area_unit,
                                CAD_COST = cad_cost,
                                DATE_COST = date_cost,
                                DATE_CREATE = date_create,
                                DATE_LOAD = cad_record_date,
                                XC = xc,
                                YC = yc,
                                XMIN = xmin,
                                YMIN = ymin,
                                XMAX = xmax,
                                YMAX = ymax,
                                REGION_KEY = reg,
                                PARCEL_CN = cn,
                                AREA_TYPE = area_type,
                                RIGHT_REG = contours,
                                CAD_UNIT = cad_unit
                            }
                        }
                    }
                };
                return callback;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }

        #region Helpers
        string removeOddNulls(string num)
        {
            if (num.IndexOf('0') == 0)
            {
                num = num.Substring(1);
                return (removeOddNulls(num));
            }
            return num;
        }
        string fixCadNum(string cm)
        {
            var newCadNum = removeOddNulls(cm.Substring(0, 3))
            + removeOddNulls(cm.Substring(3, 3))
            + (removeOddNulls(cm.Substring(6, 7)).Length > 0 ? removeOddNulls(cm.Substring(6, 7)) : "0")
            + cm.Substring(13);
            return newCadNum;
        }
        string fix(object toFix)
        {
            try
            {
                return toFix.ToString();
            }
            catch
            {
                return "";
            }
        }

        double fixNum(object toFix)
        {
            try
            {
                return Convert.ToDouble(toFix);
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }
}