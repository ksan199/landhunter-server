﻿using LandHunter.Services;
using LandHunter.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LandHunter.Controllers.Api
{
    public class MailController : ApiController
    {

        /*private readonly Lazy<IMailService> _mailService;

        public MailController(Lazy<IMailService> mailService)
        {
            _mailService = mailService;
        }*/

        [HttpGet]
        public IEnumerable<string> Info(string body, string userSubject)
        {
#if DEBUG
            string[] mails = { "techno199@live.ru" };
#else
            string[] mails = { "mvovk@uncapital.ru", "a-sobolev@yandex.ru", "soboleva@uncapital.ru" };   
#endif
            List<string> list = new List<string>();

            foreach (var mail in mails)
            {

                var section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;

                var fromAddress = new MailAddress(section.Network.UserName, "landhunter.ru");
                var toAddress = new MailAddress(mail, "");
                string fromPassword = section.Network.Password;
                string subject = userSubject == null ? "Предварительный расчет на снижение кадастровой стоипости" : userSubject;

                var smtp = new SmtpClient
                {
                    Host = section.Network.Host,
                    Port = section.Network.Port,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
            }
            return list.ToArray();
        }

        [HttpGet]
        public string Region()
        {
            string sURL, ip = GetIPAddress();
            sURL = "http://ipinfo.io/" + ip;
            WebRequest wrGETURL;
			wrGETURL = WebRequest.Create(sURL);
			
			WebProxy myProxy = new WebProxy("myproxy",80);
			myProxy.BypassProxyOnLocal = true;

	        wrGETURL.Proxy = WebProxy.GetDefaultProxy();

			Stream objStream;
			objStream = wrGETURL.GetResponse().GetResponseStream();

			StreamReader objReader = new StreamReader(objStream);

			string sLine = "", result = "";

			while (sLine!=null)
			{
				sLine = objReader.ReadLine();
                if (sLine != null)
                    result += sLine;
					//Console.WriteLine("{0}:{1}",i,sLine);
			}
            dynamic stuff = JObject.Parse(result);
            return stuff.postal;
        }


        protected string GetIPAddress()
        {
            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
            }

            return VisitorsIPAddr;
        }

    }
}
