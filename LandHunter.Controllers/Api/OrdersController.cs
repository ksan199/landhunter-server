﻿using LandHunter.ApiModels;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc.Autofac;
using SharpEngine.Web.Mvc.Services;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.WebPages.Html;
using LandHunter.Controllers.Api;
using System.Configuration;
using System.Data;
using System.Web;
using System.Security.Principal;

namespace LandHunter.Controllers
{

    public class OrdersController : ApiController
    {
        
        private readonly IRealtorsService _realtorsService;

        public OrdersController(IRealtorsService realtorsService)
        {
            _realtorsService = realtorsService;
        }
        
        [HttpGet]
        public IEnumerable<RealtorOrder> Get()
        {
            try
            {
                RealtorApiModel realtor = _realtorsService.Get(null, User);
                string realtorMail = realtor.Email;
                string realtorPhone = realtor.PhoneNumber;

                var connectionString = WebConfigurationManager.ConnectionStrings["LandHunterContext"].ConnectionString;
                SqlConnection myConnection = new SqlConnection(connectionString);

                SqlCommand cmd = new SqlCommand();
                SqlDataReader reader;
                var tmp = User;
                cmd.CommandText = String.Format("SELECT [Landhunter].[dbo].[ServiceOrders].[Id]," +
                                    "[Landhunter].[dbo].[Services].[Name]," +
                                    "[CreateDate]," +
                                    "[UpdateDate]," +
                                    "[Status]," +
                                    "[UserData]," +
                                    "[Email]," +
                                    "[ReceptionType]," +
                                    "[RosreestrNumber]," +
                                    "[ReceptionCode]," +
                                    "[ModeratorId]," +
                                    "[Comment]" +
                                    "FROM [Landhunter].[dbo].[ServiceOrders]" +
                                    "inner join [Landhunter].[dbo].[Services]" +
                                    "on [Landhunter].[dbo].[Services].[id] = [Landhunter].[dbo].[ServiceOrders].[ServiceId]" +
                                    "where [UserData] like '%{0}%'", realtorPhone);
                ///UserData like '%+7 (910) 649-24-13%'
                cmd.CommandType = CommandType.Text;
                cmd.Connection = myConnection;

                myConnection.Open();

                reader = cmd.ExecuteReader();
                List<RealtorOrder> list = new List<RealtorOrder>();
                string[]
                    CodeStatus = { "E-Mail", "Почта" };
                string type;
                int intType;
                while (reader.Read())
                {
                    IDataRecord record = (IDataRecord)reader;
                    type = String.Format("{0}", record[7]);
                    intType = Convert.ToInt32(type);
                    type = String.Format("{0}", CodeStatus[intType]);
                    list.Add(new RealtorOrder
                    {
                        ID = String.Format("{0}", record[0]),
                        Name = String.Format("{0}", record[1]),
                        CreateDate = String.Format("{0}", record[2]),
                        UpdateDate = String.Format("{0}", record[3]),
                        Status = String.Format("{0}", record[4]),
                        UserData = String.Format("{0}", record[5]),
                        Email = String.Format("{0}", record[6]),
                        ReceptionType = type,//CodeStatus[intCode], 
                        RosreestrNumber = String.Format("{0}", record[8]),
                        ReceptionCode = String.Format("{0}", record[9]),
                        ModeratorId = String.Format("{0}", record[10]),
                        Comment = String.Format("{0}", record[11])
                    });
                }

                myConnection.Close();

                return list.ToArray();
            }
            catch (Exception e)
            {
                return new List<RealtorOrder>().ToArray();
            }
        }

        [HttpGet]
        public string[] Delete(string Id)
        {
            RealtorApiModel realtor = _realtorsService.Get(null, User);
            string 
                realtorMail = realtor.Email,
                realtorId = realtor.Id,
                price;

            var connectionString = WebConfigurationManager.ConnectionStrings["LandHunterContext"].ConnectionString;
            SqlConnection myConnection = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;
            var tmp = User;
            cmd.CommandText = String.Format("SELECT [Landhunter].[dbo].[ServiceOrders].[Id], [Landhunter].[dbo].[Services].[Price]" +
                                "FROM [Landhunter].[dbo].[ServiceOrders] inner join [Landhunter].[dbo].[Services] on [Landhunter].[dbo].[Services].[id] = [Landhunter].[dbo].[ServiceOrders].[ServiceId]" +
                                "where [Email] = '{0}' and [Landhunter].[dbo].[ServiceOrders].Id = {1} and ([Status] = 0 or [Status] = 4)", realtorMail, Id);

            cmd.CommandType = CommandType.Text;
            cmd.Connection = myConnection;

            myConnection.Open();

            reader = cmd.ExecuteReader();
            List<string> list = new List<string>();
            while (reader.Read())
            {
                IDataRecord record = (IDataRecord)reader;
                list.Add(String.Format("{0}", record[0]));
                price = String.Format("{0}", record[1]);
                price = price.Trim();
                price = price.Replace(",", ".");
                if (price.Length == 0)
                {
                    price = "0";
                }
                list.Add(price);
            }

            myConnection.Close();

            // Если найден id 
            if (list.Count != 0)
            {
                cmd.CommandText = String.Format("update [Landhunter].[dbo].[ServiceOrders] set [Landhunter].[dbo].[ServiceOrders].Status = 2 where Id = {0};" +
                    "update [Landhunter].[dbo].[Realtors] set [Landhunter].[dbo].[Realtors].[Balance] = [Landhunter].[dbo].[Realtors].[Balance] + {1} where [Landhunter].[dbo].[Realtors].Id = '{2}'", list[0], list[1], realtorId);
                myConnection.Open();
                cmd.ExecuteNonQuery();
                myConnection.Close();
            }
            return list.ToArray();
        }
    }
}
