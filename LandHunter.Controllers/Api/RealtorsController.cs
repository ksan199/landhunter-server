﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;

namespace LandHunter.Controllers
{
    [Authorize(Roles = "Realtor")]
    public class RealtorsController : ApiController
    {
        private readonly IRealtorsService _realtorsService;

        public RealtorsController(IRealtorsService realtorsService)
        {
            _realtorsService = realtorsService;
        }

        [AllowAnonymous]
        [HttpPost]
        public RealtorsGridApiModel GetList(RealtorsGridBindingModel realtorsGrid)
        {
            return _realtorsService.GetList(realtorsGrid, User);
        }

        [AllowAnonymous]
        [HttpGet]
        public RealtorApiModel Get(string id = null)
        {
            return _realtorsService.Get(id, User);
        }

        [HttpPost]
        public HttpResponseMessage Update(RealtorBindingModel realtor)
        {
            if (realtor == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("realtor"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                _realtorsService.Update(realtor, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }

        [HttpPost]
        public HttpResponseMessage CheckVisit()
        {
            try
            {
                _realtorsService.CheckVisit(User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e);
            }
        }
    }
}