﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Services;
using LandHunter.Models;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace LandHunter.Controllers
{
    public class RosreestrController : ApiController
    {
        private const string
            // Адрес росреестра
            rosreestr = "https://rosreestr.ru",
            // Адрес страницы к котрой выполняем запрос на получение формы
            requestUrl = rosreestr + "/wps/portal/online_request?return_type=6",
            // Паттерн для поиска формы на странице росреестра
            getFormPattern = "<form[^>]*?method=[^>]*?(\"|')post.*?>",

            getTrPattern = "<tr[^>]*?valign=[^>]*?(\"|')top.*?>(.|\n)*?</tr>",
            getTdPattern = "<td.*?>[^<]*?<a(.|\n)*?</td>",
            getAPattern = "<a(.|\n)*?</a>",
            getHrefPattern = "\"(.*)\"",
            getResultPattern = ">[^<]*?<",
            cadNumPattern = "<td class=[^>]*?(\"|')td.*?>(.|\n)*?</td>",
            nobrPattern = "<nobr(|^>)>.*?</nobr>",
            dateFind = "<td.*?>[^<]*?<b(.|\n)*?</td>",
            date = "\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d";

        private bool next = false;

        [HttpGet]
        public IEnumerable<Rosreestr> Get(string cadNum)
        {

            string formAction = getFormAction();
            if (formAction == null)
            {
                return null;
            }
            formAction = rosreestr + formAction;
            string egrpPage = getEgrpPage(formAction, 0, cadNum);
            string[] addres = getAddres(egrpPage);
            if (addres[0].Length == 0 && addres[1].Length == 0)
            {
                egrpPage = getEgrpPage(formAction, 1, cadNum);
                addres = getAddres(egrpPage);
            }
            if (addres[0].Length == 0 && addres[1].Length == 0)
            {
                egrpPage = getEgrpPage(formAction, 2, cadNum);
                addres = getAddres(egrpPage);
            }
            if (addres[1].Length == 0 && addres[0].Length != 0)
            {
                cadNum = parseCadNum(egrpPage);
                if (cadNum.Length != 0 && !next) 
                {
                    next = true;
                    return Get(cadNum);
                }
            }

            if (addres[1].Length != 0)
            {
                addres[0] = addres[1];
                foreach (Match match in Regex.Matches(addres[1], getResultPattern, RegexOptions.IgnoreCase))
                    addres[1] = match.Value;

                if (addres[1].Length > 0)
                {
                    addres[1] = addres[1].Substring(1, addres[1].Length - 2).Trim();
                }

                foreach (Match match in Regex.Matches(addres[0], getHrefPattern, RegexOptions.IgnoreCase))
                    addres[0] = match.Value;

                if (addres[0].Length > 0) 
                {
                    addres[0] = addres[0].Substring(1, addres[0].Length - 2);
                    addres[0] = getLastUpdateDate(addres[0]);
                }
            }

            Rosreestr[] result = new Rosreestr[]
            {
                new Rosreestr {CadNum = cadNum, LastUpdateDate = addres[0], EGRP = addres[1]}
            };
            return result;
        }

        public string parseCadNum(string html)
        {
            string cadNum = "";
            foreach (Match match in Regex.Matches(html, cadNumPattern, RegexOptions.IgnoreCase))
                cadNum += match.Value;

            foreach (Match match in Regex.Matches(cadNum, nobrPattern, RegexOptions.IgnoreCase))
                cadNum = match.Value;

            foreach (Match match in Regex.Matches(cadNum, getResultPattern, RegexOptions.IgnoreCase))
                cadNum = match.Value;

            if (cadNum.Length > 0)
            {
                cadNum = cadNum.Substring(1, cadNum.Length - 2).Trim();
            }
            return cadNum;
        }

        [HttpGet]
        public IEnumerable<Rosreestr> Search(string cadNum)
        {
            Rosreestr[] result = new Rosreestr[]
            {
                new Rosreestr {CadNum = cadNum}
            };
            return result;
        }

        private string getEgrpPage(string pageUrl, int type, string num)
        {
            WebRequest request = WebRequest.Create(pageUrl);
            string typeString = "";
            switch (type)
            {
                case 0:
                    typeString = "CAD_NUMBER";
                    break;
                case 1:
                    typeString = "OBJ_NUMBER";
                    break;
                case 2:
                    typeString = "OLD_NUMBER";
                    break;
            }
            var postData = "search_action=true";
            postData += "&subject=";
            postData += "&region=";
            postData += "&settlement=";
            postData += "&search_type=" + typeString;
            postData += "&cad_num=" + (type == 0 ? num : "");
            postData += "&start_position=59";
            postData += "&obj_num=" + (type == 1 ? num : "");
            postData += "&old_number=" + (type == 2 ? num : "");
            postData += "&src_object=0";
            postData += "&subject_id=101000000000";
            postData += "&region_id=-1";
            postData += "&street_type=str0";
            postData += "&street=";
            postData += "&house=";
            postData += "&building=";
            postData += "&structure=";
            postData += "&apartment=";
            postData += "&r_subject_id=101000000000";
            postData += "&right_reg=";
            postData += "&encumbrance_reg=";
            var data = Encoding.ASCII.GetBytes(postData);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }

        private string getFormPage(string url)
        {

            string
                responseFromServer; // Результат выполнения запроса на получение формы из росреестра
            // Создаем объект запроса к серверу росреестра
            WebRequest request = WebRequest.Create(url.Length == 0 ? requestUrl : url);
            request.Credentials = CredentialCache.DefaultCredentials;
            // Выполняем get запрос к сайту росрееста
            WebResponse response = request.GetResponse();
            // Создаем объект потока данных
            Stream dataStream = response.GetResponseStream();
            // Содаем объект для чтения данных из потока
            StreamReader reader = new StreamReader(dataStream);
            // Прочитываем целиком результат выполнения запроса и кладем его в прееменную
            responseFromServer = reader.ReadToEnd();
            // Завершаем чтение данных
            reader.Close();
            response.Close();
            return responseFromServer;
        }
        
        private string parseAddresString(string html) 
        {
            string addres = "";
            foreach (Match match in Regex.Matches(html, getTdPattern, RegexOptions.IgnoreCase))
                addres += match.Value;

            foreach (Match match in Regex.Matches(addres, getAPattern, RegexOptions.IgnoreCase))
                addres = match.Value;

            /*foreach (Match match in Regex.Matches(addres, getResultPattern, RegexOptions.IgnoreCase))
                addres = match.Value;

            if (addres.Length > 0)
            {
                addres = addres.Substring(1, addres.Length - 2).Trim();
            }*/
            return addres;
        }

        private string[] getAddres(string pageHtml) 
        {
            string[] addres = { "", "" };
            int i = 0;
            foreach (Match match in Regex.Matches(pageHtml, getTrPattern, RegexOptions.IgnoreCase))
                addres[i++] += parseAddresString(match.Value);
            return addres;
        }

        private string getFormAction()
        {
            string formPageString = getFormPage(""),  // Результат выполнения запроса на получение формы из росреестра
                form = "";                          // Строка формы
            // Ищем на странице форму 
            foreach (Match match in Regex.Matches(formPageString, getFormPattern, RegexOptions.IgnoreCase))
                form = match.Value;
            // Позиция подстроки
            int position;
            position = form.IndexOf("action");
            if (position != -1)
            {
                form = form.Substring(position);
            }
            else
            {
                return null;
            }
            position = form.IndexOf("\"");
            if (position != -1)
            {
                form = form.Substring(position + 1);
            }
            else
            {
                return null;
            }
            position = form.IndexOf("\"");
            if (position != -1)
            {
                form = form.Substring(0, position);
            }
            else
            {
                return null;
            }
            return form;
        }

        private string getLastUpdateDate(string url)
        {
            string html = getFormPage(rosreestr + url);
            foreach (Match match in Regex.Matches(html, dateFind, RegexOptions.IgnoreCase))
                foreach (Match match2 in Regex.Matches(match.Value, date, RegexOptions.IgnoreCase))
                    return match2.Value;
            return "";
        }
    }

}
