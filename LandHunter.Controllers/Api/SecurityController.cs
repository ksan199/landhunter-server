﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;

namespace LandHunter.Controllers
{
    [Authorize(Roles = "Realtor")]
    public class SecurityController : ApiController
    {
        private readonly ISecurityService _securityService;

        public SecurityController(ISecurityService securityService)
        {
            _securityService = securityService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<HttpResponseMessage> Register(RegistrationBindingModel registration)
        {
            if (registration == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("registration"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                await _securityService.RegisterAsync(registration);

                return new HttpResponseMessage(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<HttpResponseMessage> ResetPassword(ResetPasswordBindingModel resetPassword)
        {
            if (resetPassword == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("resetPassword"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                await _securityService.ResetPasswordAsync(resetPassword);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e.Message);
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> ChangePassword(ChangePasswordBindingModel changePassword)
        {
            if (changePassword == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("changePassword"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                await _securityService.ChangePasswordAsync(changePassword, User);

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<bool> VerifyUser(string phoneNumber)
        {
            return await _securityService.VerifyUser(phoneNumber);
        }
    }
}