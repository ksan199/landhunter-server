﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;
using WebApi.OutputCache.V2.TimeAttributes;

namespace LandHunter.Controllers
{
    public class ServicesController : ApiController
    {
        private readonly IServicesService _servicesService;

        public ServicesController(IServicesService servicesService)
        {
            _servicesService = servicesService;
        }

        [CacheOutputUntilToday]
        [HttpGet]
        public IReadOnlyCollection<ServiceCategoryApiModel> GetList()
        {
            return _servicesService.GetList();
        }

        [Authorize(Roles = "Realtor")]
        [HttpPost]
        public HttpResponseMessage Create(ServiceOrderBindingModel order)
        {
            if (order == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, new ArgumentNullException("order"));

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.OK, ModelState);

            try
            {
                _servicesService.Create(order, User);

                return new HttpResponseMessage(HttpStatusCode.Created);
            }
            catch (Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.OK, e.Message);
            }
        }
    }
}