﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LandHunter.ApiModels;
using LandHunter.Services;
using WebApi.OutputCache.V2;

namespace LandHunter.Controllers
{
    public class SitePagesController : ApiController
    {
        private readonly ISitePagesService _sitePagesService;

        public SitePagesController(ISitePagesService sitePagesService)
        {
            _sitePagesService = sitePagesService;
        }

        [CacheOutput(ClientTimeSpan = 7200, ServerTimeSpan = 7200)]
        [HttpGet]
        public SitePageApiModel Get(string pageUrl)
        {
            var page = _sitePagesService.Get(pageUrl);

            if (page == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(
                    HttpStatusCode.OK, string.Format("Страница с url '{0}' не найдена.", pageUrl)));
            }

            return page;
        }

        [CacheOutput(ClientTimeSpan = 7200, ServerTimeSpan = 7200)]
        [HttpGet]
        public IReadOnlyCollection<SitePageApiModel> GetList()
        {
            return _sitePagesService.GetList();
        }
    }
}