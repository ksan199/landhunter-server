﻿using System.Web.Mvc;

namespace LandHunter.Controllers.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "LandvertisementsAdmin",
                "Admin/Landvertisements/{action}/{id}",
                new { controller = "Advertisements", action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "DefaultAdmin",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Advertisements", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
