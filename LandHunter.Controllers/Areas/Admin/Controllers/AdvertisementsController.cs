﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using LandHunter.ViewModels;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "Moderator,SuperAdministrator")]
    public class AdvertisementsController : FiltrateCRUDController<Advertisement, int, AdvertisementGrid, AdvertisementGridOptions, AdvertisementFilter>
    {
        private readonly IAdvertisementsBaseService _advertisementsService;

        public AdvertisementsController(
            IFilterableBaseService<Advertisement, int, AdvertisementGrid, AdvertisementGridOptions, AdvertisementFilter> service,
            IAdvertisementsBaseService advertisementsService)
            : base(service)
        {
            _advertisementsService = advertisementsService;
        }

        public ActionResult ByLandGrid(string cadNum, AdvertisementGridOptions options)
        {
            return View("Grid", _advertisementsService.GetByLandActionGrid(cadNum, options, User));
        }

        [HttpPost]
        public JsonResult MoveToLand(MoveToLandViewModel data)
        {
            return ExecuteMethod(() => _advertisementsService.MoveToLand(data));
        }

        [HttpPost]
        public JsonResult AllMoveToLand()
        {
            return ExecuteMethod(() => _advertisementsService.AllMoveToLand());
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(Advertisement create)
        {
            return null;
        }

        public override string ToString()
        {
            return "Объявления";
        }
    }
}