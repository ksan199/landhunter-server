﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "Moderator,SuperAdministrator")]
    public class AdvertisementsLogsController : FiltrateCRUDController<AdvertisementLog, int, AdvertisementLogGrid, AdvertisementLogGridOptions, AdvertisementLogFilter>
    {
        public AdvertisementsLogsController(IFilterableBaseService<AdvertisementLog, int, AdvertisementLogGrid, AdvertisementLogGridOptions, AdvertisementLogFilter> service)
            : base(service)
        {
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(AdvertisementLog create)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult Delete(int id)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult BatchDelete(int[] ids)
        {
            return null;
        }

        public override string ToString()
        {
            return "Логи объявлений";
        }
    }
}