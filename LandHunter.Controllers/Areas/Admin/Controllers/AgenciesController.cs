﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "UsersAdministrator,SuperAdministrator")]
    public class AgenciesController : CRUDController<Agency, int, AgencyGrid, AgencyGridOptions>
    {
        public AgenciesController(IBaseService<Agency, int, AgencyGrid, AgencyGridOptions> service)
            : base(service)
        {
        }

        public override string ToString()
        {
            return "Агенства";
        }
    }
}