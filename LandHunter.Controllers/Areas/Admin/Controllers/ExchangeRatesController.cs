﻿using System;
using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdministrator")]
    public class ExchangeRatesController : CRUDController<ExchangeRate, int, ExchangeRateGrid, ExchangeRateGridOptions>
    {
        private readonly IExchangeRatesService _exchangeRatesService;

        public ExchangeRatesController(
            IBaseService<ExchangeRate, int, ExchangeRateGrid, ExchangeRateGridOptions> service,
            IExchangeRatesService exchangeRatesService)
            : base(service)
        {
            _exchangeRatesService = exchangeRatesService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RefreshDailyRates()
        {
            return ExecuteMethod(() => _exchangeRatesService.RefreshDailyRates());
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(ExchangeRate create)
        {
            return null;
        }

        [HttpGet]
        public override ActionResult Edit(int id)
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(ExchangeRate edit)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult Delete(int id)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult BatchDelete(int[] ids)
        {
            return null;
        }

        public override string ToString()
        {
            return "Курс валют";
        }
    }
}