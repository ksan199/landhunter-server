﻿using System;
using System.Web.Mvc;
using LandHunter.Services;
using LandHunter.ViewModels;
using ControllerBase = SharpEngine.Web.Mvc.ControllerBase;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "Moderator,SuperAdministrator")]
    public class FilesController : ControllerBase
    {
        private readonly IFilesService _filesService;

        public FilesController(IFilesService filesService)
        {
            _filesService = filesService;
        }

        public ActionResult AdvertisementFiles(int advertisementId)
        {
            return View(_filesService.GetAdvertisementFiles(advertisementId, User));
        }

        public ActionResult LandFiles(string cadNum)
        {
            return View("FilesList", _filesService.GetLandFiles(cadNum, User));
        }

        public ActionResult UserFiles(string userId)
        {
            return View("FilesList", _filesService.GetUserFiles(userId, User));
        }

        public ActionResult AgencyFiles(int agencyId)
        {
            return View("FilesList", _filesService.GetAgencyFiles(agencyId, User));
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            return View(_filesService.Edit(id, User));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditFileViewModel edit)
        {
            if (!ViewData.ModelState.IsValid)
                return View(edit);

            try
            {
                _filesService.Edit(edit, User);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Global", e.Message);

                return View(edit);
            }

            return RedirectToAction("Edit", new { id = edit.Id });
        }

        [HttpPost]
        public JsonResult Toggle(int id, bool isDeactivatedNow)
        {
            return ExecuteMethod(() => _filesService.Toggle(id, isDeactivatedNow, User));
        }

        [HttpDelete]
        public JsonResult Delete(int id)
        {
            return ExecuteMethod(() => _filesService.MarkAsDeleted(id, User));
        }

        [HttpGet]
        public JsonResult Sort(int[] ids)
        {
            return ExecuteMethod(() => _filesService.Sort(ids));
        }
    }
}