﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Services;
using ControllerBase = SharpEngine.Web.Mvc.ControllerBase;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdministrator")]
    public class LandCategoriesController : ControllerBase
    {
        private readonly IBaseService<LandCategory, string, LandCategoryGrid, LandCategoryGridOptions> _service;

        public LandCategoriesController(IBaseService<LandCategory, string, LandCategoryGrid, LandCategoryGridOptions> service)
        {
            _service = service;
        }

        public virtual ActionResult Index()
        {
            return View(model: this.ToString());
        }

        public virtual ActionResult Grid(LandCategoryGridOptions options)
        {
            return View(_service.GetActionGrid(options, User));
        }

        public override string ToString()
        {
            return "Категории земель";
        }
    }
}