﻿using System;
using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using SharpEngine.Web.Mvc.Controls;
using ControllerBase = SharpEngine.Web.Mvc.ControllerBase;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "Moderator,SuperAdministrator")]
    public class LandsController : ControllerBase
    {
        private readonly ILandsBaseService _landsService;

        public LandsController(
            ILandsBaseService landsService)
        {
            _landsService = landsService;
        }

        public virtual ActionResult Index()
        {
            return View("IndexWithFilter", model: this.ToString());
        }

        public ActionResult Filter(LandGridOptions options)
        {
            var filterKey = GridOptionsModelBinder.GridKey(ControllerContext);

            return View(_landsService.GetFilter(options, filterKey, User));
        }

        [HttpGet]
        public ActionResult ClearFilter()
        {
            GridOptionsModelBinder.ClearGridOptions(ControllerContext);

            return RedirectToAction("Index");
        }

        public virtual ActionResult Grid(LandGridOptions options)
        {
            return View(_landsService.GetActionGrid(options, User));
        }

        [HttpGet]
        public virtual ActionResult Edit(string id)
        {
            return View(_landsService.Edit(id.Replace('_', ':'), User));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit(Land edit)
        {
            if (!ViewData.ModelState.IsValid)
                return View(_landsService.FillDictionaries(edit, User));

            try
            {
                _landsService.Edit(edit, User);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Global", e.Message);

                return View(_landsService.FillDictionaries(edit, User));
            }

            return RedirectToAction("Edit", new { id = edit.CadNum.Replace(':', '_') });
        }

        [HttpPost]
        public JsonResult UpdateCadastreInfo(string[] cadNums)
        {
            return ExecuteMethod(() => _landsService.UpdateCadastreInfo(cadNums));
            //return ExecuteResultMethod(() => _landsService.UpdateAllCadastreInfo());
        }

        [HttpPost]
        public JsonResult UpdateAllCadastreInfo(int batchSize)
        {
            return ExecuteMethod(() => _landsService.UpdateAllCadastreInfo());
        }

        public override string ToString()
        {
            return "Земельные участки";
        }
    }
}