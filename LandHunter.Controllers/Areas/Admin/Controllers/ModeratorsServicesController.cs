﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdministrator")]
    public class ModeratorsServicesController : CRUDController<ModeratorService, int, ModeratorServiceGrid, ModeratorServiceGridOptions>
    {
        private readonly IModeratorsServicesBaseService _moderatorsServicesService;

        public ModeratorsServicesController(
            IBaseService<ModeratorService, int, ModeratorServiceGrid, ModeratorServiceGridOptions> service,
            IModeratorsServicesBaseService moderatorsServicesService)
            : base(service)
        {
            _moderatorsServicesService = moderatorsServicesService;
        }

        public virtual ActionResult ServiceIndex(int serviceId)
        {
            return View("Index", serviceId);
        }

        public ActionResult ServiceGrid(int serviceId, ModeratorServiceGridOptions options)
        {
            return View("Grid", _moderatorsServicesService.GetActionGrid(serviceId, options, User));
        }

        [HttpGet]
        public ActionResult ServiceCreate(int serviceId)
        {
            return View("Create", _service.BeforeGet(new ModeratorService { ServiceId = serviceId }, User));
        }
    }
}