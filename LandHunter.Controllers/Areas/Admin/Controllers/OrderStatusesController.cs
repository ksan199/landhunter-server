﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdministrator")]
    public class OrderStatusesController : CRUDController<OrderStatus, int, OrderStatusGrid, OrderStatusGridOptions>
    {
        public OrderStatusesController(IBaseService<OrderStatus, int, OrderStatusGrid, OrderStatusGridOptions> service)
            : base(service)
        {
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(OrderStatus create)
        {
            return null;
        }

        [HttpGet]
        public override ActionResult Edit(int id)
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(OrderStatus edit)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult Delete(int id)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult BatchDelete(int[] ids)
        {
            return null;
        }

        public override string ToString()
        {
            return "Статусы операций";
        }
    }
}