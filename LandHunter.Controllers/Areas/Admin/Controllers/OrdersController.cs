﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "Moderator,SuperAdministrator")]
    public class OrdersController : FiltrateCRUDController<Order, int, OrderGrid, OrderGridOptions, OrderFilter>
    {
        public OrdersController(IFilterableBaseService<Order, int, OrderGrid, OrderGridOptions, OrderFilter> service)
            : base(service)
        {
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(Order create)
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(Order edit)
        {
            return null;
        }

        public override string ToString()
        {
            return "Операции";
        }
    }
}