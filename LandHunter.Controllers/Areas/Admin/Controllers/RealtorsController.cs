﻿using System;
using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using LandHunter.ViewModels;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "UsersAdministrator,SuperAdministrator")]
    public class RealtorsController : FiltrateCRUDController<Realtor, string, RealtorGrid, RealtorGridOptions, RealtorFilter>
    {
        private readonly IRealtorsBaseService _realtorsService;

        public RealtorsController(
            IFilterableBaseService<Realtor, string, RealtorGrid, RealtorGridOptions, RealtorFilter> service,
            IRealtorsBaseService realtorsService)
            : base(service)
        {
            _realtorsService = realtorsService;
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return View(_realtorsService.FillDictionaries(new CreateRealtorViewModel(), User));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRealtorUser(CreateRealtorViewModel create)
        {
            if (!ViewData.ModelState.IsValid)
                return View("Create", _realtorsService.FillDictionaries(create, User));

            try
            {
                _realtorsService.CreateRealtorUser(create, User);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Global", e.Message);

                return View("Create", _realtorsService.FillDictionaries(create, User));
            }

            return RedirectToAction("Edit", new { id = create.Id });
        }

        [HttpGet]
        public override ActionResult Edit(string id)
        {
            return View(_realtorsService.EditRealtorUser(id, User));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRealtorUser(EditRealtorViewModel edit)
        {
            if (!ViewData.ModelState.IsValid)
                return View("Edit", _realtorsService.FillDictionaries(edit, User));

            try
            {
                _realtorsService.EditRealtorUser(edit, User);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Global", e.Message);

                return View("Edit", _realtorsService.FillDictionaries(edit, User));
            }

            return RedirectToAction("Edit", new { id = edit.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(Realtor create)
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Edit(Realtor edit)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult BatchDelete(string[] ids)
        {
            return null;
        }

        public override string ToString()
        {
            return "Риелторы";
        }
    }
}