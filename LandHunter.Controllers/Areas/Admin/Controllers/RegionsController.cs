﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdministrator")]
    public class RegionsController : CRUDController<Region, int, RegionGrid, RegionGridOptions>
    {
        public RegionsController(IBaseService<Region, int, RegionGrid, RegionGridOptions> service)
            : base(service)
        {
        }

        public override string ToString()
        {
            return "Регионы";
        }
    }
}