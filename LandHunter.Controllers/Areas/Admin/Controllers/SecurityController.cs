﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using LandHunter.AspNetIdentity;
using LandHunter.DataAccess.Context;
using LandHunter.Models;
using LandHunter.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SharpEngine.Web.Mvc.AspNetIdentity;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize]
    public class SecurityController : Controller
    {
        private readonly IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> _aspNetIdentity;

        public SecurityController(IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> aspNetIdentity)
        {
            _aspNetIdentity = aspNetIdentity;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> SignIn()
        {
            if (User.Identity.IsAuthenticated)
                return await RedirectToHomePage();

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignIn(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _aspNetIdentity.SignInAsync(model.Login, model.Password, model.RememberMe);

                    if (Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);

                    return await RedirectToHomePage(model.Login);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Global", e.Message);
                }
            }

            return View(model);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _aspNetIdentity.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Global", e.Message);
                }
            }

            return View(model);
        }

        public async Task<ActionResult> ResetPassword(string id)
        {
            var user = await _aspNetIdentity.FindUserByIdAsync(id);

            return View(new ResetPasswordViewModel { Id = id, UserName = user.UserName });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (await _aspNetIdentity.IsUserInRoleAsync(model.Id, "SuperAdministrator"))
                        throw new Exception("Нельзя сбросить пароль пользователю - супер администратору.");

                    await _aspNetIdentity.ResetPasswordAsync(model.Id, model.NewPassword);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Global", e.Message);
                }
            }

            return View(model);
        }

        public async Task<ActionResult> Manage()
        {
            return View(new AdminUserManageViewModel { Email = await _aspNetIdentity.GetEmailAsync(User.Identity.GetUserId()) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(AdminUserManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _aspNetIdentity.SetEmailAsync(User.Identity.GetUserId(), model.Email);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("Global", e.Message);
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignOut()
        {
            _aspNetIdentity.SignOut();

            return RedirectToAction("SignIn");
        }

        private async Task<RedirectToRouteResult> RedirectToHomePage(string userName = null)
        {
            if (userName != null)
            {
                var user = await _aspNetIdentity.FindUserByNameAsync(userName);

                if (user != null)
                {
                    var userRoles = await _aspNetIdentity.GetRolesForUserAsync(user.Id);

                    if (userRoles.Contains("UsersAdministrator"))
                        return RedirectToAction("Index", "Users");

                    if (userRoles.Contains("SiteAdministrator"))
                        return RedirectToAction("Index", "SitePages");
                }

                return RedirectToAction("Index", "Advertisements");
            }

            if (User.IsInRole("UsersAdministrator"))
                return RedirectToAction("Index", "Users");

            if (User.IsInRole("SiteAdministrator"))
                return RedirectToAction("Index", "SitePages");

            return RedirectToAction("Index", "Advertisements");
        }
    }
}