﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using LandHunter.ViewModels;
using SharpEngine.Core;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "Moderator,SuperAdministrator")]
    public class ServiceOrdersController : FiltrateCRUDController<ServiceOrder, int, ServiceOrderGrid, ServiceOrderGridOptions, ServiceOrderFilter>
    {
        private readonly Lazy<IServiceOrdersBaseService> _serviceOrdersService;
        private readonly Lazy<IMailService> _mailService;

        public ServiceOrdersController(
            IFilterableBaseService<ServiceOrder, int, ServiceOrderGrid, ServiceOrderGridOptions, ServiceOrderFilter> service,
            Lazy<IServiceOrdersBaseService> serviceOrdersService,
            Lazy<IMailService> mailService)
            : base(service)
        {
            _serviceOrdersService = serviceOrdersService;
            _mailService = mailService;
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(ServiceOrder create)
        {
            return null;
        }

        [Authorize(Roles = "Moderator")]
        [HttpPost]
        public JsonResult Take(int id)
        {
            return ExecuteMethod(() => _serviceOrdersService.Value.Take(id, User));
        }

        [Authorize(Roles = "Moderator")]
        [HttpGet]
        public async Task<ActionResult> SendEmail(int id)
        {
            var email = await _mailService.Value.GetNewServiceEmailAsync(id);

            return View(email);
        }

        [Authorize(Roles = "Moderator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendEmail(ServiceEmailViewModel email)
        {
            if (!ViewData.ModelState.IsValid)
                return View(email);

            try
            {
                await _mailService.Value.SendServiceEmailAsync(email);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Global", e.GetFullErrorMessage());
            }

            return View(email);
        }

        public override string ToString()
        {
            return "Заказы услуг";
        }
    }
}