﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdministrator")]
    public class ServicesController : CRUDController<Service, int, ServiceGrid, ServiceGridOptions>
    {
        public ServicesController(IBaseService<Service, int, ServiceGrid, ServiceGridOptions> service)
            : base(service)
        {
        }

        [HttpGet]
        public override ActionResult Create()
        {
            return null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override ActionResult Create(Service create)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult Delete(int id)
        {
            return null;
        }

        [HttpDelete]
        [ValidateAntiForgeryToken]
        public override JsonResult BatchDelete(int[] ids)
        {
            return null;
        }

        public override string ToString()
        {
            return "Услуги";
        }
    }
}