﻿using System;
using System.Web.Mvc;
using LandHunter.Models;
using LandHunter.Services;
using ControllerBase = SharpEngine.Web.Mvc.ControllerBase;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdministrator")]
    public class SettingsController : ControllerBase
    {
        private readonly ISettingsService _settingsService;

        public SettingsController(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return View(_settingsService.Edit(User));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Settings edit)
        {
            if (!ViewData.ModelState.IsValid)
                return View(edit);

            try
            {
                _settingsService.Edit(edit, User);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Global", e.Message);

                return View(edit);
            }

            return RedirectToAction("Edit");
        }
    }
}