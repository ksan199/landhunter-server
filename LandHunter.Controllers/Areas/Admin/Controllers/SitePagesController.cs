﻿using System.Web.Mvc;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Controllers.Areas.Admin.Controllers
{
    [Authorize(Roles = "SiteAdministrator,SuperAdministrator")]
    public class SitePagesController : CRUDController<SitePage, int, SitePageGrid, SitePageGridOptions>
    {
        public SitePagesController(IBaseService<SitePage, int, SitePageGrid, SitePageGridOptions> service)
            : base(service)
        {
        }

        public override string ToString()
        {
            return "Страницы";
        }
    }
}