﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using LandHunter.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LandHunter.DataAccess.Context
{
    public class LandHunterContext : IdentityDbContext<ApplicationUser>
    {
        public LandHunterContext()
            : base("LandHunterContext", false)
        {
            Database.SetInitializer<LandHunterContext>(null);
        }

        #region SQL Tables

        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<AdvertisementStatus> AdvertisementStatus { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Land> Land { get; set; }
        public DbSet<LandCategory> LandCategory { get; set; }
        public DbSet<AdvertisementLog> AdvertisementsLogs { get; set; }
        public DbSet<Realtor> Realtors { get; set; }
        public DbSet<SitePage> SitePages { get; set; }
        public DbSet<Agency> Agencies { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<RealtorFavouriteLand> RealtorsFavouriteLands { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<OrderType> OrderTypes { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceCategory> ServiceCategories { get; set; }
        public DbSet<ModeratorService> ModeratorsServices { get; set; }
        public DbSet<ServiceOrder> ServiceOrders { get; set; }
        public DbSet<ArcgisLog> Arcgis { get; set; }

        #endregion

        #region SQL Views

        public virtual DbSet<LandExtendedView> LandsExtendedView { get; set; }
        public DbSet<AdvertisementExtended> AdvertisementsExtended { get; set; }
        public DbSet<RealtorExtended> RealtorsExtended { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region SQL Tables

            modelBuilder.Entity<Currency>().ToTable("Currency");
            modelBuilder.Entity<ExchangeRate>().ToTable("ExchangeRates");
            modelBuilder.Entity<Land>().ToTable("Land");
            modelBuilder.Entity<Advertisement>().ToTable("Advertisements");
            modelBuilder.Entity<Realtor>().ToTable("Realtors");
            modelBuilder.Entity<AdvertisementLog>().ToTable("AdvertisementsLogs");
            modelBuilder.Entity<LandCategory>().ToTable("LandCategory");
            modelBuilder.Entity<RealtorFavouriteLand>().ToTable("RealtorsFavouriteLands");
            modelBuilder.Entity<Settings>().ToTable("Settings");
            modelBuilder.Entity<ModeratorService>().ToTable("ModeratorsServices");
            modelBuilder.Entity<ArcgisLog>().ToTable("ArcgisLog");

            /*modelBuilder.Entity<ArcgisLog>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);*/

            modelBuilder.Entity<Realtor>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<ServiceOrder>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            modelBuilder.Entity<Realtor>()
                .HasRequired(e => e.User)
                .WithOptional(e => e.Realtor)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ServiceOrder>()
                .HasRequired(e => e.Order)
                .WithOptional(e => e.ServiceOrder)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Advertisement>()
                .Property(e => e.Price)
                .HasPrecision(20, 8);

            modelBuilder.Entity<Advertisement>()
                .Property(e => e.Area)
                .HasPrecision(12, 1);

            modelBuilder.Entity<Advertisement>()
                .HasMany(e => e.Files)
                .WithOptional(e => e.Advertisement)
                .HasForeignKey(e => e.AdvertisementId);

            modelBuilder.Entity<Land>()
                .Property(e => e.Area)
                .HasPrecision(12, 1);

            modelBuilder.Entity<Land>()
                .HasMany(e => e.Advertisements)
                .WithRequired(e => e.Land)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LandCategory>()
                .HasMany(e => e.Advertisements)
                .WithOptional(e => e.LandCategory)
                .HasForeignKey(e => e.CategoryCode);

            modelBuilder.Entity<LandCategory>()
                .HasMany(e => e.Land)
                .WithOptional(e => e.LandCategory)
                .HasForeignKey(e => e.CategoryCode);

            modelBuilder.Entity<Agency>()
                .HasRequired(e => e.User)
                .WithMany(e => e.Agencies)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Realtor>()
                .Property(e => e.Balance)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Order>()
                .Property(e => e.OrderSumAmount)
                .HasPrecision(14, 2);

            modelBuilder.Entity<Order>()
                .Property(e => e.BalanceBefore)
                .HasPrecision(18, 0);

            #endregion

            #region SQL Views

            modelBuilder.Entity<LandExtendedView>().ToTable("LandsExtendedView");
            modelBuilder.Entity<AdvertisementExtended>().ToTable("AdvertisementsExtendedView");
            modelBuilder.Entity<RealtorExtended>().ToTable("RealtorsExtendedView");

            modelBuilder.Entity<LandExtendedView>()
                .Property(e => e.Area)
                .HasPrecision(12, 1);

            modelBuilder.Entity<LandExtendedView>()
                .Property(e => e.CadastrePrice)
                .HasPrecision(14, 2);

            modelBuilder.Entity<LandExtendedView>()
                .Property(e => e.MinRubPrice)
                .HasPrecision(33, 4);

            modelBuilder.Entity<LandExtendedView>()
                .HasMany(l => l.Advertisements)
                .WithRequired(a => a.LandExtended)
                .HasForeignKey(a => a.CadNum);

            modelBuilder.Entity<LandExtendedView>()
                .HasOptional(l => l.LandCategory)
                .WithMany(c => c.LandsExtended)
                .HasForeignKey(l => l.CategoryCode);

            modelBuilder.Entity<AdvertisementExtended>()
                .Property(e => e.Price)
                .HasPrecision(20, 8);

            modelBuilder.Entity<AdvertisementExtended>()
                .Property(e => e.Area)
                .HasPrecision(12, 1);

            modelBuilder.Entity<AdvertisementExtended>()
                .Property(e => e.PriceInRub)
                .HasPrecision(33, 4);

            modelBuilder.Entity<AdvertisementExtended>()
                .HasRequired(a => a.Land)
                .WithMany(l => l.AdvertisementsExtended)
                .HasForeignKey(a => a.CadNum);

            modelBuilder.Entity<RealtorExtended>()
                .HasRequired(r => r.User)
                .WithRequiredDependent();

            modelBuilder.Entity<RealtorExtended>()
                .HasOptional(r => r.Agency)
                .WithMany(a => a.RealtorsExtended)
                .HasForeignKey(r => r.AgencyId);

            modelBuilder.Entity<RealtorExtended>()
                .HasMany(r => r.Advertisements)
                .WithRequired(a => a.RealtorExtended)
                .HasForeignKey(a => a.RealtorId);

            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}