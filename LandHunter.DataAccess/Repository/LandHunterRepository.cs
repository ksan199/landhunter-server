﻿using LandHunter.DataAccess.Context;
using SharpEngine.Repository;

namespace LandHunter.DataAccess.Repository
{
    public class LandHunterRepository<TEntity, TKey> : BaseRepository<LandHunterContext, TEntity, TKey>
        where TEntity : class
    {
        public LandHunterRepository(LandHunterContext context)
            : base(context)
        {
        }

        public LandHunterRepository(LandHunterContext context, string keyName)
            : base(context, keyName)
        {
        }
    }
}