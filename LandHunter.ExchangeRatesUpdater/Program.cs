﻿using System;
using Autofac;
using LandHunter.DataAccess.Context;
using LandHunter.DataAccess.Repository;
using LandHunter.Services;
using LandHunter.Services.CBRSoapService;
using SharpEngine.Repository;

namespace LandHunter.ExchangeRatesUpdater
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            RegisterDependencies();

            try
            {
                RefreshDailyRates();

                Console.WriteLine("Daily rates was successfully updated.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while updating daily rates: " + e.Message + " - " + e.StackTrace);
            }
#if DEBUG
            Console.WriteLine("Press any key to close console.");
            Console.ReadKey();
#endif
        }

        private static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DailyInfoSoapClient>().As<DailyInfoSoap>().InstancePerLifetimeScope();
            builder.RegisterType<LandHunterContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(LandHunterRepository<,>)).As(typeof(IRepository<,>)).InstancePerLifetimeScope();
            builder.RegisterType<ExchangeRatesService>().As<IExchangeRatesService>().InstancePerLifetimeScope();

            Container = builder.Build();
        }

        /// <summary>
        /// Получает официальные курсы валют на последнюю установленную дату с сервиса ЦБ.
        /// Как правило, официальные курсы устанавливаются Банком России до 15:00 по московскому времени.
        /// </summary>
        private static void RefreshDailyRates()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var exchangeRatesService = scope.Resolve<IExchangeRatesService>();

                exchangeRatesService.RefreshDailyRates();
            }
        }
    }
}