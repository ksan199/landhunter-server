﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LandHunter.AspNetIdentity;
using LandHunter.DataAccess.Context;
using LandHunter.Models;
using LandHunter.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.AspNetIdentity;
using SharpEngine.Web.Mvc.Autofac;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AdminUserFilter : Filter<EditAdminUserViewModel>
    {
        protected override Filter<EditAdminUserViewModel> Configure()
        {
            var aspNetIdentity = AutofacLifetimeScope.GetInstance<IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>>>();

            var roles = new List<SelectListItem>();
            roles.AddRange(aspNetIdentity.GetRolesQuery()
                .Where(r => r.Name == "Moderator" || r.Name == "SiteAdministrator" || r.Name == "UsersAdministrator")
                .OrderBy(r => r.Name)
                .ToSelectList(r => r.Id, r => r.Name));

            if (roles.Count > 1)
                AddCondition(new RoleFilterCondition(roles));

            AddCondition(new BoolFilterCondition<EditAdminUserViewModel, bool>(u => u.LockoutEnabled, "Статус аккаунта", "", "Заблокирован", "Активен"));

            return this;
        }
    }
}