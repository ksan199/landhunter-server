﻿using System.Web.Mvc;
using System.Web.Mvc.Html;
using LandHunter.ViewModels;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AdminUserGrid : GridModel<EditAdminUserViewModel>
    {
        public AdminUserGrid(HtmlHelper html)
        {
            Column.For(m => html.DeleteLinkWithAntiForgery(m.Id)).Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => html.EditLink(m.Id, m.UserName, "Edit", new [] { "modal-form" })).Sortable("UserName").Named("Имя пользователя").DoNotEncode();
            Column.For(m => m.Email).Named("Email");
            Column.For(m => m.Roles).Sortable(false).Named("Роли");
            Column.For(m => m.LockoutEnabled ? "<span class=\"label label-danger\">Заблокирован</span>" : "<span class=\"label label-success\">Активен</span>").Sortable("LockoutEnabled").Named("Статус аккаунта").DoNotEncode();
            Column.For(m => html.ActionLink("Сбросить пароль", "ResetPassword", "Security", new { area = "Admin", id = m.Id }, new { @class = "btn btn-default btn-xs modal-form" })).DoNotEncode();

            Column.For(m => m.UserName).Visible(false).Searchable("UserName");
            Column.For(m => m.Email).Visible(false).Searchable("Email");
        }
    }
}