﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class AdminUserGridOptions : GridOptions
    {
        public AdminUserGridOptions()
        {
            SearchStringPlaceholder = "Имя пользователя или e-mail";

            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "UserName", Direction = SortDirection.Ascending }
                };
        }
    }
}
