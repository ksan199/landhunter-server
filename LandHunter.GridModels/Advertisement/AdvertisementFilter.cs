﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Autofac;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AdvertisementFilter : Filter<Advertisement>
    {
        protected override Filter<Advertisement> Configure()
        {
            var statusesRepository = AutofacLifetimeScope.GetRepository<AdvertisementStatus, int>();
            var landCategoriesRepository = AutofacLifetimeScope.GetRepository<LandCategory, string>();
            var currenciesRepository = AutofacLifetimeScope.GetRepository<Currency, int>();

            var statuses = new List<SelectListItem>();
            statuses.AddRange(statusesRepository.GetQuery().OrderBy(s => s.SortOrder).ToSelectList(s => s.Id, s => s.Name));

            var landCategories = new List<SelectListItem>();
            landCategories.AddRange(landCategoriesRepository.GetQuery().OrderBy(c => c.Name).ToSelectList(c => c.Code, c => c.Name));

            var currencies = new List<SelectListItem>();
            currencies.AddRange(currenciesRepository.GetAll().ToSelectList(c => c.Id, c => c.ShortName));

            AddCondition(new SingleSelectFilterCondition<Advertisement, int>(a => a.AdvertisementStatusId, statuses));

            AddCondition(new NumericFilterCondition<Advertisement, decimal>(a => a.Price));

            AddCondition(new BoolFilterCondition<Advertisement, bool>(a => a.IsOnTop));

            AddCondition(new StringFilterCondition<Advertisement, string>(a => a.Utilization));

            if (landCategories.Count > 1)
                AddCondition(new SelectFilterCondition<Advertisement, string>(a => a.CategoryCode, landCategories));

            AddCondition(new BoolFilterCondition<Advertisement, bool>(a => a.GasPipeline, trueText: "Есть"));
            AddCondition(new BoolFilterCondition<Advertisement, bool>(a => a.WaterPipeline, trueText: "Есть"));
            AddCondition(new BoolFilterCondition<Advertisement, bool>(a => a.PowerLine, trueText: "Есть"));
            AddCondition(new BoolFilterCondition<Advertisement, bool>(a => a.Sand, trueText: "Есть"));

            AddCondition(new DateFilterCondition<Advertisement, DateTime>(a => a.CreateDate));

            AddCondition(new BoolFilterCondition<Advertisement, bool>(a => a.IsSoldOut));

            AddCondition(new CurrencyFilterCondition<Advertisement>(currencies));

            return this;
        }
    }
}