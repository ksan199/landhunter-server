﻿using System;
using System.Web.Mvc;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AdvertisementGrid : GridModel<Advertisement>
    {
        public AdvertisementGrid(HtmlHelper html)
        {
            Column.For(m => html.RowCheckbox(m.Id)).SelectAllCheckboxHeader().Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => html.EditLink(m.Id, new string[0])).Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => m.IsOnTop ? "<span title=\"Объявление находится в ТОПе\" class=\"glyphicon glyphicon-chevron-up\"></span>" : null).Sortable(false).DoNotEncode();
            Column.For(m => m.CadNum).Named("Кадастровый номер");
            Column.For(m => m.Area.HasValue ? m.Area.Value.ToString("F") + " м²" : null).Sortable("Area").Named("Площадь");
            Column.For(m => Math.Round(m.Price, 2)).Sortable("Price").Named("Цена");
            Column.For(m => m.AdvertisementStatus.Name).Sortable("AdvertisementStatus.Name").Named("Статус");
            Column.For(m => RealtorsHelper.GetNameFirstFullName(m.Realtor.Surname, m.Realtor.Name, m.Realtor.Patronymic)).Sortable("Realtor.Name").Named("Риелтор");
            Column.For(m => m.Utilization).Named("Вид использования");
            Column.For(m => m.LandCategory != null ? m.LandCategory.Name : null).Sortable("LandCategory.Name").Named("Категория");
            Column.For(m => m.GasPipeline ? "Есть" : "Нет").Sortable("GasPipeline").Named("Газовые сети");
            Column.For(m => m.WaterPipeline ? "Есть" : "Нет").Sortable("WaterPipeline").Named("Водопровод");
            Column.For(m => m.PowerLine ? "Есть" : "Нет").Sortable("PowerLine").Named("Электроснабжение");
            Column.For(m => m.Sand ? "Есть" : "Нет").Sortable("Sand").Named("Песок");
            Column.For(m => m.CreateDate.ToString("g")).Sortable("CreateDate").Named("Добавлено");

            Column.For(m => m.CadNum).Visible(false).Searchable("CadNum");
            Column.For(m => m.Realtor.Surname).Visible(false).Searchable("Realtor.Surname");
            Column.For(m => m.Realtor.Name).Visible(false).Searchable("Realtor.Name");
        }
    }
}