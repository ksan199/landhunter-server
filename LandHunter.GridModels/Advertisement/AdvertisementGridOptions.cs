﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class AdvertisementGridOptions : GridOptions
    {
        public AdvertisementGridOptions()
        {
            SearchStringPlaceholder = "Кадастровый номер, фамилия или имя риелтора";

            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "AdvertisementStatus.SortOrder", Direction = SortDirection.Ascending },
                    new GridSortOptions { Column = "CreateDate", Direction = SortDirection.Descending }
                };

            FilterConditionValues = new List<FilterConditionValue>
                {
                    new FilterConditionValue { Key = "__CurrencyId", Condition = "Equal", Values = new List<string> { "1" } }
                };
        }
    }
}
