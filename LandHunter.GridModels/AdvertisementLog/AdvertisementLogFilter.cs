﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using LandHunter.Models;
using SharpEngine.Core;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Autofac;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AdvertisementLogFilter : Filter<AdvertisementLog>
    {
        protected override Filter<AdvertisementLog> Configure()
        {
            var realtorsRepository = AutofacLifetimeScope.GetRepository<RealtorExtended, int>();

            var actions = EnumExtensions.ToList<LogActionType>().ToSelectList(t => t, t => t.GetDescription());

            var realtors = new List<SelectListItem>();
            realtors.AddRange(realtorsRepository.GetQuery()
                .Select(r => new { r.Id, r.FullName })
                .OrderBy(r => r.FullName)
                .ToSelectList(r => r.Id, r => r.FullName));

            AddCondition(new EnumFilterCondition<AdvertisementLog, LogActionType>(l => l.Action, actions));

            if (realtors.Count > 0)
                AddCondition(new SelectFilterCondition<AdvertisementLog, string>(l => l.UserId, realtors, "Риелтор"));

            AddCondition(new DateFilterCondition<AdvertisementLog, DateTime>(l => l.Date));

            return this;
        }
    }
}