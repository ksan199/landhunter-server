﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Core;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AdvertisementLogGrid : GridModel<AdvertisementLog>
    {
        public AdvertisementLogGrid(HtmlHelper html)
        {
            Column.For(m => m.Action.GetDescription()).Sortable("Action").Named("Действие");
            Column.For(m => m.AdvertisementId).Named("Объявление");
            Column.For(m => m.CadNum).Named("Кадастровый номер");
            Column.For(m => m.RealtorFullName).Named("Риелтор");
            Column.For(m => html.EditLink(m.Id, "Подробнее", new[] { "modal-form" })).Sortable(false).DoNotEncode();
            Column.For(m => m.Date).Named("Дата");

            Column.For(m => m.AdvertisementId).Visible(false).NumericSearchable("AdvertisementId");
            Column.For(m => m.CadNum).Visible(false).Searchable("CadNum");
        }
    }
}