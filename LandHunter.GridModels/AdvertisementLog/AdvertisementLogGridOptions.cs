﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class AdvertisementLogGridOptions : GridOptions
    {
        public AdvertisementLogGridOptions()
        {
            SearchStringPlaceholder = "Id объявления или кадастровый номер";

            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "Date", Direction = SortDirection.Descending }
                };
        }
    }
}
