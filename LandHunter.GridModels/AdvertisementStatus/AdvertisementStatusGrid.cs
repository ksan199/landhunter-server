﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AdvertisementStatusGrid : GridModel<AdvertisementStatus>
    {
        public AdvertisementStatusGrid(HtmlHelper html)
        {
            Column.For(m => m.Name).Sortable(false).Named("Название");
            Column.For(m => m.Key).Sortable(false).Named("Ключ");
        }
    }
}