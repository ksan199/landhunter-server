﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class AgencyGrid : GridModel<Agency>
    {
        public AgencyGrid(HtmlHelper html)
        {
            Column.For(m => html.RowCheckbox(m.Id)).SelectAllCheckboxHeader().Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => html.EditLink(m.Id, m.Name, new string[0])).Sortable("Name").Named("Название").DoNotEncode();
            Column.For(m => m.OGRN).Named("ОГРН");
            Column.For(m => m.Phone).Sortable(false).Named("Телефон");
            Column.For(m => m.Email).Sortable(false).Named("E-mail");
            Column.For(m => string.Format("<a href=\"{0}\" target=\"_blank\">{0}</a>", m.Website)).Sortable(false).Named("Сайт").DoNotEncode();

            Column.For(m => m.Name).Visible(false).Searchable("Name");
            Column.For(m => m.OGRN).Visible(false).Searchable("OGRN");
            Column.For(m => m.Email).Visible(false).Searchable("Email");
            Column.For(m => m.Phone).Visible(false).Searchable("Phone");
            Column.For(m => m.Website).Visible(false).Searchable("Website");
        }
    }
}