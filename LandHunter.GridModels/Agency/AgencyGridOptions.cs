﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class AgencyGridOptions : GridOptions
    {
        public AgencyGridOptions()
        {
            SearchStringPlaceholder = "Название, ОГРН, e-mail, телефон или сайт";

            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "Name", Direction = SortDirection.Ascending }
                };
        }
    }
}
