﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class CurrencyGrid : GridModel<Currency>
    {
        public CurrencyGrid(HtmlHelper html)
        {
            Column.For(m => m.Name).Named("Название");
            Column.For(m => m.ShortName).Named("Сокращение");
            Column.For(m => m.CharCode).Named("Символьный код");
            Column.For(m => m.NumCode).Named("Цифровой код");
        }
    }
}