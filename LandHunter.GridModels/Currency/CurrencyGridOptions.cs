﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class CurrencyGridOptions : GridOptions
    {
        public CurrencyGridOptions()
        {
            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "Name", Direction = SortDirection.Ascending }
                };
        }
    }
}
