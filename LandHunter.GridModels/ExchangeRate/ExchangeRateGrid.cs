﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class ExchangeRateGrid : GridModel<ExchangeRate>
    {
        public ExchangeRateGrid(HtmlHelper html)
        {
            Column.For(m => m.Currency.Name).Sortable("Currency.Name").Named("Валюта");
            Column.For(m => m.Nominal).Named("Номинал");
            Column.For(m => m.Value).Named("Курс в рублях");
            Column.For(m => m.Date.ToString("d")).Sortable("Date").Named("Дата");
        }
    }
}