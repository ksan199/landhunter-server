﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class ExchangeRateGridOptions : GridOptions
    {
        public ExchangeRateGridOptions()
        {
            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "Date", Direction = SortDirection.Descending },
                    new GridSortOptions { Column = "Currency.Name", Direction = SortDirection.Ascending }
                };
        }
    }
}
