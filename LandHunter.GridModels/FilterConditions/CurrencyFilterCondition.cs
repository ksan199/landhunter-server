﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class CurrencyFilterCondition<TEntity> : CurrencyFilterCondition, IFilterCondition<TEntity>
        where TEntity : class
    {
        public CurrencyFilterCondition(IEnumerable<SelectListItem> dictionary)
            : base(dictionary)
        {
        }

        public IQueryable<TEntity> Apply(IQueryable<TEntity> query)
        {
            return query;
        }
    }

    public class CurrencyFilterCondition : IFilterCondition
    {
        public string Key { get; private set; }
        public string Caption { get; private set; }
        public FilterConditionValue Value { get; set; }
        public IEnumerable<SelectListItem> Dictionary { get; private set; }

        public CurrencyFilterCondition(IEnumerable<SelectListItem> dictionary)
        {
            Key = "__CurrencyId";
            Caption = "Валюта";
            Dictionary = dictionary;
        }

        public string GetInputName(int index, string field)
        {
            return string.Format("FilterConditionValues[{0}].{1}", index, field);
        }

        public string GetInputId(int index, string field)
        {
            return string.Format("FilterConditionValues{0}_{1}", index, field);
        }
    }
}