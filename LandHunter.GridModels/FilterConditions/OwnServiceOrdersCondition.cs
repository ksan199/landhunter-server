﻿using System.Linq;
using System.Web;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class OwnServiceOrdersCondition : IFilterCondition<ServiceOrder>
    {
        public string Key { get; private set; }
        public string Caption { get; private set; }
        public FilterConditionValue Value { get; set; }

        public OwnServiceOrdersCondition()
        {
            Key = "__Own";
            Caption = "Показывать только свои заказы";
        }

        public IQueryable<ServiceOrder> Apply(IQueryable<ServiceOrder> query)
        {
            if (Value == null)
                return query;

            var condition = Value.Condition;

            if (condition != null && condition == "true")
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();

                query = query.Where(o => o.ModeratorId == currentUserId);
            }

            return query;
        }

        public string GetInputName(int index, string field)
        {
            return string.Format("FilterConditionValues[{0}].{1}", index, field);
        }

        public string GetInputId(int index, string field)
        {
            return string.Format("FilterConditionValues{0}_{1}", index, field);
        }
    }
}
