﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LandHunter.ViewModels;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class RoleFilterCondition : IFilterCondition<EditAdminUserViewModel>
    {
        public string Key { get; private set; }
        public string Caption { get; private set; }
        public FilterConditionValue Value { get; set; }
        public IEnumerable<SelectListItem> Dictionary { get; private set; }

        public RoleFilterCondition(IEnumerable<SelectListItem> dictionary)
        {
            Key = "__RoleId";
            Caption = "Роль";
            Dictionary = dictionary;
        }

        public IQueryable<EditAdminUserViewModel> Apply(IQueryable<EditAdminUserViewModel> query)
        {
            if (Value == null)
                return query;

            var values = Value.Values;

            if (values != null && values.Any())
                query = query.Where(u => u.RolesIds.Any(values.Contains));

            return query;
        }

        public string GetInputName(int index, string field)
        {
            return string.Format("FilterConditionValues[{0}].{1}", index, field);
        }

        public string GetInputId(int index, string field)
        {
            return string.Format("FilterConditionValues{0}_{1}", index, field);
        }
    }
}
