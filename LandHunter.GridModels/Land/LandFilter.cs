﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Autofac;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class LandFilter : Filter<LandExtendedView>
    {
        protected override Filter<LandExtendedView> Configure()
        {
            var landCategoriesRepository = AutofacLifetimeScope.GetRepository<LandCategory, string>();
            var currenciesRepository = AutofacLifetimeScope.GetRepository<Currency, int>();

            var landCategories = new List<SelectListItem>();
            landCategories.AddRange(landCategoriesRepository.GetQuery().OrderBy(c => c.Name).ToSelectList(c => c.Code, c => c.Name));

            var currencies = new List<SelectListItem>();
            currencies.AddRange(currenciesRepository.GetAll().ToSelectList(c => c.Id, c => c.ShortName));

            AddCondition(new StringFilterCondition<LandExtendedView, string>(l => l.Utilization));

            AddCondition(new SelectFilterCondition<LandExtendedView, string>(l => l.CategoryCode, landCategories));

            AddCondition(new BoolFilterCondition<LandExtendedView, bool>(l => l.GasPipeline, trueText: "Есть"));
            AddCondition(new BoolFilterCondition<LandExtendedView, bool>(l => l.WaterPipeline, trueText: "Есть"));
            AddCondition(new BoolFilterCondition<LandExtendedView, bool>(l => l.PowerLine, trueText: "Есть"));
            AddCondition(new BoolFilterCondition<LandExtendedView, bool>(l => l.Sand, trueText: "Есть"));

            AddCondition(new DateFilterCondition<LandExtendedView, DateTime>(l => l.CreateDate));

            AddCondition(new CurrencyFilterCondition<LandExtendedView>(currencies));

            return this;
        }
    }
}