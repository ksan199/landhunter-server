﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class LandGrid : GridModel<LandExtendedView>
    {
        public LandGrid(HtmlHelper html)
        {
            Column.For(m => html.RowCheckbox(m.CadNum)).SelectAllCheckboxHeader().Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => html.EditLink(m.CadNum.Replace(':', '_'), m.CadNum, new string[0])).Sortable("CadNum").Named("Кадастровый номер").DoNotEncode();
            Column.For(m => m.Utilization).Named("Вид использования");
            Column.For(m => m.LandCategory != null ? m.LandCategory.Name : null).Sortable("LandCategory.Name").Named("Категория");
            Column.For(m => m.Area.ToString("F") + " м²").Sortable("Area").Named("Площадь");
            Column.For(m => GetAdvertisementsCountersHtml(m)).Sortable(false).Named("Объявления").DoNotEncode();
            Column.For(m => m.MinRubPrice.HasValue ? Math.Round(m.MinRubPrice.Value, 2) : (decimal?)null).Sortable("MinRubPrice").Named("Мин. цена");
            Column.For(m => m.GasPipeline ? "Есть" : "Нет").Sortable("GasPipeline").Named("Газовые сети");
            Column.For(m => m.WaterPipeline ? "Есть" : "Нет").Sortable("WaterPipeline").Named("Водопровод");
            Column.For(m => m.PowerLine ? "Есть" : "Нет").Sortable("PowerLine").Named("Электроснабжение");
            Column.For(m => m.Sand ? "Есть" : "Нет").Sortable("Sand").Named("Песок");
            Column.For(m => m.CreateDate.ToString("g")).Sortable("CreateDate").Named("Добавлен");

            Column.For(m => m.CadNum).Visible(false).Searchable("CadNum");
            Column.For(m => m.Address).Visible(false).Searchable("Address");
        }

        private string GetAdvertisementsCountersHtml(LandExtendedView land)
        {
            var newAdvertisements = land.Advertisements.Count(a => !a.IsDeleted && !a.IsSoldOut && a.AdvertisementStatus.Key == AdvertisementStatusKey.New);
            var modifiedAdvertisements = land.Advertisements.Count(a => !a.IsDeleted && !a.IsSoldOut && a.AdvertisementStatus.Key == AdvertisementStatusKey.Modified);

            var html = new StringBuilder("<span title=\"Всего\">" + land.Advertisements.Count + "</span>");

            if (newAdvertisements > 0)
                html.Append("&nbsp;<span title=\"Новых\" class=\"label label-success\">+ " + newAdvertisements + "</span>");

            if (modifiedAdvertisements > 0)
                html.Append("&nbsp;<span title=\"Измененных\" class=\"label label-warning\">+ " + modifiedAdvertisements + "</span>");

            return html.ToString();
        }
    }
}