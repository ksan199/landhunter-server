﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class LandGridOptions : GridOptions
    {
        public LandGridOptions()
        {
            SearchStringPlaceholder = "Кадастровый номер или адрес";

            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "CadNum", Direction = SortDirection.Ascending }
                };

            FilterConditionValues = new List<FilterConditionValue>
                {
                    new FilterConditionValue { Key = "__CurrencyId", Condition = "Equal", Values = new List<string> { "1" } }
                };
        }
    }
}