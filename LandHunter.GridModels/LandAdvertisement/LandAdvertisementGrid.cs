﻿using System.Web.Mvc;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class LandAdvertisementGrid : GridModel<Advertisement>
    {
        public LandAdvertisementGrid(HtmlHelper html)
        {
            Column.For(m => html.EditLink(m.Id, new string[0])).Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column
                .For(m => html.ActionLink(RealtorsHelper.GetNameFirstFullName(m.Realtor.Surname, m.Realtor.Name, m.Realtor.Patronymic), "Edit", "Realtors", new { id = m.RealtorId }, null, false))
                .Sortable("Realtor.Name").Named("Риелтор").DoNotEncode();
            Column.For(m => m.AdvertisementStatus.Name).Sortable("AdvertisementStatus.SortOrder").Named("Статус");
            Column.For(m => m.Utilization).Named("Вид использования");
            Column.For(m => m.LandCategory != null ? m.LandCategory.Name : null).Sortable("LandCategory.Name").Named("Категория");
            Column.For(m => m.Area.HasValue ? m.Area.Value.ToString("F") + " м²" : null).Sortable("Area").Named("Площадь");
            Column.For(m => m.Price + " " + m.Currency.ShortName).Sortable("Price").Named("Цена");
            Column.For(m => m.GasPipeline ? "Есть" : "Нет").Sortable("GasPipeline").Named("Газовые сети");
            Column.For(m => m.WaterPipeline ? "Есть" : "Нет").Sortable("WaterPipeline").Named("Водопровод");
            Column.For(m => m.PowerLine ? "Есть" : "Нет").Sortable("PowerLine").Named("Электроснабжение");
            Column.For(m => m.Sand ? "Есть" : "Нет").Sortable("Sand").Named("Песок");
            Column.For(m => m.CreateDate.ToString("g")).Sortable("CreateDate").Named("Добавлено");
        }
    }
}