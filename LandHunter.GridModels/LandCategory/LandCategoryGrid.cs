﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class LandCategoryGrid : GridModel<LandCategory>
    {
        public LandCategoryGrid(HtmlHelper html)
        {
            Column.For(m => m.Code).Named("Код");
            Column.For(m => m.Name).Named("Название");
            Column.For(m => m.ShortName).Named("Сокращение");
        }
    }
}