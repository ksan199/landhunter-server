﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class ModeratorServiceGrid : GridModel<ModeratorService>
    {
        public ModeratorServiceGrid(HtmlHelper html)
        {
            Column.For(m => html.DeleteLinkWithAntiForgery(m.Id)).Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => m.Moderator.UserName).Sortable(false).Named("Имя пользователя");
        }
    }
}