﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class ModeratorServiceGridOptions : GridOptions
    {
        public ModeratorServiceGridOptions()
        {
            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "Moderator.UserName", Direction = SortDirection.Ascending }
                };
        }
    }
}