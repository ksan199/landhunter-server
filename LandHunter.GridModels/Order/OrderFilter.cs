﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Autofac;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class OrderFilter : Filter<Order>
    {
        protected override Filter<Order> Configure()
        {
            var statusesRepository = AutofacLifetimeScope.GetRepository<OrderStatus, int>();
            var typesRepository = AutofacLifetimeScope.GetRepository<OrderType, int>();

            var statuses = new List<SelectListItem>();
            statuses.AddRange(statusesRepository.GetQuery().OrderBy(s => s.SortOrder).ToSelectList(s => s.Id, s => s.Name));

            var types = new List<SelectListItem>();
            types.AddRange(typesRepository.GetAll().ToSelectList(t => t.Id, t => t.Name));

            if (types.Count > 1)
                AddCondition(new SelectFilterCondition<Order, int>(o => o.OrderTypeId, types));

            if (statuses.Count > 1)
                AddCondition(new SelectFilterCondition<Order, int>(o => o.OrderStatusId, statuses));

            AddCondition(new DateFilterCondition<Order, DateTime>(o => o.CreateDate));

            AddCondition(new DateFilterCondition<Order, DateTime>(o => o.UpdateDate));

            return this;
        }
    }
}