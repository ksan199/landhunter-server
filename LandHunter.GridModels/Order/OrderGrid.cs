﻿using System.Web.Mvc;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class OrderGrid : GridModel<Order>
    {
        public OrderGrid(HtmlHelper html)
        {
            Column.For(m => html.EditLink(m.Id, m.Id.ToString(), new string[0])).Sortable("Id").Named("Номер").DoNotEncode();
            Column.For(m => RealtorsHelper.GetNameFirstFullName(m.Realtor.Surname, m.Realtor.Name, m.Realtor.Patronymic)).Sortable("Realtor.Name").Named("Риелтор");
            Column.For(m => m.OrderType.Name).Sortable("OrderType.Name").Named("Тип");
            Column.For(m => m.OrderStatus.Name).Sortable("OrderStatus.Name").Named("Статус");
            Column.For(m => m.OrderSumAmount).Named("Сумма");
            Column.For(m => m.BalanceBefore).Named("Баланс до");
            Column.For(m => m.CreateDate.ToString("g")).Sortable("CreateDate").Named("Добавлено");
            Column.For(m => m.UpdateDate.ToString("g")).Sortable("UpdateDate").Named("Обновлено");

            Column.For(m => m.Id).Visible(false).NumericSearchable("Id");
            Column.For(m => m.Realtor.Surname).Visible(false).Searchable("Realtor.Surname");
            Column.For(m => m.Realtor.Name).Visible(false).Searchable("Realtor.Name");
        }
    }
}