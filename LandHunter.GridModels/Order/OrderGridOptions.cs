﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class OrderGridOptions : GridOptions
    {
        public OrderGridOptions()
        {
            SearchStringPlaceholder = "Номер операции, фамилия или имя риелтора";

            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "UpdateDate", Direction = SortDirection.Descending }
                };
        }
    }
}
