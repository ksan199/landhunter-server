﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class OrderStatusGridOptions : GridOptions
    {
        public OrderStatusGridOptions()
        {
            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "SortOrder", Direction = SortDirection.Ascending }
                };
        }
    }
}
