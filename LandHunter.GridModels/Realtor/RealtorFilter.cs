﻿using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class RealtorFilter : Filter<Realtor>
    {
        protected override Filter<Realtor> Configure()
        {
            AddCondition(new BoolFilterCondition<Realtor, bool>(u => u.User.LockoutEnabled, "Статус аккаунта", "", "Заблокирован", "Активен"));

            return this;
        }
    }
}