﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class RealtorGrid : GridModel<Realtor>
    {
        public RealtorGrid(HtmlHelper html)
        {
            Column.For(m => html.DeleteLinkWithAntiForgery(m.Id)).Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => html.EditLink(m.Id, RealtorsHelper.GetNameFirstFullName(m.Surname, m.Name, m.Patronymic), "Edit", new string[0])).Sortable("Name").Named("ФИО").DoNotEncode();
            Column.For(m => m.User.Email).Sortable("User.Email").Named("E-mail");
            Column.For(m => m.User.PhoneNumber).Sortable(false).Named("Телефон");
            Column.For(m => m.Advertisements.Count(a => !a.IsDeleted && !a.IsSoldOut)).Sortable(false).Named("Предложений");
            Column.For(m => m.Rating).Named("Рейтинг");
            Column.For(m => m.Balance).Named("Баланс");
            Column.For(m => m.User.LockoutEnabled ? "<span class=\"label label-danger\">Заблокирован</span>" : "<span class=\"label label-success\">Активен</span>").Sortable("User.LockoutEnabled").Named("Статус аккаунта").DoNotEncode();
            Column.For(m => html.ActionLink("Сбросить пароль", "ResetPassword", "Security", new { area = "Admin", id = m.Id }, new { @class = "btn btn-default btn-xs modal-form" })).DoNotEncode();

            Column.For(m => m.Surname).Visible(false).Searchable("Surname");
            Column.For(m => m.Name).Visible(false).Searchable("Name");
            Column.For(m => m.User.Email).Visible(false).Searchable("User.Email");
            Column.For(m => m.User.PhoneNumber).Visible(false).Searchable("User.PhoneNumber");
        }
    }
}