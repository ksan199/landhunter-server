﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class RegionGrid : GridModel<Region>
    {
        public RegionGrid(HtmlHelper html)
        {
            Column.For(m => m.Id).Named("Код ФНС");
            Column.For(m => m.Name).Named("Название");
            Column.For(m => m.CianId).Named("ID на ЦИАН");
        }
    }
}