﻿using System;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class RegionGridOptions : GridOptions
    {
        public RegionGridOptions()
        {
            PageSize = int.MaxValue;
        }
    }
}