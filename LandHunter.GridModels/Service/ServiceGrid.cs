﻿using System.Linq;
using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class ServiceGrid : GridModel<Service>
    {
        public ServiceGrid(HtmlHelper html)
        {
            Column.For(m => m.ServiceCategory.Name).Sortable(false).Named("Категория");
            Column.For(m => html.EditLink(m.Id, m.Name, "Edit", new string[0])).Sortable(false).Named("Название").DoNotEncode();
            Column.For(m => m.Price.HasValue ? m.Price.Value.ToString("F") : "бесплатная").Sortable(false).Named("Стоимость");
            Column.For(m => m.ModeratorsService.Any()
                ? m.ModeratorsService.Select(ms => ms.Moderator.UserName).OrderBy(n => n).Aggregate((n1, n2) => n1 + ", " + n2)
                : null).Sortable(false).Named("Операторы");
        }
    }
}