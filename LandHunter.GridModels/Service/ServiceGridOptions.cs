﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class ServiceGridOptions : GridOptions
    {
        public ServiceGridOptions()
        {
            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "ServiceCategory.SortOrder", Direction = SortDirection.Ascending },
                    new GridSortOptions { Column = "SortOrder", Direction = SortDirection.Ascending }
                };
        }
    }
}
