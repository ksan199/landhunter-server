﻿using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class ServiceCategoryGrid : GridModel<ServiceCategory>
    {
        public ServiceCategoryGrid(HtmlHelper html)
        {
            Column.For(m => m.Name).Sortable(false).Named("Название");
        }
    }
}