﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using LandHunter.Models;
using SharpEngine.Core;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Autofac;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class ServiceOrderFilter : Filter<ServiceOrder>
    {
        protected override Filter<ServiceOrder> Configure()
        {
            var servicesRepository = AutofacLifetimeScope.GetRepository<Service, int>();

            var services = new List<SelectListItem>();
            services.AddRange(servicesRepository.GetQuery()
                .OrderBy(s => s.ServiceCategory.SortOrder)
                .ThenBy(s => s.SortOrder)
                .Select(s => new { s.Id, s.Name, Category = s.ServiceCategory.Name })
                .ToList()
                .ToSelectList(s => s.Id, s => s.Category + " - " + s.Name));

            var statuses = new List<SelectListItem>();
            statuses.AddRange(Enum.GetValues(typeof(ServiceOrderStatus)).Cast<ServiceOrderStatus>().ToSelectList(s => s, s => s.GetDescription()));


            if (HttpContext.Current.User.IsInRole("SuperAdministrator"))
            {
                var moderatorsServicesRepository = AutofacLifetimeScope.GetRepository<ModeratorService, int>();

                var moderators = new List<SelectListItem>();

                moderators.AddRange(moderatorsServicesRepository.GetQuery()
                    .Select(ms => new { ms.ModeratorId, ms.Moderator.UserName })
                    .Distinct()
                    .ToList()
                    .OrderBy(m => m.UserName)
                    .ToSelectList(m => m.ModeratorId, m => m.UserName));

                if (moderators.Count > 1)
                    AddCondition(new SelectFilterCondition<ServiceOrder, string>(o => o.ModeratorId, moderators));
            }

            if (services.Count > 1)
                AddCondition(new SelectFilterCondition<ServiceOrder, int>(o => o.ServiceId, services, "Услуга"));

            AddCondition(new EnumFilterCondition<ServiceOrder, ServiceOrderStatus>(o => o.Status, statuses));

            AddCondition(new DateFilterCondition<ServiceOrder, DateTime>(o => o.CreateDate));

            AddCondition(new DateFilterCondition<ServiceOrder, DateTime>(o => o.UpdateDate));

            if (HttpContext.Current.User.IsInRole("Moderator"))
                AddCondition(new OwnServiceOrdersCondition());

            return this;
        }
    }
}