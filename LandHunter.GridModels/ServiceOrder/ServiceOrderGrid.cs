﻿using System.Web.Mvc;
using LandHunter.Common.Enums;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Core;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class ServiceOrderGrid : GridModel<ServiceOrder>
    {
        public ServiceOrderGrid(HtmlHelper html)
        {
            Column.For(m => html.EditLink(m.Id, m.Id.ToString(), new string[0])).Sortable("Id").Named("Номер").DoNotEncode();
            Column.For(m => m.ServiceName).Sortable("Service.Name").Named("Услуга");
            Column.For(m => m.Realtor).Sortable(false).Named("Риелтор");
            Column.For(m => m.RosreestrNumber).Sortable(false).Named("Номер в Росреестре");
            Column.For(m => m.ReceptionCode).Sortable(false).Named("Код получения");
            Column.For(m => GetReceptionTypeColumn(m, html)).Sortable("ReceptionType").Named("Тип получения").DoNotEncode();
            Column.For(m => m.Status.GetDescription()).Sortable("Status").Named("Статус");
            Column.For(m => GetModeratorColumn(m, html)).Sortable("Moderator.UserName").Named("Оператор").DoNotEncode();
            Column.For(m => m.CreateDate.ToString("g")).Sortable("CreateDate").Named("Добавлено");
            Column.For(m => m.UpdateDate.ToString("g")).Sortable("UpdateDate").Named("Обновлено");
        }

        private string GetReceptionTypeColumn(ServiceOrder order, HtmlHelper html)
        {
            var user = html.ViewContext.RequestContext.HttpContext.User;

            if (order.ReceptionType == ReceptionType.Email && order.ModeratorId == user.Identity.GetUserId())
            {
                return html.ActionLink(order.ReceptionType.GetDescription(), "SendEmail", "ServiceOrders", new { id = order.Id }, new { @class = "btn btn-xs btn-info modal-form", title = "Отправить результат по электронной почте" }, false);
            }
            else
            {
                return order.ReceptionType.GetDescription();
            }
        }

        private string GetModeratorColumn(ServiceOrder order, HtmlHelper html)
        {
            var user = html.ViewContext.RequestContext.HttpContext.User;

            if (user.IsInRole("Moderator") && order.ModeratorId == null && order.Status != ServiceOrderStatus.Suspended)
            {
                return html.ActionLink("Взять", "Take", "ServiceOrders", new { id = order.Id }, new { @class = "btn btn-xs btn-success take-btn" }, false);
            }
            else if (order.ModeratorId == user.Identity.GetUserId())
            {
                return "<b>" + order.ModeratorName + "</b>";
            }
            else
            {
                return order.ModeratorName;
            }
        }
    }
}