﻿using System;
using System.Collections.Generic;
using SharpEngine.Core.Sorting;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    [Serializable]
    public class ServiceOrderGridOptions : GridOptions
    {
        public ServiceOrderGridOptions()
        {
            SearchStringPlaceholder = "Номер заказа";

            SortOptions = new List<GridSortOptions>
                {
                    new GridSortOptions { Column = "UpdateDate", Direction = SortDirection.Descending }
                };
        }
    }
}
