﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.GridModels
{
    public class SitePageGrid : GridModel<SitePage>
    {
        public SitePageGrid(HtmlHelper html)
        {
            var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

            Attributes(new Dictionary<string, object> { { "data-table-sortable", "true" }, { "data-url-sortable", url.Action("Sort") } });
            Sections.RowAttributes(r => new Dictionary<string, object> { { "data-item-id", r.Item.Id } });

            Column.For(m => html.RowCheckbox(m.Id)).SelectAllCheckboxHeader().Attributes(@class => "mvcgrid-options-column").DoNotEncode();
            Column.For(m => html.DragHandleImage()).Attributes(@class => "drag-handle mvcgrid-options-column").DoNotEncode();
            Column.For(m => html.EditLink(m.Id, m.Title, new string[0])).Sortable(false).Named("Название").DoNotEncode();
            Column.For(m => m.Url).Sortable(false).Named("Адрес");
            Column.For(m => m.ShowInMenu ? "Да" : "Нет").Sortable(false).Named("Показывать в меню");
        }
    }
}