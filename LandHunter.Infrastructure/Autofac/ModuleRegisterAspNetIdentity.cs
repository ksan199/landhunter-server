﻿using System.Web.Mvc;
using Autofac;
using LandHunter.AspNetIdentity.Services;
using LandHunter.DataAccess.Context;
using LandHunter.AspNetIdentity;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SharpEngine.Web.Mvc.AspNetIdentity;

namespace LandHunter.Infrastructure.Autofac
{
    public class ModuleRegisterAspNetIdentity : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<UserStore<ApplicationUser>>(c => new UserStore<ApplicationUser>(DependencyResolver.Current.GetService<LandHunterContext>())).AsImplementedInterfaces();
            builder.Register<IdentityFactoryOptions<ApplicationUserManager>>(c => new IdentityFactoryOptions<ApplicationUserManager> { DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("LandHunter") }).AsSelf();
            builder.RegisterType<SmsService>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            builder.Register<RoleManager<IdentityRole>>(c => new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(DependencyResolver.Current.GetService<LandHunterContext>()))).AsSelf().InstancePerRequest();

            builder.RegisterGeneric(typeof(AspNetIdentity<,,,>)).As(typeof(IAspNetIdentity<,,,>)).InstancePerRequest();
        }
    }
}