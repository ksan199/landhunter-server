﻿using Autofac;
using LandHunter.DataAccess.Context;

namespace LandHunter.Infrastructure.Autofac
{
    public class ModuleRegisterContext : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LandHunterContext>().AsSelf().InstancePerRequest();
        }
    }
}