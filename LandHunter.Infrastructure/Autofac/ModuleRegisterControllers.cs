﻿using Autofac;
using LandHunter.Controllers;

namespace LandHunter.Infrastructure.Autofac
{
    public class ModuleRegisterControllers : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(SecurityController).Assembly);
        }
    }
}