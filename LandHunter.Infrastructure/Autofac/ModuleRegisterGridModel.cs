﻿using Autofac;
using LandHunter.GridModels;

namespace LandHunter.Infrastructure.Autofac
{
    public class ModuleRegisterGridModel : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(SitePageGrid).Assembly);
        }
    }
}