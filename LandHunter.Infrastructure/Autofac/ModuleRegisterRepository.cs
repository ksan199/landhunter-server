﻿using System.Web.Mvc;
using Autofac;
using LandHunter.DataAccess.Context;
using LandHunter.DataAccess.Repository;
using LandHunter.Models;
using SharpEngine.Repository;

namespace LandHunter.Infrastructure.Autofac
{
    public class ModuleRegisterRepository : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(LandHunterRepository<,>)).As(typeof(IRepository<,>)).InstancePerRequest();

            builder.Register(c =>
                    new LandHunterRepository<Land, string>(DependencyResolver.Current.GetService<LandHunterContext>(), "CadNum"))
                .As<IRepository<Land, string>>().InstancePerRequest();

            builder.Register(c =>
                    new LandHunterRepository<LandExtendedView, string>(DependencyResolver.Current.GetService<LandHunterContext>(), "CadNum"))
                .As<IRepository<LandExtendedView, string>>().InstancePerRequest();

            builder.Register(c =>
                    new LandHunterRepository<LandCategory, string>(DependencyResolver.Current.GetService<LandHunterContext>(), "Code"))
                .As<IRepository<LandCategory, string>>().InstancePerRequest();
        }
    }
}