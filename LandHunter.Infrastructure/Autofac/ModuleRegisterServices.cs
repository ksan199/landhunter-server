﻿using Autofac;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services;
using LandHunter.Services.CBRSoapService;
using SharpEngine.Web.Mvc.Services;
using SmsRu;

namespace LandHunter.Infrastructure.Autofac
{
    public class ModuleRegisterServices : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Base services

            builder.RegisterType<BaseService<Region, int, RegionGrid, RegionGridOptions>>().As<IBaseService<Region, int, RegionGrid, RegionGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<LandCategory, string, LandCategoryGrid, LandCategoryGridOptions>>().As<IBaseService<LandCategory, string, LandCategoryGrid, LandCategoryGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<SitePage, int, SitePageGrid, SitePageGridOptions>>().As<IBaseService<SitePage, int, SitePageGrid, SitePageGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<Currency, int, CurrencyGrid, CurrencyGridOptions>>().As<IBaseService<Currency, int, CurrencyGrid, CurrencyGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<AdvertisementStatus, int, AdvertisementStatusGrid, AdvertisementStatusGridOptions>>().As<IBaseService<AdvertisementStatus, int, AdvertisementStatusGrid, AdvertisementStatusGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<OrderStatus, int, OrderStatusGrid, OrderStatusGridOptions>>().As<IBaseService<OrderStatus, int, OrderStatusGrid, OrderStatusGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<OrderType, int, OrderTypeGrid, OrderTypeGridOptions>>().As<IBaseService<OrderType, int, OrderTypeGrid, OrderTypeGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<ServiceCategory, int, ServiceCategoryGrid, ServiceCategoryGridOptions>>().As<IBaseService<ServiceCategory, int, ServiceCategoryGrid, ServiceCategoryGridOptions>>().InstancePerRequest();
            builder.RegisterType<BaseService<Service, int, ServiceGrid, ServiceGridOptions>>().As<IBaseService<Service, int, ServiceGrid, ServiceGridOptions>>().InstancePerRequest();
            
            builder.RegisterType<ExchangeRatesService>().As<IBaseService<ExchangeRate, int, ExchangeRateGrid, ExchangeRateGridOptions>>().InstancePerRequest();
            builder.RegisterType<AgenciesBaseService>().As<IBaseService<Agency, int, AgencyGrid, AgencyGridOptions>>().InstancePerRequest();
            builder.RegisterType<ModeratorsServicesBaseService>().As<IBaseService<ModeratorService, int, ModeratorServiceGrid, ModeratorServiceGridOptions>>().InstancePerRequest();
            
            builder.RegisterType<RealtorsBaseService>().As<IFilterableBaseService<Realtor, string, RealtorGrid, RealtorGridOptions, RealtorFilter>>().InstancePerRequest();
            builder.RegisterType<AdvertisementsBaseService>().As<IFilterableBaseService<Advertisement, int, AdvertisementGrid, AdvertisementGridOptions, AdvertisementFilter>>().InstancePerRequest();
            builder.RegisterType<AdvertisementsLogsBaseService>().As<IFilterableBaseService<AdvertisementLog, int, AdvertisementLogGrid, AdvertisementLogGridOptions, AdvertisementLogFilter>>().InstancePerRequest();
            builder.RegisterType<OrdersBaseService>().As<IFilterableBaseService<Order, int, OrderGrid, OrderGridOptions,OrderFilter>>().InstancePerRequest();
            builder.RegisterType<ServiceOrdersBaseService>().As<IFilterableBaseService<ServiceOrder, int, ServiceOrderGrid, ServiceOrderGridOptions, ServiceOrderFilter>>().InstancePerRequest();

            // Custom services

            builder.RegisterType<AdminUsersService>().As<IAdminUsersService>().InstancePerRequest();
            builder.RegisterType<RealtorsBaseService>().As<IRealtorsBaseService>().InstancePerRequest();
            builder.RegisterType<ExchangeRatesService>().As<IExchangeRatesService>().InstancePerRequest();
            builder.RegisterType<LandsBaseService>().As<ILandsBaseService>().InstancePerRequest();
            builder.RegisterType<AdvertisementsBaseService>().As<IAdvertisementsBaseService>().InstancePerRequest();
            builder.RegisterType<FilesService>().As<IFilesService>().InstancePerRequest();
            builder.RegisterType<SettingsService>().As<ISettingsService>().InstancePerRequest();
            builder.RegisterType<RatingService>().As<IRatingService>().InstancePerRequest();
            builder.RegisterType<ModeratorsServicesBaseService>().As<IModeratorsServicesBaseService>().InstancePerRequest();
            builder.RegisterType<ServiceOrdersBaseService>().As<IServiceOrdersBaseService>().InstancePerRequest();
            builder.RegisterType<MailService>().As<IMailService>().InstancePerRequest();

            // Api services

            builder.RegisterType<SecurityService>().As<ISecurityService>().InstancePerRequest();
            builder.RegisterType<LandsService>().As<ILandsService>().InstancePerRequest();
            builder.RegisterType<SitePagesService>().As<ISitePagesService>().InstancePerRequest();
            builder.RegisterType<CurrenciesService>().As<ICurrenciesService>().InstancePerRequest();
            builder.RegisterType<ClustersService>().As<IClustersService>().InstancePerRequest();
            builder.RegisterType<DictionariesService>().As<IDictionariesService>().InstancePerRequest();
            builder.RegisterType<AdvertisementsService>().As<IAdvertisementsService>().InstancePerRequest();
            builder.RegisterType<RealtorsService>().As<IRealtorsService>().InstancePerRequest();
            builder.RegisterType<AgenciesService>().As<IAgenciesService>().InstancePerRequest();
            builder.RegisterType<OrdersService>().As<IOrdersService>().InstancePerRequest();
            builder.RegisterType<ServicesService>().As<IServicesService>().InstancePerRequest();

            builder.RegisterType<LandsGridService>().As<IGridService<Land>>().InstancePerRequest();
            builder.RegisterType<AdvertisementsGridService>().As<IGridService<AdvertisementExtended>>().InstancePerRequest();
            builder.RegisterType<RealtorsGridService>().As<IGridService<RealtorExtended>>().InstancePerRequest();


            builder.RegisterType<ArcgisService>().As<IArcgisService>().InstancePerRequest();

            // External services

            builder.RegisterType<DailyInfoSoapClient>().As<DailyInfoSoap>().InstancePerRequest();
            builder.RegisterType<SmsRuProvider>().As<ISmsProvider>().InstancePerRequest();
            builder.RegisterType<ExternalService>().As<IExternalService>().InstancePerRequest();
        }
    }
}