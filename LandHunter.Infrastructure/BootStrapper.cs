﻿using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using LandHunter.Infrastructure.Autofac;
using SharpEngine.Web.Mvc.Autofac;

namespace LandHunter.Infrastructure
{
    public static class BootStrapper
    {
        public static void ConfigureApplication()
        {
            SetupDependencyInjection();
        }

        private static void SetupDependencyInjection()
        {
            Module[] modules =
                {
                    new ModuleRegisterContext(),
                    new ModuleRegisterRepository(),
                    new ModuleRegisterAspNetIdentity(),
                    new ModuleRegisterGridModel(),
                    new ModuleRegisterServices(),
                    new ModuleRegisterControllers()
                };

            var container = new AutofacModulesResolver(modules).Container;

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}