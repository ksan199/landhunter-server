using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SharpEngine.Web.Mvc.Attributes;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Advertisement : EntityBase<int>
    {
        public Advertisement()
        {
            Files = new HashSet<File>();
        }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(128, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [HiddenInput(DisplayValue = false)]
        public string RealtorId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "�������")]
        public string RealtorFullName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [DropDownList("StatusesDictionary")]
        [Display(Name = "������ ����������")]
        public int AdvertisementStatusId { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public IEnumerable<SelectListItem> StatusesDictionary { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [Display(Name = "����", Description = "�� ���� ����������.")]
        public decimal Price { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [DropDownList("CurrenciesDictionary")]
        [Display(Name = "������")]
        public int CurrencyId { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public IEnumerable<SelectListItem> CurrenciesDictionary { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [UIHint("Text")]
        [Display(Name = "����������� ����� �������")]
        public string CadNum { get; set; }

        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "�����")]
        public string Address { get; set; }

        [UIHint("TextArea")]
        [Display(Name = "��������")]
        public string Description { get; set; }

        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "��� �������������")]
        public string Utilization { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [DropDownList("CategoriesDictionary")]
        [Display(Name = "���������")]
        public string CategoryCode { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public IEnumerable<SelectListItem> CategoriesDictionary { get; set; }

        [Display(Name = "�������", Description = "� ������ ����������.")]
        public decimal? Area { get; set; }

        [Display(Name = "������� ����")]
        public bool GasPipeline { get; set; }

        [Display(Name = "����������")]
        public bool WaterPipeline { get; set; }

        [Display(Name = "����������������")]
        public bool PowerLine { get; set; }

        [Display(Name = "�����")]
        public bool Sand { get; set; }

        [UIHint("Text")]
        [Display(Name = "���������")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "���������� ���� ����������")]
        public bool ShowCreateDate { get; set; }

        [Display(Name = "���������� � ����", Description = "��� �������� �������� ���������� � ���, ������ ���������� �� ����� ������� ������������� �������� ���� ������.")]
        public bool IsOnTop { get; set; }

        [Display(Name = "������� ������")]
        public bool IsSoldOut { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsDeleted { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Realtor Realtor { get; set; }

        [ScaffoldColumn(false)]
        public virtual RealtorExtended RealtorExtended { get; set; }

        [ScaffoldColumn(false)]
        public virtual AdvertisementStatus AdvertisementStatus { get; set; }

        [ScaffoldColumn(false)]
        public virtual Currency Currency { get; set; }

        [ScaffoldColumn(false)]
        public virtual Land Land { get; set; }

        [ScaffoldColumn(false)]
        public virtual LandExtendedView LandExtended { get; set; }

        [ScaffoldColumn(false)]
        public virtual LandCategory LandCategory { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<File> Files { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "����������";
        }

        #endregion
    }
}
