using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class AdvertisementLog : EntityBase<int>
    {
        [UIHint("Text")]
        [Display(Name = "��������")]
        public LogActionType Action { get; set; }

        [UIHint("Text")]
        [Display(Name = "����������")]
        public int AdvertisementId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [UIHint("Text")]
        [Display(Name = "����������� �����")]
        public string CadNum { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(128, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [HiddenInput(DisplayValue = false)]
        public string UserId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "�������")]
        public string RealtorFullName { get; set; }

        [UIHint("Raw")]
        [Display(Name = "��������")]
        public string Description { get; set; }

        [UIHint("Text")]
        [Display(Name = "����")]
        public DateTime Date { get; set; }

        #region Methods

        public override string ToString()
        {
            return "��� ����������";
        }

        #endregion
    }
}
