﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;
using SharpEngine.Web.Mvc.Validation;

namespace LandHunter.Models
{
    public class Agency : EntityBase<int>
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "ОГРН")]
        public string OGRN { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Адрес")]
        public string Address { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [AdditionalMetadata("CssClass", "phone-input")]
        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        [EmailAddress(ErrorMessage = null, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "EmailIncorrect")]
        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [RegularExpression(ValidationRegex.Url, ErrorMessage = "Url адрес введен некорректно.")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Сайт")]
        public string Website { get; set; }

        [UIHint("TextArea")]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(128, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [HiddenInput(DisplayValue = false)]
        public string UserId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "Создатель")]
        public string UserDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsDeleted { get; set; }

        [NotMapped]
        [HttpPostedFileExtensions("jpeg,jpg,png", ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "AllowedFileExtensions")]
        [HttpPostedFileSize(15360, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "UploadedFilesSizeIncorrect")]
        [UIHint("File")]
        [Display(Name = "Логотип", Description = "Максимальный объем файла - 15 мб.")]
        public HttpPostedFileBase HttpPostedFile { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ICollection<Realtor> Realtors { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<RealtorExtended> RealtorsExtended { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<File> Files { get; set; }

        [ScaffoldColumn(false)]
        public virtual ApplicationUser User { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Агенство";
        }

        #endregion
    }
}
