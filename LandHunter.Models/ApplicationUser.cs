﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LandHunter.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
        }

        public ApplicationUser(string userName)
            : base(userName)
        {
        }

        public bool IsDeleted { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Realtor Realtor { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<File> Files { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<Agency> Agencies { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<ModeratorService> ModeratorServices { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<ServiceOrder> ServiceOrders { get; set; }

        #endregion
    }
}