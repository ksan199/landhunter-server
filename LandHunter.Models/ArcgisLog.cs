﻿using SharpEngine.Web.Mvc.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandHunter.Models
{
    public class ArcgisLog : EntityBase<int>
    {
        public string ip { get; set; }

        public string request { get; set; }

        public DateTime timeStart { get; set; }

        public DateTime timeEnd { get; set; }
        
    }
}
