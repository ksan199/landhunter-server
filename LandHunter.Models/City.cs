﻿using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Entities;

namespace LandHunter.Models
{
    public class City : EntityBase<int>
    {    
        [Display(Name = "Идентификатор региона")]
        public string RegionId { get; set; }

        [Display(Name="Регион")]
        public string RegionName { get; set; }

        [Display(Name = "Название города")]
        public string Name { get; set; }
    }
}