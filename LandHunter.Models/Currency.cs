using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using LandHunter.Common.JsonConverters;
using Newtonsoft.Json;
using SharpEngine.Data;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Currency : IEntityBase<int>
    {
        public Currency()
        {
            Advertisements = new HashSet<Advertisement>();
            ExchangeRates = new HashSet<ExchangeRate>();
        }

        [JsonConverter(typeof(ToStringConverter))]
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "��������")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(10, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "����������")]
        public string ShortName { get; set; }

        [JsonIgnore]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(10, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "���������� ���")]
        public string CharCode { get; set; }

        [JsonIgnore]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [Display(Name = "�������� ���")]
        public int NumCode { get; set; }

        #region Navigation properties

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<Advertisement> Advertisements { get; set; }

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<ExchangeRate> ExchangeRates { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "������";
        }

        #endregion
    }
}
