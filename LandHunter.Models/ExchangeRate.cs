using System;
using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Entities;

namespace LandHunter.Models
{
    public class ExchangeRate : EntityBase<int>
    {
        public int CurrencyId { get; set; }

        public decimal Nominal { get; set; }

        public decimal Value { get; set; }

        public DateTime Date { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Currency Currency { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "���� ������";
        }

        #endregion
    }
}