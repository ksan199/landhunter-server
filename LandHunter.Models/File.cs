using System;
using LandHunter.Common.Enums;
using SharpEngine.Data;
using SharpEngine.Web.Mvc.Entities;
using System.ComponentModel.DataAnnotations;

namespace LandHunter.Models
{
    public class File : EntityBase<int>, ISortableEntity
    {
        [Required]
        [StringLength(50)]
        public string FileName { get; set; }

        [StringLength(50)]
        public string Extension { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        [Required]
        public FileTypeKey Type { get; set; }

        public FileCategoryKey? Category { get; set; }

        [Required]
        [StringLength(300)]
        public string Path { get; set; }

        [StringLength(300)]
        public string ThumbnailPath { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        public int? AdvertisementId { get; set; }

        [StringLength(50)]
        public string CadNum { get; set; }

        public int? AgencyId { get; set; }

        [Required]
        public int SortOrder { get; set; }

        [Required]
        public DateTime UploadDate { get; set; }

        public bool IsTemporary { get; set; }

        public bool IsDeactivated { get; set; }

        public bool IsDeleted { get; set; }

        public string Md5Hash { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ApplicationUser User { get; set; }

        [ScaffoldColumn(false)]
        public virtual Advertisement Advertisement { get; set; }

        [ScaffoldColumn(false)]
        public virtual Land Land { get; set; }

        [ScaffoldColumn(false)]
        public virtual Agency Agency { get; set; }

        #endregion
    }
}
