using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Web;
using System.Web.Mvc;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Attributes;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Land
    {
        public Land()
        {
            Advertisements = new HashSet<Advertisement>();
            Files = new HashSet<File>();
            Orders = new HashSet<Order>();
        }

        [Key]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [UIHint("Text")]
        [Display(Name = "����������� �����")]
        public string CadNum { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int RegionId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "������")]
        public string RegionName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "�����")]
        public string Address { get; set; }

        [AdditionalMetadata("CssClass", "prop-changed-flag")]
        [Display(Name = "����� ������� (�� �����������)")]
        public bool AddressChanged { get; set; }

        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "��� �������������")]
        public string Utilization { get; set; }

        [AdditionalMetadata("CssClass", "prop-changed-flag")]
        [Display(Name = "��� ������������� ������� (�� �����������)")]
        public bool UtilizationChanged { get; set; }

        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [DropDownList("CategoriesDictionary")]
        [Display(Name = "���������")]
        public string CategoryCode { get; set; }

        [AdditionalMetadata("CssClass", "prop-changed-flag")]
        [Display(Name = "��������� �������� (�� �����������)")]
        public bool CategoryCodeChanged { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public IEnumerable<SelectListItem> CategoriesDictionary { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [Display(Name = "�������", Description = "� ������ ����������.")]
        public decimal Area { get; set; }

        [AdditionalMetadata("CssClass", "prop-changed-flag")]
        [Display(Name = "������� �������� (�� �����������)")]
        public bool AreaChanged { get; set; }

        [UIHint("Text")]
        [Display(Name = "����������� ���������", Description = "�� ���� ����������, � ������.")]
        public decimal? CadastrePrice { get; set; }

        [UIHint("Text")]
        [Display(Name = "��������� ���������� �� ���")]
        public DateTime? UpdateDate { get; set; }

        [UIHint("TextArea")]
        [Display(Name = "��������")]
        public string Description { get; set; }

        [Display(Name = "������� ����")]
        public bool GasPipeline { get; set; }

        [Display(Name = "����������")]
        public bool WaterPipeline { get; set; }

        [Display(Name = "����������������")]
        public bool PowerLine { get; set; }          

        [Display(Name = "�����")]
        public bool Sand { get; set; }

        [UIHint("Text")]
        [Display(Name = "��������")]
        public DateTime CreateDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [UIHint("Text")]
        [Display(Name = "���-�� ����������")]
        public int ViewsCount { get; set; }

        [ScaffoldColumn(false)]
        public DbGeometry CentrPoint { get; set; }

        [NotMapped]
        [HttpPostedFileExtensions("jpeg,jpg,png", ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "AllowedFileExtensions")]
        [HttpPostedFileSize(15360, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "UploadedFilesSizeIncorrect")]
        [UIHint("Files")]
        [Display(Name = "����������� �������", Description = "����� ������� ��������� ������. ������������ ����� - 15 ��.")]
        public IEnumerable<HttpPostedFileBase> HttpPostedFile { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Region Region { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<Advertisement> Advertisements { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<AdvertisementExtended> AdvertisementsExtended { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<File> Files { get; set; }

        [ScaffoldColumn(false)]
        public virtual LandCategory LandCategory { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<RealtorFavouriteLand> RealtorsFavouriteLand { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<Order> Orders { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "��������� �������";
        }

        #endregion
    }
}
