using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class LandCategory
    {
        public LandCategory()
        {
            Advertisements = new HashSet<Advertisement>();
            Land = new HashSet<Land>();
        }

        [JsonProperty(PropertyName = "Id")]
        [Key]
        [StringLength(50)]
        public string Code { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(1024)]
        public string Name { get; set; }

        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Сокращение")]
        public string ShortName { get; set; }

        #region Navigation properties

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<Advertisement> Advertisements { get; set; }

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<Land> Land { get; set; }

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<LandExtendedView> LandsExtended { get; set; }

        #endregion
    }
}
