using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SharpEngine.Web.Mvc.Attributes;
using SharpEngine.Web.Mvc.Entities;
using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class ModeratorService : EntityBase<int>
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(128, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [DropDownList("ModeratorsDictionary")]
        [Display(Name = "���������")]
        public string ModeratorId { get; set; }

        [NotMapped]
        [ScaffoldColumn(false)]
        public IEnumerable<SelectListItem> ModeratorsDictionary { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ServiceId { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ApplicationUser Moderator { get; set; }

        [ScaffoldColumn(false)]
        public virtual Service Service { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "�������� ������";
        }

        #endregion
    }
}