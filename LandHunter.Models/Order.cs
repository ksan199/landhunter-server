﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using LandHunter.Common.Helpers;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Order : EntityBase<int>
    {  
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(128, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [HiddenInput(DisplayValue = false)]
        public string RealtorId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "Риелтор")]
        public string RealtorFullName { get { return RealtorsHelper.GetNameFirstFullName(Realtor.Surname, Realtor.Name, Realtor.Patronymic); } }

        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Тип")]
        public int OrderTypeId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "Тип")]
        public string OrderTypeName { get { return OrderType.Name; } }

        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Статус")]
        public int OrderStatusId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "Статус")]
        public string OrderStatusName { get { return OrderStatus.Name; } }

        [UIHint("Text")]
        [Display(Name = "Сумма")]
        public decimal OrderSumAmount { get; set; }

        [UIHint("Text")]
        [Display(Name = "Баланс до")]
        public decimal BalanceBefore { get; set; }

        [UIHint("Text")]
        [Display(Name = "Добавлено")]
        public DateTime CreateDate { get; set; }

        [UIHint("Text")]
        [Display(Name = "Обновлено")]
        public DateTime UpdateDate { get; set; }

        [NotMapped]
        [UIHint("BlockHeader")]
        [Display(Name = "")]
        public string BlockHeader1 { get { return "Пополнение баланса"; } }

        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [UIHint("Text")]
        [Display(Name = "Способ оплаты")]
        public string PaymentType { get; set; }

        [UIHint("Text")]
        [Display(Name = "Идентификатор в Платежной системе")]
        public long? InvoiceId { get; set; }

        [NotMapped]
        [UIHint("BlockHeader")]
        [Display(Name = "")]
        public string BlockHeader2 { get { return "Публикация"; } }

        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [UIHint("Text")]
        [Display(Name = "Кадастровый номер")]
        public string CadNum { get; set; }

        [UIHint("Text")]
        [Display(Name = "Дата начала публикации")]
        public DateTime? AdStartDate{ get; set; }

        [UIHint("Text")]
        [Display(Name = "Дата окончания публикации")]
        public DateTime? AdEndDate { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Land Land { get; set; }

        [ScaffoldColumn(false)]
        public virtual Realtor Realtor { get; set; }

        [ScaffoldColumn(false)]
        public virtual OrderStatus OrderStatus { get; set; }

        [ScaffoldColumn(false)]
        public virtual OrderType OrderType { get; set; }

        [ScaffoldColumn(false)]
        public virtual ServiceOrder ServiceOrder { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Операция";
        }

        #endregion
    }
}