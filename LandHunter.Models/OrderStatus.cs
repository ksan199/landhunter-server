﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using Newtonsoft.Json;
using SharpEngine.Data;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class OrderStatus : EntityBase<int>, ISortableEntity
    {
        public OrderStatus()
        {
            Orders = new HashSet<Order>();
        }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string Name { get; set; }

        [JsonIgnore]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [HiddenInput(DisplayValue = false)]
        public OrderStatusKey Key { get; set; }

        [JsonIgnore]
        [HiddenInput(DisplayValue = false)]
        public int SortOrder { get; set; }

        #region Navigation properties

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<Order> Orders { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Статус заказа";
        }

        #endregion
    }
}
