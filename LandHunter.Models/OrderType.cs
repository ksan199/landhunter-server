﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using Newtonsoft.Json;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class OrderType : EntityBase<int>
    {
        public OrderType()
        {
            Orders = new HashSet<Order>();
        }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string Name { get; set; }

        [JsonIgnore]
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [HiddenInput(DisplayValue = false)]
        public OrderTypeKey Key { get; set; }

        #region Navigation properties

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<Order> Orders { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Тип операции";
        }

        #endregion
    }
}
