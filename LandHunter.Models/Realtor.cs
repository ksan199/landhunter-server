using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SharpEngine.Data;

namespace LandHunter.Models
{
    public class Realtor : IEntityBase<string>
    {
        public Realtor()
        {
            Advertisements = new HashSet<Advertisement>();
            Orders = new HashSet<Order>();
        }

        [StringLength(128)]
        [HiddenInput(DisplayValue = false)]
        public string Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? AgencyId { get; set; }

        [StringLength(255)]
        public string Surname { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Patronymic { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Website { get; set; }

        public int ViewsCount { get; set; }

        public int Rating { get; set; }

        public int RatingBonus { get; set; }

        public decimal Balance { get; set; }

        public int ContinuedVisitsCount { get; set; }

        public int TotalVisitsCount { get; set; }

        public DateTime LastVisitDate { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ApplicationUser User { get; set; }

        [ScaffoldColumn(false)]
        public virtual Agency Agency { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<Advertisement> Advertisements { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<RealtorFavouriteLand> RealtorFavouriteLands { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<Order> Orders { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "�������";
        }

        #endregion
    }
}