﻿using System.ComponentModel.DataAnnotations;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class RealtorFavouriteLand : EntityBase<int>
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(128, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string RealtorId { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        public string CadNum { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Realtor Realtor { get; set; }

        [ScaffoldColumn(false)]
        public virtual Land Land { get; set; }

        #endregion
    }
}
