﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandHunter.Models
{
    public class RealtorOrder
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public string Status { get; set; }
        public string UserData { get; set; }
        public string Email { get; set; }
        public string ReceptionType { get; set; }
        public string RosreestrNumber { get; set; }
        public string ReceptionCode { get; set; }
        public string ModeratorId { get; set; }
        public string Comment { get; set; }
    }
}
