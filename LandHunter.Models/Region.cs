using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Region : EntityBase<int>
    {
        public Region()
        {
            Lands = new HashSet<Land>();
        }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(1024)]
        public string Name { get; set; }

        public int CianId { get; set; }

        #region Navigation properties

        [JsonIgnore]
        [ScaffoldColumn(false)]
        public virtual ICollection<Land> Lands { get; set; }

        #endregion
    }
}
