﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SharpEngine.Data;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Service : EntityBase<int>, ISortableEntity
    {
        private string _serviceCategoryName;

        public Service()
        {
            ModeratorsService = new HashSet<ModeratorService>();
            ServiceOrders = new HashSet<ServiceOrder>();
        }

        [HiddenInput(DisplayValue = false)]
        public int ServiceCategoryId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "Категория")]
        public string ServiceCategoryName
        {
            get
            {
                if (ServiceCategory != null)
                    _serviceCategoryName = ServiceCategory.Name;

                return _serviceCategoryName;
            }
            set
            {
                _serviceCategoryName = value;
            }
        }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [UIHint("Text")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Стоимость")]
        public decimal? Price { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SortOrder { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ServiceCategory ServiceCategory { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<ModeratorService> ModeratorsService { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<ServiceOrder> ServiceOrders { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Услуга";
        }

        #endregion
    }
}