﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SharpEngine.Data;
using SharpEngine.Web.Mvc.Entities;

namespace LandHunter.Models
{
    public class ServiceCategory : NamedEntityBase<int>, ISortableEntity
    {
        public ServiceCategory()
        {
            Services = new HashSet<Service>();
        }

        [HiddenInput(DisplayValue = false)]
        public int SortOrder { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ICollection<Service> Services { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Категория услуг";
        }

        #endregion
    }
}