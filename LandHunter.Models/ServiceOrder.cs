﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using LandHunter.Common;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class ServiceOrder : EntityBase<int>
    {
        private string _serviceName;
        private string _realtor;
        private ServiceOrderUserData _userDataParsed;
        private string _moderatorName;

        [HiddenInput(DisplayValue = false)]
        public int ServiceId { get; set; }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "Услуга")]
        public string ServiceName
        {
            get
            {
                if (Service != null)
                    _serviceName = Service.Name;

                return _serviceName;
            }
            set
            {
                _serviceName = value;
            }
        }

        [NotMapped]
        [UIHint("Text")]
        [Display(Name = "Риелтор")]
        public string Realtor
        {
            get
            {
                if (Order != null)
                    _realtor = Order.RealtorFullName;

                return _realtor;
            }
            set
            {
                _realtor = value;
            }
        }

        [UIHint("Text")]
        [Display(Name = "Дата создания")]
        public DateTime CreateDate { get; set; }

        [UIHint("Text")]
        [Display(Name = "Дата обновления", Description = "Обновляется при смене статуса.")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "Статус")]
        public ServiceOrderStatus Status { get; set; }

        [NotMapped]
        public IEnumerable<SelectListItem> StatusesDictionary { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Указанные данные")]
        public string UserData { get; set; }

        [NotMapped]
        public ServiceOrderUserData UserDataParsed
        {
            get
            {
                if (_userDataParsed == null)
                    _userDataParsed = UserData != null ? JsonHelper.Deserialize<ServiceOrderUserData>(UserData) : null;

                return _userDataParsed;
            }
        }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "EmailIncorrect")]
        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [UIHint("Text")]
        [Display(Name = "Тип получения")]
        public ReceptionType ReceptionType { get; set; }

        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Номер в Росреестре")]
        public string RosreestrNumber { get; set; }

        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Код получения")]
        public string ReceptionCode { get; set; }

        [StringLength(128, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Оператор")]
        public string ModeratorId { get; set; }

        [NotMapped]
        public IEnumerable<SelectListItem> ModeratorsDictionary { get; set; }

        [NotMapped]
        public string ModeratorName
        {
            get
            {
                if (Moderator != null)
                    _moderatorName = Moderator.UserName;

                return _moderatorName;
            }
            set
            {
                _moderatorName = value;
            }
        }

        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Service Service { get; set; }

        [ScaffoldColumn(false)]
        public virtual ApplicationUser Moderator { get; set; }

        [ScaffoldColumn(false)]
        public virtual Order Order { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return "Заказ услуги";
        }

        #endregion
    }
}