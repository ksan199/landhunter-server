﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Settings : EntityBase<int>
    {
        [NotMapped]
        [UIHint("BlockHeader")]
        [Display(Name = "")]
        public string BlockHeader1 { get { return "SMS шаблоны"; } }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "SMS сообщение после регистрации", Description = "Параметры: 0 - пароль нового пользователя.")]
        public string RegistrationMessage { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "SMS сообщение после cброса пароля", Description = "Параметры: 0 - новый пароль пользователя.")]
        public string ResetMessage { get; set; }

        [NotMapped]
        [UIHint("BlockHeader")]
        [Display(Name = "")]
        public string BlockHeader2 { get { return "Операции"; } }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [Display(Name = "Сумма списания за публикацию объявления", Description = "В рублях. Допускаются дробные значения.")]
        public decimal LandOrderPublishSum { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [Display(Name = "Кол-во дней, которое будет генерироваться объявление")]
        public int LandOrderPublishDays { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Сообщение об одинаковых объявлениях", Description = "Параметры: 0 - дата начала публикации, 1 - дата окончания публикации.")]
        public string OrderExistsMessage { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Сообщение об успешном размещении объявления", Description = "Параметры: 0 - дата начала публикации, 1 - дата окончания публикации.")]
        public string OrderSuccessMessage { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Сообщение о нехватке средств по операции", Description = "Параметры: 0 - сумма снятия, 1 - баланс пользователя, 2 - разница средств.")]
        public string OrderBalanceNotEnoughMessage { get; set; }


        #region Methods

        public override string ToString()
        {
            return "Настройки портала";
        }

        #endregion
    }
}