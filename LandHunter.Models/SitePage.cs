﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SharpEngine.Data;
using SharpEngine.Web.Mvc.Entities;
using SharpEngine.Web.Mvc.Resources;
using SharpEngine.Web.Mvc.Validation;

namespace LandHunter.Models
{
    public class SitePage : EntityBase<int>, ISortableEntity
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(200, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Название")]
        public string Title { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(200, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [RegularExpression(ValidationRegex.SafeName, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "SafeNameIncorrect")]
        [Display(Name = "Адрес")]
        public string Url { get; set; }

        [AllowHtml]
        [UIHint("TextArea")]
        [AdditionalMetadata("CssClass", "ckeditor")]
        [Display(Name = "Содержание")]
        public string Content { get; set; }

        [Display(Name = "Показывать в меню")]
        public bool ShowInMenu { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SortOrder { get; set; }

        #region Methods

        public override string ToString()
        {
            return "Страница";
        }

        #endregion
    }
}