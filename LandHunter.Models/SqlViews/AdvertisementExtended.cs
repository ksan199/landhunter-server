using System;
using System.ComponentModel.DataAnnotations;

namespace LandHunter.Models
{
    public class AdvertisementExtended
    {
        [Key]
        public int Id { get; set; }

        public string RealtorId { get; set; }

        public int AdvertisementStatusId { get; set; }

        public decimal Price { get; set; }

        public int CurrencyId { get; set; }

        [StringLength(50)]
        public string CadNum { get; set; }

        [StringLength(1000)]
        public string Address { get; set; }

        public string Description { get; set; }

        [StringLength(1000)]
        public string Utilization { get; set; }

        [StringLength(50)]
        public string CategoryCode { get; set; }

        public decimal? Area { get; set; }

        public bool GasPipeline { get; set; }

        public bool WaterPipeline { get; set; }

        public bool PowerLine { get; set; }

        public bool Sand { get; set; }

        public DateTime CreateDate { get; set; }

        [Display(Name = "���������� � ����", Description = "��� �������� �������� ���������� � ���, ������ ���������� �� ����� ������� ������������� �������� ���� ������.")]
        public bool IsOnTop { get; set; }

        public bool IsSoldOut { get; set; }

        public bool IsDeleted { get; set; }

        public decimal? PriceInRub { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual Land Land { get; set; }

        #endregion
    }
}