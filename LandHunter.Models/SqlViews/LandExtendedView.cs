using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LandHunter.Models
{
    public class LandExtendedView
    {
        [Key]
        [StringLength(50)]
        public string CadNum { get; set; }

        public int RegionId { get; set; }

        [StringLength(1000)]
        public string Address { get; set; }

        public bool AddressChanged { get; set; }

        public string Description { get; set; }

        [StringLength(1000)]
        [Display(Name = "��� �������������")]
        public string Utilization { get; set; }

        public bool UtilizationChanged { get; set; }

        [StringLength(50)]
        [Display(Name = "���������")]
        public string CategoryCode { get; set; }

        public bool CategoryCodeChanged { get; set; }

        [Display(Name = "������� ����")]
        public bool GasPipeline { get; set; }

        [Display(Name = "����������")]
        public bool WaterPipeline { get; set; }

        [Display(Name = "����������������")]
        public bool PowerLine { get; set; }

        [Display(Name = "�����")]
        public bool Sand { get; set; }

        public decimal Area { get; set; }

        public bool AreaChanged { get; set; }

        public decimal? CadastrePrice { get; set; }

        [Display(Name = "��������")]
        public DateTime CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public DbGeometry CentrPoint { get; set; }

        public int ViewsCount { get; set; }

        public decimal? MinRubPrice { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ICollection<Advertisement> Advertisements { get; set; }

        [ScaffoldColumn(false)]
        public virtual LandCategory LandCategory { get; set; }

        #endregion
    }
}
