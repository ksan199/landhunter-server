using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LandHunter.Models
{
    public class RealtorExtended
    {
        [Key]
        [StringLength(128)]
        public string Id { get; set; }

        public int? AgencyId { get; set; }

        [StringLength(255)]
        public string Surname { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Patronymic { get; set; }

        public string FullName { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Website { get; set; }

        public int ViewsCount { get; set; }

        public int Rating { get; set; }

        public int AdvertisementsCount { get; set; }

        #region Navigation properties

        [ScaffoldColumn(false)]
        public virtual ApplicationUser User { get; set; }

        [ScaffoldColumn(false)]
        public virtual Agency Agency { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<Advertisement> Advertisements { get; set; }

        #endregion
    }
}