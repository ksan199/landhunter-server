﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using LandHunter.Common.JsonConverters;
using Newtonsoft.Json;
using SharpEngine.Data;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.Models
{
    public class Test : IEntityBase<int>
    {
        public Test() 
        {
        }

        [JsonConverter(typeof(ToStringConverter))]
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Название")]
        public string Name { get; set; }

    }
}
