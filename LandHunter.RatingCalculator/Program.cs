﻿using System;
using Autofac;
using LandHunter.DataAccess.Context;
using LandHunter.DataAccess.Repository;
using LandHunter.Services;
using SharpEngine.Repository;

namespace LandHunter.RatingCalculator
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            RegisterDependencies();

            try
            {
                CalculateRating();

                Console.WriteLine("Realtors rating was successfully calculated.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while calculating realtors rating: " + e.Message + " - " + e.StackTrace);
            }
#if DEBUG
            Console.WriteLine("Press any key to close console.");
            Console.ReadKey();
#endif
        }

        private static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<LandHunterContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(LandHunterRepository<,>)).As(typeof(IRepository<,>)).InstancePerLifetimeScope();
            builder.RegisterType<RatingService>().As<IRatingService>().InstancePerLifetimeScope();

            Container = builder.Build();
        }

        private static void CalculateRating()
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var ratingService = scope.Resolve<IRatingService>();

                ratingService.Calculate();
            }
        }
    }
}
