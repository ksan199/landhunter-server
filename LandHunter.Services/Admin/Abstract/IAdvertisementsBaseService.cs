using System.Security.Principal;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.ViewModels;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.Services
{
    public interface IAdvertisementsBaseService
    {
        ActionGrid<Advertisement, LandAdvertisementGrid> GetByLandActionGrid(string cadNum, AdvertisementGridOptions options, IPrincipal principal);
        void MoveToLand(MoveToLandViewModel data);
        void AllMoveToLand();
    }
}