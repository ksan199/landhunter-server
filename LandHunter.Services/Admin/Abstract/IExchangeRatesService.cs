using System;

namespace LandHunter.Services
{
    public interface IExchangeRatesService
    {
        void RefreshDailyRates();
    }
}