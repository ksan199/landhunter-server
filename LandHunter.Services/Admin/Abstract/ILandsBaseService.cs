using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.Services
{
    public interface ILandsBaseService
    {
        IQueryable<LandExtendedView> GetQuery(IPrincipal principal);
        ActionGrid<LandExtendedView, LandGrid> GetActionGrid(LandGridOptions options, IPrincipal principal);
        Land Get(string id);
        Land Edit(string id, IPrincipal principal);
        void Edit(Land edit, IPrincipal principal);
        Land FillDictionaries(Land entity, IPrincipal principal);
        Filter<LandExtendedView> GetFilter(LandGridOptions options, string gridKey, IPrincipal principal);
        int UpdateAllCadastreInfo(int batchSize = 20);
        int UpdateCadastreInfo(IEnumerable<string> cadNums);
    }
}