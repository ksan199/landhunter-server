using System.Security.Principal;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.Services
{
    public interface IModeratorsServicesBaseService
    {
        ActionGrid<ModeratorService, ModeratorServiceGrid> GetActionGrid(int serviceId, ModeratorServiceGridOptions options, IPrincipal principal);
    }
}