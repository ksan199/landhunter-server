﻿using System.Security.Principal;
using LandHunter.ViewModels;

namespace LandHunter.Services
{
    public interface IRealtorsBaseService
    {
        EditRealtorViewModel EditRealtorUser(string id, IPrincipal principal);
        RealtorViewModel FillDictionaries(RealtorViewModel entity, IPrincipal principal);
        void CreateRealtorUser(CreateRealtorViewModel create, IPrincipal principal);
        void EditRealtorUser(EditRealtorViewModel edit, IPrincipal principal);
    }
}