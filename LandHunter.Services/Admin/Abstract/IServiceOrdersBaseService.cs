using System.Security.Principal;

namespace LandHunter.Services
{
    public interface IServiceOrdersBaseService
    {
        void Take(int serviceOrderId, IPrincipal principal);
    }
}