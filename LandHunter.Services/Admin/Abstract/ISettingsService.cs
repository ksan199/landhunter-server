using System.Security.Principal;
using LandHunter.Models;

namespace LandHunter.Services
{
    public interface ISettingsService
    {
        Settings Get();
        Settings Edit(IPrincipal principal);
        void Edit(Settings edit, IPrincipal principal);
    }
}