﻿using System.Linq;
using LandHunter.Common.Helpers;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.Services
{
    internal static class ActionGridHelper
    {
        internal static int? TryGetCurrencyId(IGridOptions options)
        {
            try
            {
                return options.FilterConditionValues
                    .Where(f => f.Key == "__CurrencyId")
                    .Select(f => int.Parse(f.Values[0]))
                    .Single();
            }
            catch
            {
                return null;
            }
        }

        internal static string TryNormalizeCadNum(string searchString)
        {
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                var cadNum = LandsHelper.NormalizeCadNum(searchString);

                if (cadNum != null)
                    searchString = cadNum;
            }

            return searchString;
        }
    }
}
