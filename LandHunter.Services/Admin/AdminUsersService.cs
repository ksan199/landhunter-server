﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using EntityFramework.Extensions;
using LandHunter.AspNetIdentity;
using LandHunter.DataAccess.Context;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.AspNetIdentity;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.Services
{
    public class AdminUsersService : IAdminUsersService
    {
        private readonly Lazy<LandHunterContext> _dbContext;
        private readonly Lazy<DbSet<ApplicationUser>> _usersDbSet;
        private readonly Lazy<DbSet<IdentityUserRole>> _usersRolesDbSet;
        private readonly IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> _aspNetIdentity;

        public AdminUsersService(
            Lazy<LandHunterContext> dbContext,
            IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> aspNetIdentity)
        {
            _dbContext = dbContext;
            _usersDbSet = new Lazy<DbSet<ApplicationUser>>(() => _dbContext.Value.Set<ApplicationUser>());
            _usersRolesDbSet = new Lazy<DbSet<IdentityUserRole>>(() => _dbContext.Value.Set<IdentityUserRole>());
            _aspNetIdentity = aspNetIdentity;
        }

        public IQueryable<EditAdminUserViewModel> GetQuery(IPrincipal principal)
        {
            var rolesIds = _aspNetIdentity.GetRolesQuery()
                .Where(r => r.Name == "Moderator" || r.Name == "SiteAdministrator" || r.Name == "UsersAdministrator")
                .Select(r => r.Id)
                .ToArray();

            var currentUserId = principal.Identity.GetUserId();

            return _usersDbSet.Value
                .Where(u => !u.IsDeleted && u.Roles.All(ur => rolesIds.Contains(ur.RoleId)) && u.Id != currentUserId)
                .Select(u =>  new { u.Id, u.UserName, u.Email, u.Roles, u.LockoutEnabled })
                .ToList()
                .Select(u => new EditAdminUserViewModel
                    {
                        Id = u.Id,
                        UserName = u.UserName,
                        Email = u.Email,
                        RolesIds = u.Roles.Select(r => r.RoleId).ToArray(),
                        LockoutEnabled = u.LockoutEnabled
                    })
                .AsQueryable();
        }

        public ActionGrid<EditAdminUserViewModel, AdminUserGrid> GetActionGrid(AdminUserGridOptions options, IPrincipal principal)
        {
            var filter = new AdminUserFilter().Configure(principal).Init(options);

            var query = filter.Apply(GetQuery(principal));

            return new ActionGrid<EditAdminUserViewModel, AdminUserGrid>(query, options, FillRolesForUsers, true, false).WithFilter(filter);
        }

        public EditAdminUserViewModel Get(string id)
        {
            return _usersDbSet.Value
                .Where(u => !u.IsDeleted && u.Id == id)
                .Select(u => new { u.Id, u.UserName, u.Email, u.Roles, u.LockoutEnabled })
                .ToList()
                .Select(u => new EditAdminUserViewModel
                    {
                        Id = u.Id,
                        UserName = u.UserName,
                        Email = u.Email,
                        RolesIds = u.Roles.Select(r => r.RoleId).ToArray(),
                        LockoutEnabled = u.LockoutEnabled
                    })
                .SingleOrDefault();
        }

        public CreateAdminUserViewModel Create(IPrincipal principal)
        {
            return FillDictionaries(new CreateAdminUserViewModel(), principal) as CreateAdminUserViewModel;
        }

        public void Create(CreateAdminUserViewModel create, IPrincipal principal)
        {
            _aspNetIdentity.CreateUser(
                new ApplicationUser(create.UserName)
                    {
                        Email = create.Email,
                        EmailConfirmed = true
                    },
                create.Password);

            var userId = _usersDbSet.Value.Where(u => u.UserName == create.UserName).Select(u => u.Id).Single();

            if (create.RolesIds != null && create.RolesIds.Any())
            {
                foreach (var roleId in create.RolesIds)
                    _usersRolesDbSet.Value.Add(new IdentityUserRole { UserId = userId, RoleId = roleId });

                _dbContext.Value.SaveChanges();
            }

            create.Id = userId;
        }

        public EditAdminUserViewModel Edit(string id, IPrincipal principal)
        {
            return FillDictionaries(Get(id), principal) as EditAdminUserViewModel;
        }

        public void Edit(EditAdminUserViewModel edit, IPrincipal principal)
        {
            if (_usersDbSet.Value.Any(u => u.Id == edit.Id && u.UserName.ToLower() != edit.UserName.ToLower()))
            {
                if (_usersDbSet.Value.Any(u => u.Id != edit.Id && u.UserName.ToLower() == edit.UserName.ToLower()))
                    throw new Exception("Невозможно сменить имя пользователя. Пользователь с таким именем уже существует.");

                _usersDbSet.Value.Where(u => u.Id == edit.Id).Update(u => new ApplicationUser
                    {
                        UserName = edit.UserName,
                        Email = edit.Email,
                        LockoutEnabled = edit.LockoutEnabled
                    });
            }
            else
            {
                _usersDbSet.Value.Where(u => u.Id == edit.Id).Update(u => new ApplicationUser
                    {
                        Email = edit.Email,
                        LockoutEnabled = edit.LockoutEnabled
                    });
            }

            _usersRolesDbSet.Value.Where(ur => ur.UserId == edit.Id).Delete();

            if (edit.RolesIds != null && edit.RolesIds.Any())
            {
                foreach (var roleId in edit.RolesIds)
                    _usersRolesDbSet.Value.Add(new IdentityUserRole { UserId = edit.Id, RoleId = roleId });

                _dbContext.Value.SaveChanges();
            }
        }

        public AdminUserViewModel FillDictionaries(AdminUserViewModel user, IPrincipal principal)
        {
            var rolesDictionary = new List<SelectListItem>();

            rolesDictionary.AddRange(_aspNetIdentity.GetRolesQuery()
                .Where(r => r.Name == "Moderator" || r.Name == "SiteAdministrator" || r.Name == "UsersAdministrator").OrderBy(r => r.Name)
                .ToSelectList(r => r.Id, r => r.Name, r => user.RolesIds != null && user.RolesIds.Contains(r.Id)));

            user.RolesDictionary = rolesDictionary;

            return user;
        }

        public void Delete(string id, IPrincipal principal)
        {
            _usersDbSet.Value.Where(u => u.Id == id).Update(u => new ApplicationUser { IsDeleted = true, LockoutEnabled = true });
        }

        public Filter<EditAdminUserViewModel> GetFilter(AdminUserGridOptions options, string gridKey, IPrincipal principal)
        {
            return new AdminUserFilter().Configure(principal).Init(options, gridKey);
        }

        private void FillRolesForUsers(IEnumerable<EditAdminUserViewModel> users)
        {
            var roles = _aspNetIdentity.GetRolesQuery().Select(r => new { r.Id, r.Name }).ToList();

            foreach (var user in users)
            {
                var userRoles = roles.Where(r => user.RolesIds.Contains(r.Id)).Select(r => r.Name);

                if (userRoles.Any())
                    user.Roles = userRoles.Aggregate((r1, r2) => r1 + ", " + r2);
            }
        }
    }
}
