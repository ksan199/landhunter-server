﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.ViewModels;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Web.Mvc.Services;
using System.Web.Mvc;

namespace LandHunter.Services
{
    public class AdvertisementsBaseService : FilterableBaseService<Advertisement, int, AdvertisementGrid, AdvertisementGridOptions, AdvertisementFilter>, IAdvertisementsBaseService
    {
        private readonly Lazy<IRepository<Realtor, string>> _realtorsRepository;
        private readonly Lazy<IRepository<AdvertisementStatus, int>> _advertisementStatusesRepository;
        private readonly Lazy<IRepository<Currency, int>> _currenciesRepository;
        private readonly Lazy<IRepository<LandCategory, string>> _landCategoriesRepository;
        private readonly Lazy<IRepository<Land, string>> _landsRepository;
        private readonly Lazy<IRepository<File, int>> _filesRepository;
        private readonly Lazy<ICurrenciesService> _currenciesService;
        private int? _currencyId;

        public AdvertisementsBaseService(
            Lazy<IRepository<Advertisement, int>> repository,
            Lazy<IRepository<Realtor, string>> realtorsRepository,
            Lazy<IRepository<AdvertisementStatus, int>> advertisementStatusesRepository,
            Lazy<IRepository<Currency, int>> currenciesRepository,
            Lazy<IRepository<LandCategory, string>> landCategoriesRepository,
            Lazy<IRepository<Land, string>> landsRepository,
            Lazy<IRepository<File, int>> filesRepository,
            Lazy<ICurrenciesService> currenciesService)
            : base(repository)
        {
            _realtorsRepository = realtorsRepository;
            _advertisementStatusesRepository = advertisementStatusesRepository;
            _currenciesRepository = currenciesRepository;
            _landCategoriesRepository = landCategoriesRepository;
            _landsRepository = landsRepository;
            _filesRepository = filesRepository;
            _currenciesService = currenciesService;
        }

        public override IQueryable<Advertisement> GetQuery(IPrincipal principal)
        {
            return _repository.Value.Where(a => !a.IsDeleted);
        }

        public override void Edit(Advertisement edit, IPrincipal principal)
        {
            base.Edit(edit, principal);

            if (edit.IsOnTop)
                _repository.Value.Update(a => a.Id != edit.Id && a.CadNum == edit.CadNum, a => new Advertisement { IsOnTop = false });
        }

        public override ActionGrid<Advertisement, AdvertisementGrid> GetActionGrid(AdvertisementGridOptions options, IPrincipal principal)
        {
            _currencyId = ActionGridHelper.TryGetCurrencyId(options);
            options.SearchString = ActionGridHelper.TryNormalizeCadNum(options.SearchString);

            var filter = new AdvertisementFilter().Configure(_filterService, principal).Init(options);
            var query = filter.Apply(GetQuery(principal));

            return new ActionGrid<Advertisement, AdvertisementGrid>(query, options, ConvertPrice, true, false).WithFilter(filter);
        }

        private void ConvertPrice(IEnumerable<Advertisement> advertisements)
        {
            if (_currencyId.HasValue)
            {
                foreach (var advertisement in advertisements)
                {
                    advertisement.Price = _currenciesService.Value.ConvertPrice(advertisement.Price, advertisement.CurrencyId, _currencyId.Value);
                }
            }
        }

        public override Advertisement BeforeGet(Advertisement entity, IPrincipal principal)
        {
            entity.RealtorFullName = _realtorsRepository.Value.Where(r => r.Id == entity.RealtorId)
                .Select(r => new { r.Name, r.Surname, r.Patronymic })
                .ToList()
                .Select(r => RealtorsHelper.GetFullName(r.Surname, r.Name, r.Patronymic))
                .Single();

            var statusesDictionary = new List<SelectListItem> { new SelectListItem { Text = "", Value = "", Selected = entity.AdvertisementStatusId == 0 } };
            statusesDictionary.AddRange(_advertisementStatusesRepository.Value.GetQuery().OrderBy(s => s.SortOrder).ToSelectList(s => s.Id, s => s.Name, s => s.Id == entity.AdvertisementStatusId));

            entity.StatusesDictionary = statusesDictionary;

            var currenciesDictionary = new List<SelectListItem> { new SelectListItem { Text = "", Value = "", Selected = entity.CurrencyId == 0 } };
            currenciesDictionary.AddRange(_currenciesRepository.Value.GetQuery().OrderBy(c => c.Name).ToSelectList(c => c.Id, c => c.Name, c => c.Id == entity.CurrencyId));

            entity.CurrenciesDictionary = currenciesDictionary;

            var categoriesDictionary = new List<SelectListItem> { new SelectListItem { Text = "", Value = "", Selected = entity.CategoryCode == null } };
            categoriesDictionary.AddRange(_landCategoriesRepository.Value.GetQuery().OrderBy(c => c.Name).ToSelectList(c => c.Code, c => c.Name, c => c.Code == entity.CategoryCode));

            entity.CategoriesDictionary = categoriesDictionary;

            return entity;
        }

        public override void Delete(int id, IPrincipal principal)
        {
            Delete(new[] { id }, principal);
        }

        public override void Delete(int[] ids, IPrincipal principal)
        {
            _repository.Value.Update(e => ids.Contains(e.Id), a => new Advertisement { IsDeleted = true });
        }

        public ActionGrid<Advertisement, LandAdvertisementGrid> GetByLandActionGrid(string cadNum, AdvertisementGridOptions options, IPrincipal principal)
        {
            var query = GetQuery(principal).Where(a => a.Land.CadNum == cadNum);

            return new ActionGrid<Advertisement, LandAdvertisementGrid>(query, options, false, false);
        }

        public void AllMoveToLand()
        {
            MoveToLandViewModel data = new MoveToLandViewModel()
            {
                AdvertisementId = 0,
                MoveAddress = true,
                MoveDescription = true,
                MoveUtilization = true,
                MoveCategoryCode = true,
                MoveArea = true,
                MoveCommunications = true
            };

            var advertisements = _repository.Value
                .Where(a => !a.IsDeleted && a.AdvertisementStatusId == 3)
                .Select(a => new
                {
                    a.Id
                });
            List<int> idList = new List<int>();
            foreach (var advertisement in advertisements)
            {
                idList.Add(advertisement.Id);
            }
            List<Exception> errors = new List<Exception>();
            foreach (var advertisementId in idList)
            {
                data.AdvertisementId = advertisementId;
                try
                {
                    MoveToLand(data);
                }
                catch (Exception e)
                {
                    errors.Add(e);
                }
            }
            _repository.Value.Update(a => !a.IsDeleted && a.AdvertisementStatusId == 3, a => new Advertisement { AdvertisementStatusId = 4 });
        }

        public void MoveToLand(MoveToLandViewModel data)
        {
            var advertisement = _repository.Value
                .Where(a => !a.IsDeleted && a.Id == data.AdvertisementId)
                .Select(a => new
                    {
                        a.CadNum,
                        a.Address,
                        a.Description,
                        a.Utilization,
                        a.CategoryCode,
                        a.Area,
                        a.GasPipeline,
                        a.PowerLine,
                        a.WaterPipeline,
                        a.Sand,
                        FilesIds = a.Files.Where(f => !f.IsDeleted && f.Category == FileCategoryKey.Image).Select(f => f.Id)
                    })
                .Single();
            var land = _landsRepository.Value.Single(l => l.CadNum == advertisement.CadNum);

            if (data.MoveAddress)
            {
                if (string.IsNullOrWhiteSpace(advertisement.Address))
                    throw new Exception("Адрес является обязательным полем у участка.");

                land.Address = advertisement.Address;
                land.AddressChanged = true;
            }

            if (data.MoveDescription)
            {
                land.Description = advertisement.Description;
            }

            if (data.MoveUtilization)
            {
                land.Utilization = advertisement.Utilization;
                land.UtilizationChanged = true;
            }

            if (data.MoveCategoryCode)
            {
                land.CategoryCode = advertisement.CategoryCode;
                land.CategoryCodeChanged = true;
            }

            if (data.MoveArea)
            {
                if (!advertisement.Area.HasValue)
                    throw new Exception("Площадь является обязательным полем у участка.");

                land.Area = advertisement.Area.Value;
                land.AreaChanged = true;
            }

            if (data.MoveCommunications)
            {
                land.GasPipeline = advertisement.GasPipeline;
                land.WaterPipeline = advertisement.WaterPipeline;
                land.PowerLine = advertisement.PowerLine;
                land.Sand = advertisement.Sand;
            }
            
            data.ImagesIdsToMove = data.ImagesIdsToMove.Where(advertisement.FilesIds.Contains).ToArray();

            if (data.ImagesIdsToMove.Any())
            {
                _filesRepository.Value.Update(f => data.ImagesIdsToMove.Contains(f.Id), f => new File { CadNum = land.CadNum });
            }

            
            _landsRepository.Value.Update(land);
            _landsRepository.Value.SaveChanges();
        }
    }
}