﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.Common.Helpers;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Services
{
    public class AdvertisementsLogsBaseService : FilterableBaseService<AdvertisementLog, int, AdvertisementLogGrid, AdvertisementLogGridOptions, AdvertisementLogFilter>
    {
        private readonly Lazy<IRepository<Realtor, string>> _realtorsRepository;

        public AdvertisementsLogsBaseService(
            Lazy<IRepository<AdvertisementLog, int>> repository,
            Lazy<IRepository<Realtor, string>> realtorsRepository)
            : base(repository)
        {
            _realtorsRepository = realtorsRepository;
        }

        public override ActionGrid<AdvertisementLog, AdvertisementLogGrid> GetActionGrid(AdvertisementLogGridOptions options, IPrincipal principal)
        {
            options.SearchString = ActionGridHelper.TryNormalizeCadNum(options.SearchString);

            var filter = new AdvertisementLogFilter().Configure(_filterService, principal).Init(options);
            var query = filter.Apply(GetQuery(principal));

            return new ActionGrid<AdvertisementLog, AdvertisementLogGrid>(query, options, InsertUserInfo, true, false).WithFilter(filter);
        }

        private void InsertUserInfo(IEnumerable<AdvertisementLog> logs)
        {
            var usersIds = logs.Select(l => l.UserId).Distinct().ToArray();

            var realtors = _realtorsRepository.Value
                .Where(r => usersIds.Contains(r.Id))
                .Select(r => new { r.Id, r.Name, r.Surname, r.Patronymic })
                .ToList();

            foreach (var log in logs)
            {
                log.RealtorFullName = realtors
                    .Where(r => r.Id == log.UserId)
                    .Select(r => RealtorsHelper.GetFullName(r.Surname, r.Name, r.Patronymic))
                    .FirstOrDefault();

                if (log.RealtorFullName == null)
                    log.RealtorFullName = log.UserId;
            }
        }

        public override AdvertisementLog BeforeGet(AdvertisementLog entity, IPrincipal principal)
        {
            entity.RealtorFullName = _realtorsRepository.Value.Where(r => r.Id == entity.UserId)
                .Select(r => new { r.Name, r.Surname, r.Patronymic })
                .ToList()
                .Select(r => RealtorsHelper.GetFullName(r.Surname, r.Name, r.Patronymic))
                .FirstOrDefault();

            if (entity.RealtorFullName == null)
                entity.RealtorFullName = entity.UserId;

            if (entity.Description != null)
                entity.Description = entity.Description.Replace(Environment.NewLine, "<br />");

            return entity;
        }
    }
}