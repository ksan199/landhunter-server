﻿using System;
using System.Linq;
using System.Security.Principal;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using LandHunter.GridModels;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Services
{
    public class AgenciesBaseService : BaseService<Agency, int, AgencyGrid, AgencyGridOptions>
    {
        private readonly Lazy<IRepository<ApplicationUser, string>> _usersRepository;
        private readonly Lazy<IRepository<File, int>> _filesRepository;
        private readonly Lazy<IFilesService> _filesService;

        public AgenciesBaseService(
            Lazy<IRepository<Agency, int>> repository,
            Lazy<IRepository<ApplicationUser, string>> usersRepository,
            Lazy<IRepository<File, int>> filesRepository,
            Lazy<IFilesService> filesService)
            : base(repository)
        {
            _usersRepository = usersRepository;
            _filesRepository = filesRepository;
            _filesService = filesService;
        }

        public override IQueryable<Agency> GetQuery(IPrincipal principal)
        {
            return _repository.Value.Where(a => !a.IsDeleted);
        }

        public override ActionGrid<Agency, AgencyGrid> GetActionGrid(AgencyGridOptions options, IPrincipal principal)
        {
            var query = GetQuery(principal);

            return new ActionGrid<Agency, AgencyGrid>(query, options, true, false);
        }

        public override Agency Create(IPrincipal principal)
        {
            return BeforeGet(new Agency { UserId = principal.Identity.GetUserId() }, principal);
        }

        public override void Create(Agency create, IPrincipal principal)
        {
            _repository.Value.Add(create);
            _repository.Value.SaveChanges();

            if (create.HttpPostedFile != null)
                _filesService.Value.Save(create.HttpPostedFile, principal, category: FileCategoryKey.Image, resize: ResizeImage.Agency, agencyId: create.Id);
        }

        public override void Edit(Agency edit, IPrincipal principal)
        {
            _repository.Value.Update(edit);
            _repository.Value.SaveChanges();

            if (edit.HttpPostedFile != null)
            {
                _filesRepository.Value.Update(f => f.AgencyId == edit.Id, f => new File { IsDeleted = true });

                _filesService.Value.Save(edit.HttpPostedFile, principal, category: FileCategoryKey.Image, resize: ResizeImage.Agency, agencyId: edit.Id);
            }
        }

        public override Agency BeforeGet(Agency entity, IPrincipal principal)
        {
            entity.UserDescription = _usersRepository.Value.Where(u => u.Id == entity.UserId)
                .Select(u => new { u.UserName, u.Realtor })
                .ToList()
                .Select(u => u.UserName + (u.Realtor != null ? " (" + RealtorsHelper.GetFullName(u.Realtor.Surname, u.Realtor.Name, u.Realtor.Patronymic) + ")" : null))
                .Single();

            return entity;
        }

        public override void Delete(int id, IPrincipal principal)
        {
            Delete(new[] { id }, principal);
        }

        public override void Delete(int[] ids, IPrincipal principal)
        {
            _repository.Value.Update(e => ids.Contains(e.Id), a => new Agency { IsDeleted = true });
        }
    }
}