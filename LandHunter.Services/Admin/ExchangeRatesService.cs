﻿using System;
using System.Data;
using System.Linq;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.Services.CBRSoapService;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Services
{
    public class ExchangeRatesService : BaseService<ExchangeRate, int, ExchangeRateGrid, ExchangeRateGridOptions>, IExchangeRatesService
    {
        private readonly Lazy<IRepository<Currency, int>> _currencyRepository;
        private readonly DailyInfoSoap _dailyInfoSoap;

        public ExchangeRatesService(
            Lazy<IRepository<ExchangeRate, int>> repository,
            Lazy<IRepository<Currency, int>> currencyRepository,
            DailyInfoSoap dailyInfoSoap)
            : base(repository)
        {
            _currencyRepository = currencyRepository;
            _dailyInfoSoap = dailyInfoSoap;
        }

        public void RefreshDailyRates()
        {
            var ratesLastDate = _dailyInfoSoap.GetLatestDateTime();

            if (_repository.Value.GetQuery().All(r => r.Date != ratesLastDate))
            {
                var exchangeRates = _dailyInfoSoap.GetCursOnDate(ratesLastDate).Tables[0];

                if (exchangeRates.AsEnumerable().Any())
                {
                    _currencyRepository.Value.GetQuery()
                        .Select(c => new { c.Id, c.NumCode })
                        .ToList()
                        .ForEach(c =>
                            {
                                var currencyRate = exchangeRates.AsEnumerable().FirstOrDefault(r => r.Field<int>("Vcode") == c.NumCode);

                                if (currencyRate != null)
                                {
                                    _repository.Value.Add(new ExchangeRate
                                        {
                                            CurrencyId = c.Id,
                                            Nominal = currencyRate.Field<decimal>("Vnom"),
                                            Value = currencyRate.Field<decimal>("Vcurs"),
                                            Date = ratesLastDate
                                        });
                                }
                            });

                    _repository.Value.SaveChanges();
                }
            }
        }
    }
}