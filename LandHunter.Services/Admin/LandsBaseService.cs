﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter.Services
{
    public class LandsBaseService : ILandsBaseService
    {
        private readonly Lazy<IRepository<Land, string>> _landsRepository;
        private readonly Lazy<IRepository<LandExtendedView, string>> _landsExtendedRepository;
        private readonly Lazy<IRepository<Region, int>> _regionsRepository;
        private readonly Lazy<IRepository<LandCategory, string>> _landCategoriesRepository;
        private readonly Lazy<IFilesService> _filesService;
        private readonly Lazy<IExternalService> _externalService;
        private readonly Lazy<ICurrenciesService> _currenciesService;
        private int? _currencyId;

        public LandsBaseService(
            Lazy<IRepository<Land, string>> landsRepository,
            Lazy<IRepository<LandExtendedView, string>> landsExtendedRepository,
            Lazy<IRepository<Region, int>> regionsRepository,
            Lazy<IRepository<LandCategory, string>> landCategoriesRepository,
            Lazy<IFilesService> filesService,
            Lazy<IExternalService> externalService,
            Lazy<ICurrenciesService> currenciesService)
        {
            _landsRepository = landsRepository;
            _landsExtendedRepository = landsExtendedRepository;
            _regionsRepository = regionsRepository;
            _landCategoriesRepository = landCategoriesRepository;
            _filesService = filesService;
            _externalService = externalService;
            _currenciesService = currenciesService;
        }

        public IQueryable<LandExtendedView> GetQuery(IPrincipal principal)
        {
            return _landsExtendedRepository.Value.GetQuery();
        }

        public ActionGrid<LandExtendedView, LandGrid> GetActionGrid(LandGridOptions options, IPrincipal principal)
        {
            _currencyId = ActionGridHelper.TryGetCurrencyId(options);
            options.SearchString = ActionGridHelper.TryNormalizeCadNum(options.SearchString);

            var filter = new LandFilter().Configure(principal).Init(options);

            var query = filter.Apply(GetQuery(principal));

            return new ActionGrid<LandExtendedView, LandGrid>(query, options, ConvertMinPrice, true, false).WithFilter(filter);
        }

        private void ConvertMinPrice(IEnumerable<LandExtendedView> lands)
        {
            if (_currencyId.HasValue)
            {
                foreach (var land in lands)
                {
                    if (land.MinRubPrice.HasValue)
                        land.MinRubPrice = _currenciesService.Value.ConvertPrice(land.MinRubPrice.Value, _currencyId.Value);
                }
            }
        }

        public Land Get(string id)
        {
            return _landsRepository.Value.Single(id);
        }

        public Land Edit(string id, IPrincipal principal)
        {
            return FillDictionaries(Get(id), principal);
        }

        public void Edit(Land edit, IPrincipal principal)
        {
            var cadastreInfo = _landsRepository.Value
                .Where(l => l.CadNum == edit.CadNum)
                .Select(l => new { l.Address, l.Utilization, l.CategoryCode, l.Area })
                .Single();

            _landsRepository.Value.Update(l => l.CadNum == edit.CadNum, l => new Land
                {
                    Address = edit.Address,
                    Utilization = edit.Utilization,
                    CategoryCode = edit.CategoryCode,
                    Area = edit.Area,
                    AddressChanged = cadastreInfo.Address != edit.Address,
                    UtilizationChanged = cadastreInfo.Utilization != edit.Utilization,
                    CategoryCodeChanged = cadastreInfo.CategoryCode != edit.CategoryCode,
                    AreaChanged = cadastreInfo.Area != edit.Area,
                    Description = edit.Description,
                    GasPipeline = edit.GasPipeline,
                    WaterPipeline = edit.WaterPipeline,
                    PowerLine = edit.PowerLine,
                    Sand = edit.Sand
                });

            if (edit.HttpPostedFile != null && edit.HttpPostedFile.Any(f => f != null))
                _filesService.Value.Save(edit.HttpPostedFile, principal, category: FileCategoryKey.Image, cadNum: edit.CadNum);
        }

        public Land FillDictionaries(Land entity, IPrincipal principal)
        {
            entity.RegionName = _regionsRepository.Value.Where(r => r.Id == entity.RegionId).Select(r => r.Name).FirstOrDefault();

            var categoriesDictionary = new List<SelectListItem> { new SelectListItem { Text = "", Value = "", Selected = entity.CategoryCode == null } };
            categoriesDictionary.AddRange(_landCategoriesRepository.Value.GetQuery().OrderBy(c => c.Name).ToSelectList(c => c.Code, c => c.Name, c => c.Code == entity.CategoryCode));

            entity.CategoriesDictionary = categoriesDictionary;

            return entity;
        }

        public Filter<LandExtendedView> GetFilter(LandGridOptions options, string gridKey, IPrincipal principal)
        {
            return new LandFilter().Configure(principal).Init(options, gridKey);
        }

        public int UpdateAllCadastreInfo(int batchSize = 20)
        {
            var result = 0;

            var allCadNums = _landsRepository.Value.GetQuery().Select(l => l.CadNum).ToList();

            var skip = 0;

            while (skip < allCadNums.Count)
            {
                var batch = allCadNums.Skip(skip).Take(batchSize).ToList();

                result += UpdateCadastreInfo(batch);

                skip += batchSize;
            }

            return result;
        }

        public int UpdateCadastreInfo(IEnumerable<string> cadNums)
        {
            var cadastresInfo = _externalService.Value.GetCadastresInfo(cadNums);

            var updatedLands = 0;

            foreach (var cadastre in cadastresInfo)
            {
                var land = _landsRepository.Value.SingleOrDefault(cadastre.CadNum);

                if (land != null)
                {
                    if (!land.AddressChanged)
                        land.Address = cadastre.Address;

                    if (!land.UtilizationChanged)
                        land.Utilization = cadastre.Utilization;

                    if (!land.CategoryCodeChanged)
                        land.CategoryCode = cadastre.CategoryCode;

                    if (!land.AreaChanged)
                        land.Area = cadastre.Area;

                    land.CadastrePrice = cadastre.Price;

                    land.UpdateDate = DateTime.Now;

                    _landsRepository.Value.Update(land);

                    updatedLands++;
                }
            }

            _landsRepository.Value.SaveChanges();

            return updatedLands;
        }
    }
}