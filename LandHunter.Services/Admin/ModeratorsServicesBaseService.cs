﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using LandHunter.AspNetIdentity;
using LandHunter.DataAccess.Context;
using LandHunter.GridModels;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.AspNetIdentity;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Services
{
    public class ModeratorsServicesBaseService : BaseService<ModeratorService, int, ModeratorServiceGrid, ModeratorServiceGridOptions>, IModeratorsServicesBaseService
    {
        private readonly IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> _aspNetIdentity;

        public ModeratorsServicesBaseService(
            Lazy<IRepository<ModeratorService, int>> repository,
            IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> aspNetIdentity)
            : base(repository)
        {
            _aspNetIdentity = aspNetIdentity;
        }

        public override ModeratorService BeforeGet(ModeratorService entity, IPrincipal principal)
        {
            var existingIds = _repository.Value.Where(ms => ms.ServiceId == entity.ServiceId).Select(ms => ms.ModeratorId).ToList();

            var moderatorRoleId = _aspNetIdentity.GetRolesQuery()
                .Where(r => r.Name == "Moderator").Select(r => r.Id).SingleOrDefault();

            var moderatorsQuery = _aspNetIdentity.GetUsersQuery()
                .Where(u => !u.IsDeleted && !u.LockoutEnabled && u.Roles.Any(ur => ur.RoleId == moderatorRoleId) && !existingIds.Contains(u.Id));

            var moderatorsDictionary = new List<SelectListItem> { new SelectListItem { Text = "", Value = "", Selected = entity.ModeratorId == null } };
            moderatorsDictionary.AddRange(moderatorsQuery.OrderBy(u => u.UserName).ToSelectList(u => u.Id, u => u.UserName, u => u.Id == entity.ModeratorId));

            entity.ModeratorsDictionary = moderatorsDictionary;

            return entity;
        }

        public ActionGrid<ModeratorService, ModeratorServiceGrid> GetActionGrid(int serviceId, ModeratorServiceGridOptions options, IPrincipal principal)
        {
            var query = GetQuery(principal).Where(ms => ms.ServiceId == serviceId);

            return new ActionGrid<ModeratorService, ModeratorServiceGrid>(query, options, false, false);
        }
    }
}