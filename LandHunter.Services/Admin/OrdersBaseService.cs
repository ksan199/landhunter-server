﻿using System;
using System.Security.Principal;
using LandHunter.GridModels;
using LandHunter.Models;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Services
{
    public class OrdersBaseService : FilterableBaseService<Order, int, OrderGrid, OrderGridOptions, OrderFilter>
    {
        public OrdersBaseService(Lazy<IRepository<Order, int>> repository)
            : base(repository)
        {
        }

        public override ActionGrid<Order, OrderGrid> GetActionGrid(OrderGridOptions options, IPrincipal principal)
        {
            var filter = new OrderFilter().Configure(_filterService, principal).Init(options);
            var query = filter.Apply(GetQuery(principal));

            return new ActionGrid<Order, OrderGrid>(query, options, true, false).WithFilter(filter);
        }
    }
}