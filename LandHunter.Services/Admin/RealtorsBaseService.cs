﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using EntityFramework.Extensions;
using LandHunter.AspNetIdentity;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using LandHunter.DataAccess.Context;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.AspNetIdentity;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Services
{
    public class RealtorsBaseService : FilterableBaseService<Realtor, string, RealtorGrid, RealtorGridOptions, RealtorFilter>, IRealtorsBaseService
    {
        private readonly IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> _aspNetIdentity;
        private readonly Lazy<IRepository<Agency, int>> _agenciesRepository;
        private readonly Lazy<IRepository<File, int>> _filesRepository;
        private readonly Lazy<IFilesService> _filesService;

        public RealtorsBaseService(
            Lazy<IRepository<Realtor, string>> repository,
            IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> aspNetIdentity,
            Lazy<IRepository<Agency, int>> agenciesRepository,
            Lazy<IRepository<File, int>> filesRepository,
            Lazy<IFilesService> filesService)
            : base(repository)
        {
            _aspNetIdentity = aspNetIdentity;
            _agenciesRepository = agenciesRepository;
            _filesRepository = filesRepository;
            _filesService = filesService;
        }

        public override IQueryable<Realtor> GetQuery(IPrincipal principal)
        {
            return _repository.Value.GetQuery().Where(r => !r.User.IsDeleted);
        }

        public override ActionGrid<Realtor, RealtorGrid> GetActionGrid(RealtorGridOptions options, IPrincipal principal)
        {
            var filter = new RealtorFilter().Configure(principal).Init(options);
            var query = filter.Apply(GetQuery(principal));

            return new ActionGrid<Realtor, RealtorGrid>(query, options, true, false).WithFilter(filter);
        }

        public override Realtor Get(string id)
        {
            return _repository.Value.Single(r => !r.User.IsDeleted && r.Id == id);
        }

        public override void Delete(string id, IPrincipal principal)
        {
            _aspNetIdentity.GetUsersQuery().Where(u => u.Id == id).Update(u => new ApplicationUser { IsDeleted = true, LockoutEnabled = true });
        }

        public EditRealtorViewModel EditRealtorUser(string id, IPrincipal principal)
        {
            var realtor = Get(id);
            var advertisements = realtor.Advertisements.Select(a => new { a.IsSoldOut, a.IsDeleted }).ToList();

            var editRealtor = new EditRealtorViewModel
                {
                    Id = id,
                    PhoneNumber = realtor.User.PhoneNumber,
                    Surname = realtor.Surname,
                    Name = realtor.Name,
                    Patronymic = realtor.Patronymic,
                    Email = realtor.User.Email,
                    Address = realtor.Address,
                    Website = realtor.Website,
                    AgencyId = realtor.AgencyId,
                    LockoutEnabled = realtor.User.LockoutEnabled,
                    Balance = realtor.Balance,
                    ViewsCount = realtor.ViewsCount,
                    Rating = realtor.Rating,
                    RatingBonus = realtor.RatingBonus,
                    ContinuedVisitsCount = realtor.ContinuedVisitsCount,
                    TotalVisitsCount = realtor.TotalVisitsCount,
                    LastVisitDate = realtor.LastVisitDate,
                    AllAdvertisementsCount = advertisements.Count(),
                    ActiveAdvertisementsCount = advertisements.Count(a => !a.IsSoldOut && !a.IsDeleted),
                    SoldOutAdvertisementsCount = advertisements.Count(a => a.IsSoldOut && !a.IsDeleted),
                    DeletedAdvertisementsCount = advertisements.Count(a => a.IsDeleted)
                };

            return (EditRealtorViewModel)FillDictionaries(editRealtor, principal);
        }

        public RealtorViewModel FillDictionaries(RealtorViewModel entity, IPrincipal principal)
        {
            var agenciesDictionary = new List<SelectListItem> { new SelectListItem { Text = "", Value = "", Selected = !entity.AgencyId.HasValue } };
            agenciesDictionary.AddRange(_agenciesRepository.Value.GetQuery().OrderBy(a => a.Name).ToSelectList(a => a.Id, a => a.Name, a => a.Id == entity.AgencyId));

            entity.AgenciesDictionary = agenciesDictionary;

            return entity;
        }

        public void CreateRealtorUser(CreateRealtorViewModel create, IPrincipal principal)
        {
            var userName = RealtorsHelper.GetUserNameFromPhone(create.PhoneNumber);

            _aspNetIdentity.CreateUser(
                new ApplicationUser(userName)
                    {
                        PhoneNumber = create.PhoneNumber,
                        PhoneNumberConfirmed = true,
                        Email = create.Email,
                        EmailConfirmed = create.Email != null
                    },
                create.Password);

            var userId = _aspNetIdentity.GetUsersQuery().Where(u => u.UserName == userName).Select(u => u.Id).Single();

            _aspNetIdentity.AddUserToRole(userId, "Realtor");

            _repository.Value.Add(new Realtor
                {
                    Id = userId,
                    Surname = create.Surname,
                    Name = create.Name,
                    Patronymic = create.Patronymic,
                    Address = create.Address,
                    Website = create.Website,
                    AgencyId = create.AgencyId,
                    LastVisitDate = DateTime.Now
                });

            _repository.Value.SaveChanges();

            if (create.HttpPostedFile != null)
                _filesService.Value.Save(create.HttpPostedFile, principal, category: FileCategoryKey.Image, resize: ResizeImage.Realtor, userId: userId);

            create.Id = userId;
        }

        public void EditRealtorUser(EditRealtorViewModel edit, IPrincipal principal)
        {
            var userName = RealtorsHelper.GetUserNameFromPhone(edit.PhoneNumber);

            if (_aspNetIdentity.GetUsersQuery().Any(u => u.Id == edit.Id && u.UserName != userName))
            {
                if (_aspNetIdentity.GetUsersQuery().Any(u => u.Id != edit.Id && u.UserName == userName))
                    throw new Exception("Невозможно сменить номер телефона. Пользователь с таким номером телефона уже существует.");

                _aspNetIdentity.GetUsersQuery().Where(u => u.Id == edit.Id).Update(u => new ApplicationUser
                    {
                        UserName = userName,
                        PhoneNumber = edit.PhoneNumber,
                        Email = edit.Email,
                        EmailConfirmed = edit.Email != null,
                        LockoutEnabled = edit.LockoutEnabled
                    });
            }
            else
            {
                _aspNetIdentity.GetUsersQuery().Where(u => u.Id == edit.Id).Update(u => new ApplicationUser
                    {
                        Email = edit.Email,
                        EmailConfirmed = edit.Email != null,
                        LockoutEnabled = edit.LockoutEnabled
                    });
            }

            _repository.Value.Where(r => r.Id == edit.Id).Update(r => new Realtor
                {
                    Surname = edit.Surname,
                    Name = edit.Name,
                    Patronymic = edit.Patronymic,
                    Address = edit.Address,
                    Website = edit.Website,
                    AgencyId = edit.AgencyId,
                    RatingBonus = edit.RatingBonus
                });

            if (edit.HttpPostedFile != null)
            {
                _filesRepository.Value.Update(f => f.UserId == edit.Id, f => new File { IsDeleted = true });

                _filesService.Value.Save(edit.HttpPostedFile, principal, category: FileCategoryKey.Image, resize: ResizeImage.Realtor, userId: edit.Id);
            }
        }
    }
}
