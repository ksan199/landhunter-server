﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using LandHunter.Common.Enums;
using LandHunter.GridModels;
using LandHunter.Models;
using LandHunter.ViewModels;
using Microsoft.AspNet.Identity;
using SharpEngine.Core;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Web.Mvc.Services;

namespace LandHunter.Services
{
    public class ServiceOrdersBaseService : FilterableBaseService<ServiceOrder, int, ServiceOrderGrid, ServiceOrderGridOptions, ServiceOrderFilter>, IServiceOrdersBaseService
    {
        private readonly Lazy<IRepository<ModeratorService, int>> _moderatorsServicesRepository;

        public ServiceOrdersBaseService(
            Lazy<IRepository<ServiceOrder, int>> repository,
            Lazy<IRepository<ModeratorService, int>> moderatorsServicesRepository)
            : base(repository)
        {
            _moderatorsServicesRepository = moderatorsServicesRepository;
        }

        public override void Edit(ServiceOrder edit, IPrincipal principal)
        {
            var order = _repository.Value.Single(edit.Id);

            if (order.Status == ServiceOrderStatus.Suspended)
                throw new Exception("Нельзя изменять не оплаченный заказ.");

            if (principal.IsInRole("SuperAdministrator"))
            {
                order.ModeratorId = edit.ModeratorId;
            }
            else
            {
                var currentUserId = principal.Identity.GetUserId();

                if (edit.ModeratorId != currentUserId)
                    throw new Exception("Вы должны быть оператором данного заказа, для его изменения.");
            }

            if (order.Status != edit.Status)
                order.UpdateDate = DateTime.Now;

            order.RosreestrNumber = edit.RosreestrNumber;
            order.ReceptionCode = edit.ReceptionCode;
            order.Status = edit.Status;
            order.Comment = edit.Comment;

            _repository.Value.Update(order);
            _repository.Value.SaveChanges();
        }

        public override IQueryable<ServiceOrder> GetQuery(IPrincipal principal)
        {
            var query = _repository.Value.GetQuery();

            if (principal.IsInRole("Moderator"))
            {
                var currentUserId = principal.Identity.GetUserId();

                query = query.Where(o => o.Service.ModeratorsService.Any(ms => ms.ModeratorId == currentUserId));
            }

            return query;
        }

        public override ActionGrid<ServiceOrder, ServiceOrderGrid> GetActionGrid(ServiceOrderGridOptions options, IPrincipal principal)
        {
            var filter = new ServiceOrderFilter().Configure(_filterService, principal).Init(options);
            var query = filter.Apply(GetQuery(principal));

            return new ActionGrid<ServiceOrder, ServiceOrderGrid>(query, options, true, false).WithFilter(filter);
        }

        public override ServiceOrder BeforeGet(ServiceOrder entity, IPrincipal principal)
        {
            var statuses = new List<SelectListItem>();
            statuses.AddRange(Enum.GetValues(typeof(ServiceOrderStatus)).Cast<ServiceOrderStatus>()
                .Where(s => entity.Status == ServiceOrderStatus.Suspended || s != ServiceOrderStatus.Suspended)
                .ToSelectList(s => s, s => s.GetDescription(), s => s == entity.Status));

            entity.StatusesDictionary = statuses;

            if (principal.IsInRole("SuperAdministrator"))
            {
                var moderators = new List<SelectListItem> { new SelectListItem { Value = "", Text = "", Selected = entity.ModeratorId == null } };

                moderators.AddRange(_moderatorsServicesRepository.Value
                    .Where(ms => ms.ServiceId == entity.ServiceId)
                    .Select(ms => new { ms.ModeratorId, ms.Moderator.UserName })
                    .ToList()
                    .OrderBy(m => m.UserName)
                    .ToSelectList(m => m.ModeratorId, m => m.UserName, m => m.ModeratorId == entity.ModeratorId));

                entity.ModeratorsDictionary = moderators;
            }

            return entity;
        }

        public void Take(int serviceOrderId, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            if (_repository.Value.GetQuery().Any(o => o.Id == serviceOrderId && o.ModeratorId != null))
                throw new Exception("На данный заказ уже назначен оператор.");

            if (_repository.Value.GetQuery().Any(o => o.Id == serviceOrderId && o.Status == ServiceOrderStatus.Suspended))
                throw new Exception("Нельзя взять не оплаченный заказ.");

            _repository.Value.Update(o => o.Id == serviceOrderId, o => new ServiceOrder { ModeratorId = currentUserId, Status = ServiceOrderStatus.InProcessing });
        }
    }
}