﻿using System;
using System.Linq;
using System.Security.Principal;
using LandHunter.Models;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly Lazy<IRepository<Settings, int>> _settingsRepository;

        public SettingsService(Lazy<IRepository<Settings, int>> settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        public Settings Get()
        {
            return _settingsRepository.Value.GetQuery().Single();
        }

        public Settings Edit(IPrincipal principal)
        {
            return Get();
        }

        public void Edit(Settings edit, IPrincipal principal)
        {
            _settingsRepository.Value.Update(edit);
            _settingsRepository.Value.SaveChanges();
        }
    }
}