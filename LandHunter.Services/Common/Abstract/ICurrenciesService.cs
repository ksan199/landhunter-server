using System.Collections.Generic;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface ICurrenciesService
    {
        IEnumerable<ExchangeRateApiModel> GetExchangeRates();
        decimal ConvertPrice(decimal price, int toCurrencyId);
        decimal ConvertPrice(decimal price, int fromCurrencyId, int toCurrencyId);
    }
}