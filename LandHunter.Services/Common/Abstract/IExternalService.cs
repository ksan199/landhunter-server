﻿using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace LandHunter.Services
{
    public interface IExternalService
    {
        DbGeometry GetCentroid(string cadNum);
        IReadOnlyList<CadastreInfo> GetCadastresInfo(IEnumerable<string> cadNums);
        CadastreInfo GetCadastreInfo(string cadNum);
    }
}