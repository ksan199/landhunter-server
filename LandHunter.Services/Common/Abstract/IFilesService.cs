﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Web;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Models;
using LandHunter.ViewModels;

namespace LandHunter.Services
{
    public interface IFilesService
    {
        IEnumerable<File> GetAdvertisementFiles(int advertisementId, IPrincipal principal);
        IEnumerable<File> GetLandFiles(string cadNum, IPrincipal principal);
        IEnumerable<File> GetUserFiles(string userId, IPrincipal principal);
        IEnumerable<File> GetAgencyFiles(int agencyId, IPrincipal principal);
        IEnumerable<FileApiModel> Save(HttpRequest request, IPrincipal principal,
            FileCategoryKey? category = null, bool isTemporary = false,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null);
        IEnumerable<FileApiModel> Save(HttpPostedFileBase postedFile, IPrincipal principal,
            FileCategoryKey? category = null, ResizeImage resize = ResizeImage.Default, bool isTemporary = false,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null);
        IEnumerable<FileApiModel> Save(IEnumerable<HttpPostedFileBase> postedFiles, IPrincipal principal,
            FileCategoryKey? category = null, ResizeImage resize = ResizeImage.Default, bool isTemporary = false,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null);
        void AssignTempFile(int id, IPrincipal principal, FileCategoryKey category,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null);
        EditFileViewModel Edit(int id, IPrincipal principal);
        void Edit(EditFileViewModel edit, IPrincipal principal);
        void Toggle(int id, bool isDeactivatedNow, IPrincipal principal);
        void MarkAsDeleted(int id, IPrincipal principal);
        void Sort(int[] ids);
        void DeleteAllTempFiles(string webSitePath, int daysBeforeNow);

        void FixMd5(string webSitePath);
    }
}