using System.Threading.Tasks;
using LandHunter.Models;
using LandHunter.ViewModels;

namespace LandHunter.Services
{
    public interface IMailService
    {
        Task<ServiceEmailViewModel> GetNewServiceEmailAsync(int serviceOrderId);
        Task SendServiceEmailAsync(ServiceEmailViewModel email);
        void SendServiceOrderNotices(ServiceOrder serviceOrder, string[] moderatorsEmails);
        void SendServiceOrderPaidUpNotices(ServiceOrder serviceOrder, string[] moderatorsEmails);
    }
}