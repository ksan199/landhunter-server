﻿namespace LandHunter.Services
{
    public interface IRatingService
    {
        void Calculate();
    }
}