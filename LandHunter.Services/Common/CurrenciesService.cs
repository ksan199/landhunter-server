﻿using System;
using System.Collections.Generic;
using System.Linq;
using LandHunter.ApiModels;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class CurrenciesService : ICurrenciesService
    {
        private readonly Lazy<IRepository<Currency, int>> _currenciesRepository;
        private readonly Lazy<IRepository<ExchangeRate, int>> _exchangeRatesRepository;
        private const int RubNumCode = 643;

        public CurrenciesService(
            Lazy<IRepository<Currency, int>> currenciesRepository,
            Lazy<IRepository<ExchangeRate, int>> exchangeRatesRepository)
        {
            _currenciesRepository = currenciesRepository;
            _exchangeRatesRepository = exchangeRatesRepository;
        }

        public IEnumerable<ExchangeRateApiModel> GetExchangeRates()
        {
            var result = new List<ExchangeRateApiModel>();

            var currencies = _currenciesRepository.Value.GetQuery().ToList();
            var foreignСurrenciesCount = currencies.Count(c => c.NumCode != RubNumCode);
            var exchangeRates = _exchangeRatesRepository.Value.GetQuery()
                .OrderByDescending(r => r.Date)
                .Take(foreignСurrenciesCount)
                .ToList();

            currencies.ForEach(fromCur =>
                {
                    var fromCurToRub = exchangeRates.FirstOrDefault(r => r.CurrencyId == fromCur.Id);

                    currencies.Where(toCur => toCur.Id != fromCur.Id).ToList().ForEach(toCur =>
                        {
                            var toCurToRub = exchangeRates.FirstOrDefault(r => r.CurrencyId == toCur.Id);

                            var exchangeRate = new ExchangeRateApiModel
                                {
                                    From = fromCur.Id,
                                    To = toCur.Id
                                };

                            if (fromCurToRub != null && toCur.NumCode == RubNumCode)
                                exchangeRate.Value = fromCurToRub.Value / fromCurToRub.Nominal;
                            else if (fromCur.NumCode == RubNumCode && toCurToRub != null)
                                exchangeRate.Value = toCurToRub.Nominal / toCurToRub.Value;
                            else if (fromCurToRub != null && toCurToRub != null)
                                exchangeRate.Value = (fromCurToRub.Value / fromCurToRub.Nominal) / (toCurToRub.Value / toCurToRub.Nominal);

                            result.Add(exchangeRate);
                        });
                });

            return result;
        }

        public decimal ConvertPrice(decimal price, int toCurrencyId)
        {
            var rubId = _currenciesRepository.Value.Where(c => c.NumCode == RubNumCode).Select(c => c.Id).Single();

            return ConvertPrice(price, rubId, toCurrencyId);
        }

        public decimal ConvertPrice(decimal price, int fromCurrencyId, int toCurrencyId)
        {
            var exchangeRates = GetExchangeRates();

            return CurrenciesHelper.Convert(price, fromCurrencyId, toCurrencyId, exchangeRates);
        }
    }
}