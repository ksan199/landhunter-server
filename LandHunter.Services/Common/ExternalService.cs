﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Web;
using LandHunter.Common.Helpers;
using Newtonsoft.Json.Linq;

namespace LandHunter.Services
{
    public class ExternalService : IExternalService
    {
        private static readonly CultureInfo UsNumbersCulture = CultureInfo.GetCultureInfo("en-US");

        private const string CadastreUrlForOne = "http://maps.rosreestr.ru/arcgis/rest/services/Cadastre/CadastreSelected/MapServer/exts/GKNServiceExtension/online/land/find?f=json&onlyAttributes=false&returnGeometry=false&cadNum={0}";
        private const string CadastreUrlForMany = "http://maps.rosreestr.ru/arcgis/rest/services/Cadastre/CadastreSelected/MapServer/exts/GKNServiceExtension/online/land/find?f=json&onlyAttributes=false&returnGeometry=false&cadNums=[{0}]";
        private const string GeometryUrl = "http://services.arcgis.com/128AVqe19g9TGt8d/arcgis/rest/services/Parcels/FeatureServer/0/query?returnGeometry=true&f=json&where=CAD_NUM+LIKE+'{0}'";

        public DbGeometry GetCentroid(string cadNum)
        {
            var rings = new List<List<Tuple<double, double>>>();

            var requestUrl = string.Format(AppConfigHelper.GetSetting("GeometryUrl", GeometryUrl), HttpUtility.UrlEncode(cadNum));

            var jsonRespond = WebRequestHelper.Get(requestUrl);

            var jObjectRespond = JObject.Parse(jsonRespond);
            var futures = jObjectRespond["features"];

            var geometryRings = futures.First.SelectToken("geometry.rings");

            foreach (var geometryRing in geometryRings)
            {
                var ring = new List<Tuple<double, double>>();

                foreach (var point in geometryRing)
                {
                    var coord1 = (double)point[0];
                    var coord2 = (double)point[1];

                    ring.Add(new Tuple<double, double>(coord1, coord2));
                }

                rings.Add(ring);
            }

            var centroid = GetCentroidInternal(rings);

            return DbGeometry.PointFromText("POINT("
                + centroid.Item1.ToString("G", UsNumbersCulture) + " "
                + centroid.Item2.ToString("G", UsNumbersCulture) + ")", 4326);
        }

        private static Tuple<double, double> GetCentroidInternal(IReadOnlyList<IReadOnlyList<Tuple<double, double>>> rings)
        {
            var f = new List<double[]>();

            foreach (var ring in rings)
            {
                double a = 0;
                double c = 0;
                double e = 0;

                for (var i = 0; i < (ring.Count - 1); i++)
                {
                    var point = ring[i];
                    var nextPoint = ring[i + 1];

                    var h = (point.Item1 * nextPoint.Item2) - (nextPoint.Item1 * point.Item2);

                    a += (point.Item1 + nextPoint.Item1) * h;
                    c += (point.Item2 + nextPoint.Item2) * h;
                    e += h;
                }

                if (0 < e)
                    e *= -1;

                f.Add(new[] { a, c, e / 2 });
            }

            f = f.OrderBy(x => x[2]).ToList();

            var d = 6 * f[0][2];

            return new Tuple<double, double>(f[0][0] / d, f[0][1] / d);
        }

        public IReadOnlyList<CadastreInfo> GetCadastresInfo(IEnumerable<string> cadNums)
        {
            var requestUrl = string.Format(AppConfigHelper.GetSetting("CadastreUrlForMany", CadastreUrlForMany), string.Join(",", cadNums.Select(c => "\"" + c + "\"")));

            return GetCadastreInfoInternal(requestUrl);
        }

        public CadastreInfo GetCadastreInfo(string cadNum)
        {
            var requestUrl = string.Format(AppConfigHelper.GetSetting("CadastreUrlForOne", CadastreUrlForOne), cadNum);

            return GetCadastreInfoInternal(requestUrl).FirstOrDefault();
        }

        private static IReadOnlyList<CadastreInfo> GetCadastreInfoInternal(string fullUrl)
        {
            var cadastresInfo = new List<CadastreInfo>();

            var jsonRespond = WebRequestHelper.Get(fullUrl);

            var jObjectRespond = JObject.Parse(jsonRespond);
            var futures = jObjectRespond["features"];

            foreach (var future in futures)
            {
                var cadNum = GetFutureAttribute(future, "CAD_NUM");
                var address = GetFutureAttribute(future, "OBJECT_ADDRESS");
                var utilization = GetFutureAttribute(future, "UTIL_BY_DOC");
                var categoryCode = GetFutureAttribute(future, "CATEGORY_TYPE");
                var area = decimal.Parse(GetFutureAttribute(future, "AREA_VALUE"), UsNumbersCulture);
                var areaUnit = GetFutureAttribute(future, "AREA_UNIT");
                var cadCost = GetFutureAttribute(future, "CAD_COST");
                var price = cadCost != null ? decimal.Parse(cadCost, UsNumbersCulture) : (decimal?)null;

                LandsHelper.NormalizeCadNum(ref cadNum);

                cadastresInfo.Add(new CadastreInfo
                    {
                        CadNum = cadNum,
                        Address = address,
                        Utilization = utilization,
                        CategoryCode = categoryCode,
                        Area = LandsHelper.ConvertAreaToMeters(area, areaUnit),
                        Price = price
                    });
            }

            return cadastresInfo;
        }

        private static string GetFutureAttribute(JToken futureToken, string attribute)
        {
            var value = ((string)futureToken.SelectToken("attributes." + attribute));

            if (value != null)
                value = value.Trim();

            return !string.IsNullOrEmpty(value) ? value : null;
        }
    }

    public class CadastreInfo
    {
        public string CadNum { get; set; }
        public string Address { get; set; }
        public string Utilization { get; set; }
        public string CategoryCode { get; set; }
        public decimal Area { get; set; }
        public decimal? Price { get; set; }
    }
}