﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.ViewModels;
using SharpEngine.Repository;
using SharpEngine.Web.Mvc;
using File = LandHunter.Models.File;
using System.Security.Cryptography;
using System.Text;

namespace LandHunter.Services
{
    public class FilesService : IFilesService
    {
        private readonly Lazy<IRepository<File, int>> _repository;

        private static readonly string[] ImagesExtensions = { "bmp", "gif", "svg", "tif", "tiff", "jpe", "jpg", "jpeg", "png" };
        private static readonly string[] VideoExtensions = { "avi", "m4u", "m4v", "mov", "mp4", "mpeg", "mpg", "qt" };
        private static readonly string[] DocsExtensions = { "doc", "xsl", "xlsx", "xltx", "xls", "txt", "djvu", "djv", "docx", "dotx",
            "docm", "dotm", "htm", "html", "ppt", "pptx", "potx", "ppsx", "ppam", "pptm", "potm", "ppsm", "rtf", "rtx" };

        private const string ImagesPath = "~/Content/Uploads/Images";
        private const string VideoPath = "~/Content/Uploads/Video";
        private const string DocumentsPath = "~/Content/Uploads/Documents";
        private const string OtherPath = "~/Content/Uploads/Other";
        private const string ThumbnailsFolder = "Thumbnails";

        public FilesService(Lazy<IRepository<File, int>> repository)
        {
            _repository = repository;
        }

        public IEnumerable<File> GetAdvertisementFiles(int advertisementId, IPrincipal principal)
        {
            return _repository.Value
                .Where(f => f.AdvertisementId == advertisementId && !f.IsDeleted)
                .OrderBy(f => f.SortOrder)
                .ThenByDescending(f => f.UploadDate)
                .ToList();
        }

        public IEnumerable<File> GetLandFiles(string cadNum, IPrincipal principal)
        {
            return _repository.Value
                .Where(f => f.CadNum == cadNum && !f.IsDeleted)
                .OrderBy(f => f.SortOrder)
                .ThenByDescending(f => f.UploadDate)
                .ToList();
        }

        public IEnumerable<File> GetUserFiles(string userId, IPrincipal principal)
        {
            return _repository.Value
                .Where(f => f.UserId == userId && !f.IsDeleted)
                .OrderBy(f => f.SortOrder)
                .ThenByDescending(f => f.UploadDate)
                .ToList();
        }

        public IEnumerable<File> GetAgencyFiles(int agencyId, IPrincipal principal)
        {
            return _repository.Value
                .Where(f => f.AgencyId == agencyId && !f.IsDeleted)
                .OrderBy(f => f.SortOrder)
                .ThenByDescending(f => f.UploadDate)
                .ToList();
        }

        public IEnumerable<FileApiModel> Save(HttpRequest request, IPrincipal principal,
            FileCategoryKey? category = null, bool isTemporary = false,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null)
        {
            ResizeImage resize;

            var resizeValue = request.Form["resize"];

            if (!Enum.TryParse(resizeValue, true, out resize))
                resize = ResizeImage.Default;

            var postedFiles = request.Files.AllKeys.Select(k => request.Files[k]).Select(f => new HttpPostedFileWrapper(f));

            return Save(postedFiles, principal, category, resize, isTemporary, advertisementId, cadNum, userId, agencyId);
        }

        public IEnumerable<FileApiModel> Save(HttpPostedFileBase postedFile, IPrincipal principal,
            FileCategoryKey? category = null, ResizeImage resize = ResizeImage.Default, bool isTemporary = false,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null)
        {
            return Save(new[] { postedFile }, principal, category, resize, isTemporary, advertisementId, cadNum, userId, agencyId);
        }

        public IEnumerable<FileApiModel> Save(IEnumerable<HttpPostedFileBase> postedFiles, IPrincipal principal,
            FileCategoryKey? category = null, ResizeImage resize = ResizeImage.Default, bool isTemporary = false,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null)
        {
            var files = new List<File>();

            foreach (var postedFile in postedFiles.Where(f => f != null))
            {
                var uniqueFileName = Guid.NewGuid();
                var extension = Path.GetExtension(postedFile.FileName.ToLower());

                bool needThumbnail;
                var fileType = GetFileType(extension, out needThumbnail);

                var filePath = Path.Combine(GetPhysicalPathFor(fileType), uniqueFileName + extension);

                var image = fileType == FileTypeKey.Image ? ImageHelper.ConvertStreamToImage(postedFile.InputStream) : null;

                if (fileType == FileTypeKey.Image && (resize == ResizeImage.Realtor || resize == ResizeImage.Agency))
                {
                    var resized = ImageHelper.ScaleAndCrop(image, 200, 200);
                    resized.Save(filePath);
                }
                else
                {
                    postedFile.SaveAs(filePath);
                }

                var fileName = Path.GetFileNameWithoutExtension(postedFile.FileName);

                if (fileName != null && fileName.Length > 50)
                    fileName = fileName.Substring(0, 47) + "...";

                string md5HashSum = getMd5HashForPath(filePath);

                var file = new File
                    {
                        FileName = fileName,
                        Extension = extension,
                        Type = fileType,
                        Path = GetRelativePath(filePath),
                        Category = category,
                        AdvertisementId = advertisementId,
                        CadNum = cadNum,
                        UserId = userId,
                        AgencyId = agencyId,
                        IsTemporary = isTemporary,
                        SortOrder = 0,
                        UploadDate = DateTime.Now,
                        Md5Hash = md5HashSum
                    };

                if (needThumbnail)
                {
                    var thumbnailPath = Path.Combine(GetPhysicalPathFor(fileType), ThumbnailsFolder, uniqueFileName + extension);

                    if (fileType == FileTypeKey.Image)
                    {
                        Image thumbnail;

                        if (resize == ResizeImage.Realtor || resize == ResizeImage.Agency)
                            thumbnail = ImageHelper.ScaleAndCrop(image, 70, 70);
                        else
                            thumbnail = ImageHelper.Scale(image, 200);

                        thumbnail.Save(thumbnailPath);

                        file.ThumbnailPath = GetRelativePath(thumbnailPath);
                    }
                }

                files.Add(file);
            }

            _repository.Value.AddRange(files.ToArray());
            _repository.Value.SaveChanges();

            return files.Select(f => new FileApiModel { Id = f.Id, Path = f.Path, ThumbnailPath = f.ThumbnailPath }).ToList();
        }

        
        public void AssignTempFile(int id, IPrincipal principal, FileCategoryKey category,
            int? advertisementId = null, string cadNum = null, string userId = null, int? agencyId = null)
        {
            _repository.Value.Update(f => f.Id == id, f => new File
                {
                    Category = category,
                    AdvertisementId = advertisementId,
                    CadNum = cadNum,
                    UserId = userId,
                    AgencyId = agencyId,
                    IsTemporary = false
                });
        }

        public EditFileViewModel Edit(int id, IPrincipal principal)
        {
            return _repository.Value
                .Where(f => f.Id == id)
                .Select(f => new EditFileViewModel { Id = f.Id, FileName = f.FileName, Description = f.Description })
                .Single();
        }

        public void Edit(EditFileViewModel edit, IPrincipal principal)
        {
            _repository.Value.Update(f => f.Id == edit.Id, l => new File
                {
                    FileName = edit.FileName,
                    Description = edit.Description
                });
        }

        public void Toggle(int id, bool isDeactivatedNow, IPrincipal principal)
        {
            _repository.Value.Update(f => f.Id == id, f => new File { IsDeactivated = !isDeactivatedNow });
        }

        public void MarkAsDeleted(int id, IPrincipal principal)
        {
            _repository.Value.Update(f => f.Id == id, f => new File { IsDeleted = true });
        }

        public void Sort(int[] ids)
        {
            foreach (var id in ids)
            {
                var sortOrder = Array.IndexOf(ids, id);

                _repository.Value.Update(f => f.Id == id, f => new File { SortOrder = sortOrder });
            }
        }

        public void DeleteAllTempFiles(string webSitePath, int daysBeforeNow)
        {
            if (string.IsNullOrWhiteSpace(webSitePath))
                throw new ArgumentNullException("webSitePath");

            var filesToDelete = _repository.Value
                .Where(f => f.IsTemporary && DbFunctions.DiffDays(f.UploadDate, DateTime.Now) >= daysBeforeNow)
                .Select(f => new { f.Id, f.Path, f.ThumbnailPath })
                .ToList();

            if (filesToDelete.Any())
            {
                var idsToDelete = filesToDelete.Select(f => f.Id).ToArray();
                _repository.Value.Delete(f => idsToDelete.Contains(f.Id));

                filesToDelete.ForEach(f =>
                    {
                        var path = GetPhysicalPath(webSitePath, f.Path);
                        var thumbnailPath = f.ThumbnailPath != null ? GetPhysicalPath(webSitePath, f.ThumbnailPath) : null;

                        System.IO.File.Delete(path);

                        if (thumbnailPath != null)
                            System.IO.File.Delete(thumbnailPath);
                    });
            }
        }

        public void FixMd5(string webSitePath)
        {
            var filesWithoutMd5 = _repository.Value
               .Where(f => string.IsNullOrEmpty(f.Md5Hash))
               .ToList();


            filesWithoutMd5.ForEach(f =>
            {
                string filePath = GetPhysicalPath(webSitePath, f.Path);

                if (System.IO.File.Exists(filePath))
                {
                    f.Md5Hash = getMd5HashForPath(filePath);

                    _repository.Value.Update(f);
                    _repository.Value.SaveChanges();
                }
            });
        }

        #region Helpers

        private static FileTypeKey GetFileType(string extension, out bool needThumbnail)
        {
            var fileType = FileTypeKey.Other;
            needThumbnail = false;

            if (string.IsNullOrEmpty(extension))
                return fileType;

            extension = extension.ToLower().Replace(".", "");

            if (ImagesExtensions.Contains(extension))
            {
                needThumbnail = true;
                fileType = FileTypeKey.Image;
            }

            if (VideoExtensions.Contains(extension))
                fileType = FileTypeKey.Video;

            if (DocsExtensions.Contains(extension))
                fileType = FileTypeKey.Document;

            return fileType;
        }

        private static string GetPhysicalPathFor(FileTypeKey fileType)
        {
            switch (fileType)
            {
                case FileTypeKey.Image:
                    {
                        return HttpContext.Current.Server.MapPath(ImagesPath);
                    }
                case FileTypeKey.Video:
                    {
                        return HttpContext.Current.Server.MapPath(VideoPath);
                    }
                case FileTypeKey.Document:
                    {
                        return HttpContext.Current.Server.MapPath(DocumentsPath);
                    }
                case FileTypeKey.Other:
                default:
                    {
                        return HttpContext.Current.Server.MapPath(OtherPath);
                    }
            }
        }

        private static string GetRelativePath(string physicalPath)
        {
            var appRoot = HttpContext.Current.Request.PhysicalApplicationPath.TrimEnd('\\');

            return physicalPath.Replace(appRoot, string.Empty).Replace('\\', '/');
        }

        private static string GetPhysicalPath(string webSitePath, string relativePath)
        {
            return Path.Combine(webSitePath, relativePath.TrimStart('/').Replace('/', '\\'));
        }

        private static string getMd5HashForPath(string filePath)
        {
            string md5HashSum = "";
            using (var md5 = MD5.Create())
            {
                using (var stream = System.IO.File.OpenRead(filePath))
                {
                    byte[] md5ByteArray = md5.ComputeHash(stream);

                    StringBuilder result = new StringBuilder(md5ByteArray.Length * 2);

                    for (int i = 0; i < md5ByteArray.Length; i++)
                        result.Append(md5ByteArray[i].ToString("X2"));

                    md5HashSum = result.ToString();
                }
            }
            return md5HashSum;
        }


        #endregion
    }
}