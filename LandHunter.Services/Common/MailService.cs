﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using LandHunter.Models;
using LandHunter.Services.MVCMailer;
using LandHunter.ViewModels;
using Mvc.Mailer;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class MailService : CustomMailerBase, IMailService
    {
        private readonly Lazy<IRepository<ServiceOrder, int>> _serviceOrdersRepository;

        public MailService(Lazy<IRepository<ServiceOrder, int>> serviceOrdersRepository)
        {
            _serviceOrdersRepository = serviceOrdersRepository;
        }

        public async Task<ServiceEmailViewModel> GetNewServiceEmailAsync(int serviceOrderId)
        {
            var order = await _serviceOrdersRepository.Value
                .Where(o => o.Id == serviceOrderId)
                .Select(o => new { o.Email })
                .SingleAsync();

            return new ServiceEmailViewModel { ServiceOrderId = serviceOrderId, Recipient = order.Email, Title = "Информация по заказу на сайте www.landhunter.ru" };
        }

        public async Task SendServiceEmailAsync(ServiceEmailViewModel email)
        {
            using (var client = new SmtpClient())
            {
                var message = new MailMessage
                    {
                        Subject = email.Title,
                        Body = email.Text,
                        IsBodyHtml = false
                    };

                message.To.Add(email.Recipient);

                if (email.HttpPostedFile != null && email.HttpPostedFile.Any())
                {
                    foreach (var file in email.HttpPostedFile.Where(f => f != null))
                    {
                        var attachment = new Attachment(file.InputStream, file.FileName, file.ContentType);

                        message.Attachments.Add(attachment);
                    }
                }

                await client.SendMailAsync(message);
            }
        }

        public void SendServiceOrderNotices(ServiceOrder serviceOrder, string[] moderatorsEmails)
        {
            _serviceOrdersRepository.Value.ReferenceLoad(serviceOrder, "Service");

            CreateMessage(
                subject: "Landhunter.ru - заказ услуги «" + serviceOrder.ServiceName + "»",
                recipients: string.Join(",", moderatorsEmails),
                view: "~/Areas/Admin/Views/EmailTemplates/ServiceOrderModeratorNotice.cshtml",
                model: serviceOrder).Send();

            CreateMessage(
                subject: "Информация по заказу на сайте www.landhunter.ru",
                recipients: serviceOrder.Email,
                view: "~/Areas/Admin/Views/EmailTemplates/ServiceOrderRealtorNotice.cshtml",
                model: serviceOrder).Send();
        }

        public void SendServiceOrderPaidUpNotices(ServiceOrder serviceOrder, string[] moderatorsEmails)
        {
            _serviceOrdersRepository.Value.ReferenceLoad(serviceOrder, "Service");

            CreateMessage(
                subject: "Landhunter.ru - заказ услуги «" + serviceOrder.ServiceName + "»",
                recipients: string.Join(",", moderatorsEmails),
                view: "~/Areas/Admin/Views/EmailTemplates/ServiceOrderPaidUpModeratorNotice.cshtml",
                model: serviceOrder).Send();

            CreateMessage(
                subject: "Информация по заказу на сайте www.landhunter.ru",
                recipients: serviceOrder.Email,
                view: "~/Areas/Admin/Views/EmailTemplates/ServiceOrderPaidUpRealtorNotice.cshtml",
                model: serviceOrder).Send();
        }

        private MvcMailMessage CreateMessage(string subject, string recipients, string view, object model = null, Dictionary<string, MemoryStream> linkedResources = null)
        {
            if (model != null)
                ViewData.Model = model;

            var mvcMailMessage = new MvcMailMessage
                {
                    Subject = subject,
                    ViewName = view,
                };

            mvcMailMessage.To.Add(recipients);

            PopulateBody((MailMessage)mvcMailMessage, mvcMailMessage.ViewName, mvcMailMessage.MasterName, linkedResources);
            
            return mvcMailMessage;
        }
    }
}
