﻿using System;
using System.Linq;
using LandHunter.Models;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class RatingService : IRatingService
    {
        private readonly Lazy<IRepository<Realtor, string>> _realtorsRepository;

        public RatingService(Lazy<IRepository<Realtor, string>> realtorsRepository)
        {
            _realtorsRepository = realtorsRepository;
        }

        public void Calculate()
        {
            var realtors = _realtorsRepository.Value
                .Where(r => !r.User.IsDeleted)
                .Select(r => new
                    {
                        r.Id,
                        r.ContinuedVisitsCount,
                        r.LastVisitDate,
                        ActiveAdvertisementsCount = r.Advertisements.Count(a => !a.IsSoldOut && !a.IsDeleted),
                        r.RatingBonus
                    })
                .ToList();

            foreach (var realtor in realtors)
            {
                var rating = CalculateRating(realtor.ContinuedVisitsCount, realtor.LastVisitDate, realtor.ActiveAdvertisementsCount, realtor.RatingBonus);

                _realtorsRepository.Value.Update(r => r.Id == realtor.Id, r => new Realtor { Rating = rating });
            }
        }

        private static int CalculateRating(int continuedVisitsCount, DateTime lastVisitDate, int activeAdvertisementsCount, int ratingBonus)
        {
            var visitsBonus = 0;

            if (lastVisitDate.Date == DateTime.Today.Date && continuedVisitsCount >= 2)
                visitsBonus = (continuedVisitsCount - 1) > 21 ? 273 : 13;
            else if (lastVisitDate.Date == DateTime.Today.Date.AddDays(-1))
                visitsBonus = continuedVisitsCount > 21 ? 273 : 13;
            else if (lastVisitDate.Date < DateTime.Today.Date.AddDays(-1))
                visitsBonus = -13;

            var advertisementsBonus = 0m;

            for (var a = 1; a <= activeAdvertisementsCount; a++)
            {
                if (a <= 10)
                {
                    advertisementsBonus += 5;
                }
                else if (a > 10 && a <= 50)
                {
                    advertisementsBonus += 3;
                }
                else if (a > 50 && a <= 100)
                {
                    advertisementsBonus += 2;
                }
                else if (a > 100 && a <= 500)
                {
                    advertisementsBonus += 0.5m;
                }
                else if (a > 500)
                {
                    advertisementsBonus += 100;
                    break;
                }
            }

            return 30 + visitsBonus + decimal.ToInt32(advertisementsBonus) + ratingBonus;
        }
    }
}
