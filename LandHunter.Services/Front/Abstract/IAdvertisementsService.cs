using System.Security.Principal;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface IAdvertisementsService
    {
        GridApiModel<AdvertisementApiModel> GetList(AdvertisementsGridBindingModel advertisementsGrid, IPrincipal principal);
        AdvertisementApiModel Get(string cadNum, IPrincipal principal);
        AdvertisementApiModel ById(int id);

        void Update(AdvertisementBindingModel advertisement, IPrincipal principal);
        void Create(AdvertisementBindingModel advertisement, IPrincipal principal);
        void Sell(int id, IPrincipal principal);
        void Delete(int id, IPrincipal principal);
    }
}