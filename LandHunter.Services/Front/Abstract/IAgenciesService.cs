using System.Collections.Generic;
using System.Security.Principal;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface IAgenciesService
    {
        AgencyApiModel Get(int id, IPrincipal principal);
        IReadOnlyCollection<AgencyApiModel> GetList(IPrincipal principal);
        IReadOnlyCollection<AgencyApiModel> Autocomplete(string startsWith, IPrincipal principal);
        AgencyApiModel Create(AgencyBindingModel agency, IPrincipal principal);
        void Update(EditAgencyBindingModel agency, IPrincipal principal);
    }
}