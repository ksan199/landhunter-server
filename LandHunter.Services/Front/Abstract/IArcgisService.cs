﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandHunter.Services
{
    public interface IArcgisService
    {
        JObject Query(string queryString);
        JObject Get(string queryString);
    }
}
