﻿using System.Collections.Generic;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface IClustersService
    {
        IEnumerable<ClusterApiModel> GetLandsClusters(LandsExtentBindingModel landsExtent);
    }
}