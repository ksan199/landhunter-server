using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface IDictionariesService
    {
        DictionariesApiModel Get();
    }
}