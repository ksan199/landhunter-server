using System.Linq;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface IGridService<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> Query { get; }
        int? TotalItems { get; }
        int? TotalPages { get; }
        bool? HasPreviousPage { get; }
        bool? HasNextPage { get; }

        IGridService<TEntity> Init(IQueryable<TEntity> query, GridSettingsBindingModel gridSettings);
        IQueryable<TEntity> ApplyFilter(string key = null);
        IQueryable<TEntity> ApplySorting();
        IQueryable<TEntity> ApplyPaging();
        IQueryable<TEntity> Apply();
    }
}