﻿using System.Collections.Generic;
using System.Security.Principal;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;

namespace LandHunter.Services
{
    public interface ILandsService
    {
        LandApiModel Get(string cadNum, LandDataComposition dataComposition);
        Dictionary<string, object> GetFilterSettings();
        IReadOnlyCollection<LandApiModel> Filter(LandsGridBindingModel landsGrid);
        IReadOnlyCollection<string> GetFavourites(IPrincipal principal);
        void AddToFavourites(string cadNum, IPrincipal principal);
        void RemoveFromFavourites(string cadNum, IPrincipal principal);
    }
}