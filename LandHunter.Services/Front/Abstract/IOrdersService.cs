﻿using System.Collections.Generic;
using System.Security.Principal;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public  interface IOrdersService
    {
        OrderApiModel Create(OrderBindingModel order, IPrincipal principal);
        IReadOnlyCollection<OrderApiModel> GetLast(int? howMany, IPrincipal principal);
        YaKassaResponseApiModel CheckOrder(YaKassaRequestApiModel request);
        YaKassaResponseApiModel PaymentAviso(YaKassaRequestApiModel request);
        string GenerateAvitoAdvertisements(string webSitePath);
        string GenerateCianAdvertisements(string webSitePath);
        string GenerateIrrAdvertisements(string webSitePath, string userId);
    }
}