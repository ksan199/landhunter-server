using System.Security.Principal;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface IRealtorsService
    {
        RealtorsGridApiModel GetList(RealtorsGridBindingModel realtorsGrid, IPrincipal principal);
        RealtorApiModel Get(string id, IPrincipal principal);
        void Update(RealtorBindingModel realtor, IPrincipal principal);

        void CheckVisit(IPrincipal principal);
    }
}