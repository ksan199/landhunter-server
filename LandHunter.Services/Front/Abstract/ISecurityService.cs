using System.Security.Principal;
using System.Threading.Tasks;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface ISecurityService
    {
        Task RegisterAsync(RegistrationBindingModel registration);
        Task ResetPasswordAsync(ResetPasswordBindingModel resetPassword);
        Task ChangePasswordAsync(ChangePasswordBindingModel changePassword, IPrincipal principal);
        Task<bool> VerifyUser(string phoneNumber);
    }
}