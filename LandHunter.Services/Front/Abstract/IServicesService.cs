﻿using System.Collections.Generic;
using System.Security.Principal;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface IServicesService
    {
        IReadOnlyCollection<ServiceCategoryApiModel> GetList();
        void Create(ServiceOrderBindingModel order, IPrincipal principal);
        void ProcessSuspendedOrders(string realtorId, decimal orderSumAmount);
    }
}