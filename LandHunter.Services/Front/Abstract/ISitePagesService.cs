using System.Collections.Generic;
using LandHunter.ApiModels;

namespace LandHunter.Services
{
    public interface ISitePagesService
    {
        SitePageApiModel Get(string pageUrl);
        IReadOnlyCollection<SitePageApiModel> GetList();
    }
}