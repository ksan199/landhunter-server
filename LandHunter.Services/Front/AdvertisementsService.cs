﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Repository;
using System.Data.Entity.Spatial;

namespace LandHunter.Services
{
    public class AdvertisementsService : IAdvertisementsService
    {
        private readonly Lazy<IRepository<Advertisement, int>> _advertisementsRepository;
        private readonly Lazy<IRepository<AdvertisementExtended, int>> _advertisementsExtendedRepository;
        private readonly Lazy<IRepository<AdvertisementStatus, int>> _advertisementStatusesRepository;
        private readonly Lazy<IRepository<Land, string>> _landsRepository;
        private readonly Lazy<IRepository<AdvertisementLog, int>> _logsRepository;
        private readonly Lazy<IGridService<AdvertisementExtended>> _gridService;
        private readonly Lazy<IFilesService> _filesService;
        private readonly Lazy<IExternalService> _externalService;

        public AdvertisementsService(
            Lazy<IRepository<Advertisement, int>> advertisementsRepository,
            Lazy<IRepository<AdvertisementExtended, int>> advertisementsExtendedRepository,
            Lazy<IRepository<AdvertisementStatus, int>> advertisementStatusesRepository,
            Lazy<IRepository<Land, string>> landsRepository,
            Lazy<IRepository<AdvertisementLog, int>> logsRepository,
            Lazy<IGridService<AdvertisementExtended>> gridService,
            Lazy<IFilesService> filesService,
            Lazy<IExternalService> externalService)
        {
            _advertisementsRepository = advertisementsRepository;
            _advertisementsExtendedRepository = advertisementsExtendedRepository;
            _advertisementStatusesRepository = advertisementStatusesRepository;
            _landsRepository = landsRepository;
            _logsRepository = logsRepository;
            _gridService = gridService;
            _filesService = filesService;
            _externalService = externalService;
        }

        public GridApiModel<AdvertisementApiModel> GetList(AdvertisementsGridBindingModel advertisementsGrid, IPrincipal principal)
        {
            var grid = new GridApiModel<AdvertisementApiModel>();

            var currentUserId = principal.Identity.GetUserId();

            advertisementsGrid.RealtorId = advertisementsGrid.RealtorId ?? currentUserId;

            var query = _advertisementsExtendedRepository.Value
                .Where(a => a.RealtorId == advertisementsGrid.RealtorId)
                .Where(a => advertisementsGrid.State == AdvertisementState.All
                    || (advertisementsGrid.State == AdvertisementState.Active
                        ? !a.IsDeleted && !a.IsSoldOut
                        : (advertisementsGrid.State == AdvertisementState.SoldOut
                            ? a.IsSoldOut && !a.IsDeleted
                            : advertisementsGrid.State == AdvertisementState.Deleted && a.IsDeleted)))
                .OrderBy(a => a.Id).AsQueryable();

            if (advertisementsGrid.GridSettings != null)
            {
                var gridService = _gridService.Value.Init(query, advertisementsGrid.GridSettings);

                query = gridService.Apply();

                grid.TotalItems = gridService.TotalItems;
                grid.TotalPages = gridService.TotalPages;
                grid.HasPreviousPage = gridService.HasPreviousPage;
                grid.HasNextPage = gridService.HasNextPage;
            }

            grid.List = query
                .Select(a => new
                    {
                        a.Id,
                        a.RealtorId,
                        a.CadNum,
                        a.Land.Address,
                        a.Land.Utilization,
                        a.Land.CategoryCode,
                        a.Land.Area,
                        a.Land.ViewsCount,
                        a.Price,
                        a.CurrencyId,
                        a.CreateDate,
                        a.IsSoldOut,
                        a.IsDeleted
                    })
                .ToList()
                .Select(a => new AdvertisementApiModel
                    {
                        Id = a.Id,
                        CadNum = a.CadNum,
                        Address = a.Address,
                        Utilization = a.Utilization,
                        CategoryCode = a.CategoryCode,
                        Area = a.Area,
                        Price = a.Price,
                        CurrencyId = a.CurrencyId,
                        CreateDate = a.CreateDate,
                        State = a.IsDeleted
                            ? AdvertisementState.Deleted
                            : (a.IsSoldOut ? AdvertisementState.SoldOut : AdvertisementState.Active),
                        // Убрал проверку на идентификатор риелтора
                        ViewsCount = a.ViewsCount
                    })
                .ToList();

            return grid;
        }

        public AdvertisementApiModel Get(string cadNum, IPrincipal principal)
        {
            LandsHelper.NormalizeCadNum(ref cadNum);

            var advertisement = new AdvertisementApiModel();

            if (_landsRepository.Value.GetQuery().All(l => l.CadNum != cadNum))
            {
                advertisement.LandStatus = LandStatus.NotExist;
            }
            else
            {
                var currentUserId = principal.Identity.GetUserId();

                var land = _landsRepository.Value
                    .Where(l => l.CadNum == cadNum)
                    .Select(l => new
                    {
                        l.CadNum,
                        l.Address,
                        l.Description,
                        l.Utilization,
                        l.CategoryCode,
                        l.GasPipeline,
                        l.WaterPipeline,
                        l.PowerLine,
                        l.Sand,
                        l.Area,
                        l.CadastrePrice,
                        Images = l.Files.Where(f => f.Type == FileTypeKey.Image && !f.IsDeleted && !f.IsDeactivated)
                            .OrderBy(f => f.SortOrder).ThenByDescending(f => f.UploadDate)
                            .Select(f => new { f.FileName, f.Description, f.Path, f.ThumbnailPath }).ToList(),
                        Advertisement = l.Advertisements
                            .Where(a => !a.IsDeleted && !a.IsSoldOut && a.RealtorId == currentUserId)
                            .Select(a => new
                            {
                                a.Id,
                                a.CurrencyId,
                                a.Price,
                                a.ShowCreateDate, 
                                Documents = a.Files.Where(f => f.Category == FileCategoryKey.Document && !f.IsDeleted && !f.IsDeactivated)
                                    .OrderBy(f => f.SortOrder).ThenByDescending(f => f.UploadDate)
                                    .Select(f => new { f.FileName, f.Extension, f.Description, f.Path, f.ThumbnailPath }).ToList()
                            })
                            .FirstOrDefault()
                    })
                    .Single();

                advertisement.CadNum = land.CadNum;
                advertisement.Address = land.Address;
                advertisement.Description = land.Description;
                advertisement.Utilization = land.Utilization;
                advertisement.CategoryCode = land.CategoryCode;
                advertisement.GasPipeline = land.GasPipeline ? true : (bool?)null;
                advertisement.WaterPipeline = land.WaterPipeline ? true : (bool?)null;
                advertisement.PowerLine = land.PowerLine ? true : (bool?)null;
                advertisement.Sand = land.Sand ? true : (bool?)null;
                advertisement.Area = land.Area;
                advertisement.CadastrePrice = land.CadastrePrice;
                advertisement.Images = land.Images.Any()
                    ? land.Images.Select(i => new FileApiModel { FileName = i.FileName, Description = i.Description, Path = i.Path, ThumbnailPath = i.ThumbnailPath })
                    : null;

                if (land.Advertisement != null)
                {
                    advertisement.LandStatus = LandStatus.HasAdvertisement;
                    advertisement.Id = land.Advertisement.Id;
                    advertisement.Price = land.Advertisement.Price;
                    advertisement.CurrencyId = land.Advertisement.CurrencyId;
                    advertisement.ShowCreateDate = land.Advertisement.ShowCreateDate;
                    advertisement.Documents = land.Advertisement.Documents.Any()
                        ? land.Advertisement.Documents.Select(i => new FileApiModel { FileName = i.FileName + i.Extension, Description = i.Description, Path = i.Path, ThumbnailPath = i.ThumbnailPath })
                        : null;
                }
                else
                {
                    advertisement.LandStatus = LandStatus.Exist;
                }
            }

            return advertisement;
        }


        public AdvertisementApiModel ById(int id)
        {
            var advertisement = new AdvertisementApiModel();
            try
            {
                var tmp = _advertisementsRepository.Value.Where(a => a.Id == id).Select(a => new
                {
                    a.Id,
                    a.CadNum
                }).Single();

                advertisement.Id = tmp.Id;
                advertisement.CadNum = tmp.CadNum;
            }
            catch (Exception e)
            {
                return null;
            }
            
            return advertisement;
        }

        public void Update(AdvertisementBindingModel advertisement, IPrincipal principal)
        {
            var normalizedCadNum = advertisement.CadNum;
            LandsHelper.NormalizeCadNum(ref normalizedCadNum);

            var currentUserId = principal.Identity.GetUserId();

            var modifiedStatusId = _advertisementStatusesRepository.Value
                .Where(s => s.Key == AdvertisementStatusKey.Modified).Select(s => s.Id).Single();

            var existingAdvertisement = _advertisementsRepository.Value
                .Where(a => a.Id == advertisement.Id && a.CadNum == advertisement.CadNum && a.RealtorId == currentUserId)
                .Single();

            var log = StartLog(currentUserId, advertisement.Id, normalizedCadNum, LogActionType.Updated);
            AppendToLog(log, "Данные объявления:");
            AppendToLog(log, "- Цена: " + existingAdvertisement.Price + " => " + advertisement.Price);
            AppendToLog(log, "- Id валюты: " + existingAdvertisement.CurrencyId + " => " + advertisement.CurrencyId);
            AppendToLog(log, "- Адрес: " + existingAdvertisement.Address + " => " + advertisement.Address);
            AppendToLog(log, "- Вид использования: " + existingAdvertisement.Utilization + " => " + advertisement.Utilization);
            AppendToLog(log, "- Категория: " + existingAdvertisement.CategoryCode + " => " + advertisement.CategoryCode);
            AppendToLog(log, "- Описание: " + existingAdvertisement.Description + " => " + advertisement.Description);
            AppendToLog(log, "- Площадь: " + existingAdvertisement.Area + " => " + advertisement.Area);
            AppendToLog(log, "- Газовые сети: " + existingAdvertisement.GasPipeline + " => " + advertisement.GasPipeline);
            AppendToLog(log, "- Водопровод: " + existingAdvertisement.WaterPipeline + " => " + advertisement.WaterPipeline);
            AppendToLog(log, "- Электроснабжение: " + existingAdvertisement.PowerLine + " => " + advertisement.PowerLine);
            AppendToLog(log, "- Песок: " + existingAdvertisement.Sand + " => " + advertisement.Sand);

            existingAdvertisement.AdvertisementStatusId = modifiedStatusId;
            existingAdvertisement.Price = advertisement.Price;
            existingAdvertisement.CurrencyId = advertisement.CurrencyId;
            existingAdvertisement.Address = advertisement.Address;
            existingAdvertisement.Utilization = advertisement.Utilization;
            existingAdvertisement.CategoryCode = advertisement.CategoryCode;
            existingAdvertisement.Description = advertisement.Description;
            existingAdvertisement.Area = advertisement.Area;
            existingAdvertisement.GasPipeline = advertisement.GasPipeline;
            existingAdvertisement.WaterPipeline = advertisement.WaterPipeline;
            existingAdvertisement.PowerLine = advertisement.PowerLine;
            existingAdvertisement.Sand = advertisement.Sand;
            existingAdvertisement.CreateDate = DateTime.Now;
            existingAdvertisement.ShowCreateDate = advertisement.ShowCreateDate;

            _advertisementsRepository.Value.Update(existingAdvertisement);
            _advertisementsRepository.Value.SaveChanges();

            var land = _landsRepository.Value
                .Where(l => l.CadNum == normalizedCadNum)
                .Select(l => new
                    {
                        l.Description,
                        ExistImages = l.Files.Any(f => !f.IsDeleted && !f.IsDeactivated && f.Category == FileCategoryKey.Image),
                        HasSingleAdvertisement = l.Advertisements.All(a => a.Id == existingAdvertisement.Id)
                    })
                .Single();

            if (land.Description == null && advertisement.Description != null)
            {
                _landsRepository.Value.Update(l => l.CadNum == normalizedCadNum, l => new Land { Description = advertisement.Description });

                AppendToLog(log, "У участка изменено описание на описание из объявления.");
            }

            if (advertisement.DocumentsIds != null && advertisement.DocumentsIds.Any())
            {
                foreach (var fileId in advertisement.DocumentsIds)
                    _filesService.Value.AssignTempFile(fileId, principal, FileCategoryKey.Document, advertisement.Id);

                AppendToLog(log, "Загружены документы:");
                AppendToLog(log, "- Id файлов: " + string.Join(", ", advertisement.DocumentsIds));
            }

            if (advertisement.ImagesIds != null && advertisement.ImagesIds.Any())
            {
                var imagesToLand = new List<int>();

                foreach (var fileId in advertisement.ImagesIds)
                {
                    var assignToLand = !land.ExistImages || land.HasSingleAdvertisement;

                    _filesService.Value.AssignTempFile(
                        fileId,
                        principal,
                        FileCategoryKey.Image,
                        advertisementId: advertisement.Id,
                        cadNum: assignToLand ? normalizedCadNum : null);

                    if (assignToLand)
                        imagesToLand.Add(fileId);
                }

                AppendToLog(log, "Загружены изображения:");
                AppendToLog(log, "- Id файлов: " + string.Join(", ", advertisement.ImagesIds));

                if (imagesToLand.Any())
                    AppendToLog(log, "- Id файлов перенесенные в участок: " + string.Join(", ", advertisement.ImagesIds));
            }

            SaveLog(log);
        }

        public void Create(AdvertisementBindingModel advertisement, IPrincipal principal)
        {
            var normalizedCadNum = advertisement.CadNum;
            LandsHelper.NormalizeCadNum(ref normalizedCadNum);

            var currentUserId = principal.Identity.GetUserId();

            if (_advertisementsRepository.Value.GetQuery().Any(a => a.CadNum == normalizedCadNum && !a.IsSoldOut && !a.IsDeleted && a.RealtorId == currentUserId))
                throw new Exception("У риелтора уже есть объявление по данному участку.");

            var log = StartLog(currentUserId, 0, normalizedCadNum, LogActionType.Created);

            var land = _landsRepository.Value
                .Where(l => l.CadNum == normalizedCadNum)
                .Select(l => new { l.Description, ExistImages = l.Files.Any(f => !f.IsDeleted && !f.IsDeactivated && f.Category == FileCategoryKey.Image) })
                .SingleOrDefault();

            if (land == null)
            {
                var cadastre = _externalService.Value.GetCadastreInfo(normalizedCadNum);

                if (cadastre == null)
                    throw new Exception(string.Format("Участок с кадастровым номером {0} не найден.", normalizedCadNum));

                
                var newLand = new Land
                    {
                        CadNum = normalizedCadNum,
                        RegionId = int.Parse(normalizedCadNum.Split(':').First()),
                        Address = cadastre.Address,
                        Utilization = cadastre.Utilization,
                        CategoryCode = cadastre.CategoryCode,
                        Area = cadastre.Area,
                        CadastrePrice = cadastre.Price,
                        Description = advertisement.Description,
                        GasPipeline = advertisement.GasPipeline,
                        WaterPipeline = advertisement.WaterPipeline,
                        PowerLine = advertisement.PowerLine,
                        Sand = advertisement.Sand,
                        UpdateDate = DateTime.Now,
                        CreateDate = DateTime.Now,
                        CentrPoint = _externalService.Value.GetCentroid(normalizedCadNum)
                    };

                AppendToLog(log, "Cоздан новый участок.");

                if (advertisement.Address != null && advertisement.Address != cadastre.Address)
                {
                    newLand.Address = advertisement.Address;
                    newLand.AddressChanged = true;

                    AppendToLog(log, "У участка изменен адрес на адрес из объявления.");
                }

                if (advertisement.Area.HasValue && advertisement.Area.Value != cadastre.Area)
                {
                    newLand.Area = advertisement.Area.Value;
                    newLand.AreaChanged = true;

                    AppendToLog(log, "У участка изменена площадь на площадь из объявления.");
                }

                _landsRepository.Value.Add(newLand);
                _landsRepository.Value.SaveChanges();
            }
            else
            {
                if (land.Description == null && advertisement.Description != null)
                {
                    _landsRepository.Value.Update(l => l.CadNum == normalizedCadNum, l => new Land { Description = advertisement.Description });

                    AppendToLog(log, "У участка изменено описание на описание из объявления.");
                }
            }

            var newStatusId = _advertisementStatusesRepository.Value
                .Where(s => s.Key == AdvertisementStatusKey.New).Select(s => s.Id).Single();

            var newAdvertisement = new Advertisement
                {
                    RealtorId = currentUserId,
                    AdvertisementStatusId = newStatusId,
                    Price = advertisement.Price,
                    CurrencyId = advertisement.CurrencyId,
                    CadNum = normalizedCadNum,
                    Address = advertisement.Address,
                    Utilization = advertisement.Utilization,
                    CategoryCode = advertisement.CategoryCode,
                    Description = advertisement.Description,
                    Area = advertisement.Area,
                    GasPipeline = advertisement.GasPipeline,
                    WaterPipeline = advertisement.WaterPipeline,
                    PowerLine = advertisement.PowerLine,
                    Sand = advertisement.Sand,
                    CreateDate = DateTime.Now
                };

            _advertisementsRepository.Value.Add(newAdvertisement);
            _advertisementsRepository.Value.SaveChanges();

            log.AdvertisementId = newAdvertisement.Id;
            AppendToLog(log, "Данные объявления:");
            AppendToLog(log, "- Цена: " + newAdvertisement.Price);
            AppendToLog(log, "- Id валюты: " + newAdvertisement.CurrencyId);
            AppendToLog(log, "- Адрес: " + newAdvertisement.Address);
            AppendToLog(log, "- Вид использования: " + newAdvertisement.Utilization);
            AppendToLog(log, "- Категория: " + newAdvertisement.CategoryCode);
            AppendToLog(log, "- Описание: " + newAdvertisement.Description);
            AppendToLog(log, "- Площадь: " + newAdvertisement.Area);
            AppendToLog(log, "- Газовые сети: " + newAdvertisement.GasPipeline);
            AppendToLog(log, "- Водопровод: " + newAdvertisement.WaterPipeline);
            AppendToLog(log, "- Электроснабжение: " + newAdvertisement.PowerLine);
            AppendToLog(log, "- Песок: " + newAdvertisement.Sand);

            if (advertisement.DocumentsIds != null && advertisement.DocumentsIds.Any())
            {
                foreach (var fileId in advertisement.DocumentsIds)
                    _filesService.Value.AssignTempFile(fileId, principal, FileCategoryKey.Document, newAdvertisement.Id);

                AppendToLog(log, "Загружены документы:");
                AppendToLog(log, "- Id файлов: " + string.Join(", ", advertisement.DocumentsIds));
            }

            if (advertisement.ImagesIds != null && advertisement.ImagesIds.Any())
            {
                var imagesToLand = new List<int>();

                foreach (var fileId in advertisement.ImagesIds)
                {
                    var assignToLand = land == null || !land.ExistImages;

                    _filesService.Value.AssignTempFile(
                        fileId,
                        principal,
                        FileCategoryKey.Image,
                        advertisementId: newAdvertisement.Id,
                        cadNum: assignToLand ? normalizedCadNum : null);

                    if (assignToLand)
                        imagesToLand.Add(fileId);
                }

                AppendToLog(log, "Загружены изображения:");
                AppendToLog(log, "- Id файлов: " + string.Join(", ", advertisement.ImagesIds));

                if (imagesToLand.Any())
                    AppendToLog(log, "- Id файлов перенесенные в участок: " + string.Join(", ", advertisement.ImagesIds));
            }

            SaveLog(log);
        }

        public void Sell(int id, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            if (!_advertisementsRepository.Value.GetQuery().Any(a => !a.IsDeleted && a.Id == id && a.RealtorId == currentUserId))
                throw new Exception("Объявление не найдено, удалено или принадлежит другому пользователю.");

            _advertisementsRepository.Value.Update(a => a.Id == id, a => new Advertisement { IsSoldOut = true });

            Log(currentUserId, id, _advertisementsRepository.Value.Where(a => a.Id == id).Select(a => a.CadNum).SingleOrDefault(), LogActionType.SoldOut);
        }

        public void Delete(int id, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            if (!_advertisementsRepository.Value.GetQuery().Any(a => !a.IsDeleted && a.Id == id && a.RealtorId == currentUserId))
                throw new Exception("Объявление не найдено, удалено или принадлежит другому пользователю.");

            _advertisementsRepository.Value.Update(a => a.Id == id, a => new Advertisement { IsDeleted = true });

            Log(currentUserId, id, _advertisementsRepository.Value.Where(a => a.Id == id).Select(a => a.CadNum).SingleOrDefault(), LogActionType.Deleted);
        }

        #region Log Helper Methods

        private void Log(string userId, int advertisementId, string cadNum, LogActionType action, string description = null)
        {
            var log = StartLog(userId, advertisementId, cadNum, action, description);

            SaveLog(log);
        }

        private AdvertisementLog StartLog(string userId, int advertisementId, string cadNum, LogActionType action, string description = null)
        {
            return new AdvertisementLog
                {
                    UserId = userId,
                    AdvertisementId = advertisementId,
                    CadNum = cadNum,
                    Action = action,
                    Description = description
                };
        }

        private AdvertisementLog AppendToLog(AdvertisementLog log, string description)
        {
            log.Description += (log.Description != null ? Environment.NewLine : null) + description;

            return log;
        }

        private void SaveLog(AdvertisementLog log)
        {
            try
            {
                log.Date = DateTime.Now;

                _logsRepository.Value.Add(log);
                _logsRepository.Value.SaveChanges();
            }
            catch
            {
            }
        }

        #endregion
    }
}