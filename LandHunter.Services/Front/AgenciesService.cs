﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class AgenciesService : IAgenciesService
    {
        private readonly Lazy<IRepository<Agency, int>> _agenciesRepository;
        private readonly Lazy<IRepository<File, int>> _filesRepository;
        private readonly Lazy<IFilesService> _filesService;

        public AgenciesService(
            Lazy<IRepository<Agency, int>> agenciesRepository,
            Lazy<IRepository<File, int>> filesRepository,
            Lazy<IFilesService> filesService)
        {
            _agenciesRepository = agenciesRepository;
            _filesRepository = filesRepository;
            _filesService = filesService;
        }

        public AgencyApiModel Get(int id, IPrincipal principal)
        {
            return _agenciesRepository.Value
                .Where(a => !a.IsDeleted && a.Id == id)
                .Select(a => new AgencyApiModel
                    {
                        Id = a.Id,
                        Name = a.Name,
                        OGRN = a.OGRN,
                        Address = a.Address,
                        Phone = a.Phone,
                        Email = a.Email,
                        Website = a.Website,
                        Description = a.Description,
                        Image = a.Files
                            .Where(f => !f.IsDeleted && !f.IsDeactivated)
                            .Select(f => new FileApiModel { Id = f.Id, ThumbnailPath = f.ThumbnailPath, Path = f.Path })
                            .FirstOrDefault()
                    })
                .SingleOrDefault();
        }

        public IReadOnlyCollection<AgencyApiModel> GetList(IPrincipal principal)
        {
            return _agenciesRepository.Value
                .Where(a => !a.IsDeleted)
                .OrderBy(a => a.Name)
                .Select(a => new AgencyApiModel
                    {
                        Id = a.Id,
                        Name = a.Name
                    })
                .ToList();
        }

        public IReadOnlyCollection<AgencyApiModel> Autocomplete(string startsWith, IPrincipal principal)
        {
            var agenciesList = new List<AgencyApiModel>();

            if (!string.IsNullOrWhiteSpace(startsWith))
            {
                startsWith = startsWith.ToLower();

                agenciesList = _agenciesRepository.Value
                    .Where(a => !a.IsDeleted && a.Name.ToLower().StartsWith(startsWith))
                    .OrderBy(a => a.Name)
                    .Select(a => new AgencyApiModel
                        {
                            Id = a.Id,
                            Name = a.Name
                        })
                    .ToList();
            }

            return agenciesList;
        }

        public AgencyApiModel Create(AgencyBindingModel agency, IPrincipal principal)
        {
            var newAgency = new Agency
                {
                    Name = agency.Name,
                    OGRN = agency.OGRN,
                    Address = agency.Address,
                    Phone = agency.Phone,
                    Email = agency.Email,
                    Website = agency.Website,
                    Description = agency.Description,
                    UserId = principal.Identity.GetUserId()
                };

            _agenciesRepository.Value.Add(newAgency);
            _agenciesRepository.Value.SaveChanges();

            if (agency.ImageId.HasValue)
                _filesService.Value.AssignTempFile(agency.ImageId.Value, principal, FileCategoryKey.Image, agencyId: newAgency.Id);

            return new AgencyApiModel { Id = newAgency.Id };
        }

        public void Update(EditAgencyBindingModel agency, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            var existingAgency = _agenciesRepository.Value
                .Where(a => a.Id == agency.Id && a.UserId == currentUserId && !a.IsDeleted)
                .Single();

            existingAgency.Name = agency.Name;
            existingAgency.OGRN = agency.OGRN;
            existingAgency.Address = agency.Address;
            existingAgency.Phone = agency.Phone;
            existingAgency.Email = agency.Email;
            existingAgency.Website = agency.Website;
            existingAgency.Description = agency.Description;

            _agenciesRepository.Value.Update(existingAgency);
            _agenciesRepository.Value.SaveChanges();

            var currentImageId = existingAgency.Files.Where(f => !f.IsDeleted && !f.IsDeactivated).Select(f => (int?)f.Id).FirstOrDefault();

            if (agency.ImageId != currentImageId)
            {
                if (currentImageId.HasValue)
                    _filesService.Value.MarkAsDeleted(currentImageId.Value, principal);

                if (agency.ImageId.HasValue)
                    _filesService.Value.AssignTempFile(agency.ImageId.Value, principal, FileCategoryKey.Image, agencyId: agency.Id);
            }
        }
    }
}