﻿using LandHunter.Common.Helpers;
using LandHunter.DataAccess.Context;
using LandHunter.Models;
using Newtonsoft.Json.Linq;
using SharpEngine.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace LandHunter.Services
{
    public class ArcgisService : IArcgisService
    {

        //private readonly Lazy<IRepository<ArcgisLog, int>> arcgisLog;
        private readonly Lazy<LandHunterContext> dbContext;
        private readonly string filterPattern;


        public ArcgisService(
            //Lazy<IRepository<ArcgisLog, int>> _arcgisLog,
            Lazy<LandHunterContext> _dbContext
        )
        {
            //arcgisLog = _arcgisLog;
            dbContext = _dbContext;
            filterPattern = AppConfigHelper.GetSetting("SHGPattern", "");
        }

        private string arcgisQuery = "http://localhost:8081/arcgis/rest/services/LH/Lands/MapServer/0/query?";
        private string arcgisUrl = "http://localhost:8081/arcgis/rest/services/LH/Lands/MapServer/0?";

        //private string arcgisQuery = "https://www.landhunter.ru/shg/arcgis/rest/services/LH/Lands/MapServer/0/query?";
        //private string arcgisUrl = "https://www.landhunter.ru/shg/arcgis/rest/services/LH/Lands/MapServer/0?";
        
        public JObject GetQuery(string arcgisUrl, string queryString)
        {

            //if (queryString.IndexOf("outFields=*") != -1 && queryString.IndexOf("1%3d1") != -1)
            if (Regex.IsMatch(queryString, filterPattern))
            {
                throw new System.Exception { 

                };
            }
            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(arcgisUrl + queryString);
            wrGETURL.ContentType = "application/json; charset=utf-8";

            WebProxy myProxy = new WebProxy("myproxy", 80);
            myProxy.BypassProxyOnLocal = true;

            wrGETURL.Proxy = WebProxy.GetDefaultProxy();

            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();

            StreamReader objReader = new StreamReader(objStream);

            string sLine = "", response = "";
            int i = 0;

            while (sLine != null)
            {
                i++;
                sLine = objReader.ReadLine();
                if (sLine != null)
                    response += sLine;
            }
            JObject jObj = JObject.Parse(response);
            return jObj;
        }

        protected string GetUserIP()
        {
            string VisitorsIPAddr = string.Empty;
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
            }

            return VisitorsIPAddr;
        }

        private ArcgisLog WriteLog(string queryString) 
        {
            ArcgisLog log = new ArcgisLog();
            log.request = queryString;
            log.ip = GetUserIP();
            log.timeStart = DateTime.Now;
            log.timeEnd = DateTime.Now;

            dbContext.Value.Configuration.ValidateOnSaveEnabled = false;
            dbContext.Value.Arcgis.Add(log);
            dbContext.Value.SaveChanges();

            return log;
        }

        private void UpdateLog(ArcgisLog log)
        {
            bool oldValidateOnSaveEnabled = dbContext.Value.Configuration.ValidateOnSaveEnabled;

            try
            {
                dbContext.Value.Configuration.ValidateOnSaveEnabled = false;

                ArcgisLog _log = dbContext.Value.Arcgis.Where(c => c.Id == log.Id).FirstOrDefault<ArcgisLog>();

                if (_log != null)
                {
                    _log.timeEnd = DateTime.Now;
                }
                dbContext.Value.Entry(_log).State = EntityState.Modified;
                dbContext.Value.SaveChanges();
            }
            finally
            {
                dbContext.Value.Configuration.ValidateOnSaveEnabled = oldValidateOnSaveEnabled;
            }
        }

        public JObject Get(string queryString)
        {
            var log = WriteLog(queryString);
            WriteLog(queryString);
            try
            {
                var response = GetQuery(arcgisUrl, queryString);
                UpdateLog(log);
                return response;
            }
            catch
            {
                return null;
            }
        }

        public JObject Query(string queryString)
        {
            var log = WriteLog(queryString);
            try
            {
                var response = GetQuery(arcgisQuery, queryString);
                UpdateLog(log);
                return response;
            }
            catch
            {
                return null;
            }
        }

    }
}
