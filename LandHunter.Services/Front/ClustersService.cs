﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using LandHunter.ApiModels;
using LandHunter.DataAccess.Context;

namespace LandHunter.Services
{
    public class ClustersService : IClustersService
    {
        private readonly Lazy<LandHunterContext> _dbContext;

        public ClustersService(Lazy<LandHunterContext> dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<ClusterApiModel> GetLandsClusters(LandsExtentBindingModel landsExtent)
        {
            #region Parse filter params

            string utilization = null;
            string categoryCode = null;
            string cadNum = null;
            decimal? areaMin = null;
            decimal? areaMax = null;
            decimal? priceMin = null;
            decimal? priceMax = null;
            int? currencyId = null;

            if (landsExtent.Filters != null && landsExtent.Filters.Any())
            {
                var utilizationFilter = landsExtent.Filters.FirstOrDefault(f => f.Key.Equals("LandUtilization", StringComparison.InvariantCultureIgnoreCase));

                if (utilizationFilter != null && utilizationFilter.Values != null && utilizationFilter.Values.Any())
                    utilization = utilizationFilter.Values[0];

                var categoryFilter = landsExtent.Filters.FirstOrDefault(f => f.Key.Equals("LandCategory", StringComparison.InvariantCultureIgnoreCase));

                if (categoryFilter != null && categoryFilter.Values != null && categoryFilter.Values.Any())
                    categoryCode = categoryFilter.Values[0];

                var areaFilter = landsExtent.Filters.FirstOrDefault(f => f.Key.Equals("Area", StringComparison.InvariantCultureIgnoreCase));

                if (areaFilter != null && areaFilter.Values != null && areaFilter.Values.Any())
                {
                    areaMin = Decimal.Parse(areaFilter.Values[0], CultureInfo.InvariantCulture);

                    if (areaFilter.Values.Length > 1)
                        areaMax = Decimal.Parse(areaFilter.Values[1], CultureInfo.InvariantCulture);
                }

                var priceFilter = landsExtent.Filters.FirstOrDefault(f => f.Key.Equals("Price", StringComparison.InvariantCultureIgnoreCase));

                if (priceFilter != null && priceFilter.Values != null && priceFilter.Values.Any())
                {
                    priceMin = Decimal.Parse(priceFilter.Values[0], CultureInfo.InvariantCulture);

                    if (priceFilter.Values.Length > 1)
                        priceMax = Decimal.Parse(priceFilter.Values[1], CultureInfo.InvariantCulture);
                }

                var cadNumFilter = landsExtent.Filters.FirstOrDefault(f => f.Key.Equals("CadNum", StringComparison.InvariantCultureIgnoreCase));

                if (cadNumFilter != null && cadNumFilter.Values != null && cadNumFilter.Values.Any())
                {
                    cadNum = cadNumFilter.Values[0];
                }

                currencyId = landsExtent.CurrencyId;
            }

            #endregion

            var xMinParam = new SqlParameter("@XMin", landsExtent.XMin);
            var yMinParam = new SqlParameter("@YMin", landsExtent.YMin);
            var xMaxParam = new SqlParameter("@XMax", landsExtent.XMax);
            var yMaxParam = new SqlParameter("@YMax", landsExtent.YMax);
            var resolutionParam = new SqlParameter("@Resolution", landsExtent.Resolution);
            var utilizationParam = utilization != null
                ? new SqlParameter("@Utilization", utilization)
                : new SqlParameter("@Utilization", DBNull.Value);
            var categoryCodeParam = categoryCode != null
                ? new SqlParameter("@CategoryCode", categoryCode)
                : new SqlParameter("@CategoryCode", DBNull.Value);
            var areaMinParam = areaMin.HasValue
                ? new SqlParameter("@AreaMin", areaMin)
                : new SqlParameter("@AreaMin", DBNull.Value);
            var areaMaxParam = areaMax.HasValue
                ? new SqlParameter("@AreaMax", areaMax)
                : new SqlParameter("@AreaMax", DBNull.Value);
            var priceMinParam = priceMin.HasValue
                ? new SqlParameter("@PriceMin", priceMin)
                : new SqlParameter("@PriceMin", DBNull.Value);
            var priceMaxParam = priceMax.HasValue
                ? new SqlParameter("@PriceMax", priceMax)
                : new SqlParameter("@PriceMax", DBNull.Value);
            var currencyIdParam = currencyId.HasValue
                ? new SqlParameter("@CurrencyId", currencyId)
                : new SqlParameter("@CurrencyId", DBNull.Value);


            var cadNumParam = cadNum != null
                ? new SqlParameter("@CadNum", cadNum)
                : new SqlParameter("@CadNum", DBNull.Value);

            var clusters = _dbContext.Value.Database.SqlQuery<ClusterApiModel>(
                    "[dbo].[GetClusters] @xMin, @yMin, @xMax, @yMax, @Resolution, @Utilization, @CategoryCode, @AreaMin, @AreaMax, @PriceMin, @PriceMax, @CurrencyId, @CadNum",
                    xMinParam, yMinParam, xMaxParam, yMaxParam, resolutionParam, utilizationParam, categoryCodeParam, areaMinParam, areaMaxParam, priceMinParam, priceMaxParam, currencyIdParam, cadNumParam)
                .ToList();

            return clusters;
        }
    }
}