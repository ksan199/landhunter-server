﻿using System;
using System.Linq;
using LandHunter.ApiModels;
using LandHunter.Models;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class DictionariesService : IDictionariesService
    {
        private readonly Lazy<IRepository<Region, int>> _regionsRepository;
        private readonly Lazy<IRepository<LandCategory, string>> _landCategoriesRepository;
        private readonly Lazy<IRepository<AdvertisementStatus, int>> _advertisementStatusesRepository;
        private readonly Lazy<IRepository<Currency, int>> _currenciesRepository;

        public DictionariesService(
            Lazy<IRepository<Region, int>> regionsRepository,
            Lazy<IRepository<LandCategory, string>> landCategoriesRepository,
            Lazy<IRepository<AdvertisementStatus, int>> advertisementStatusesRepository,
            Lazy<IRepository<Currency, int>> currenciesRepository)
        {
            _regionsRepository = regionsRepository;
            _landCategoriesRepository = landCategoriesRepository;
            _advertisementStatusesRepository = advertisementStatusesRepository;
            _currenciesRepository = currenciesRepository;
        }

        public DictionariesApiModel Get()
        {
            return new DictionariesApiModel
                {
                    Regions = _regionsRepository.Value.GetAll().ToList(),
                    LandCategories = _landCategoriesRepository.Value.GetAll().ToList(),
                    AdvertisementStatuses = _advertisementStatusesRepository.Value.GetAll().ToList(),
                    Currencies = _currenciesRepository.Value.GetAll().ToList()
                };
        }
    }
}