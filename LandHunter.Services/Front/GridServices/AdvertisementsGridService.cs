﻿using System.Linq;
using LandHunter.Common.Helpers;
using LandHunter.Models;

namespace LandHunter.Services
{
    public class AdvertisementsGridService : GridService<AdvertisementExtended>
    {
        protected override void InitFilterConditions()
        {
            AddSingleParamCondition<string>("CadNum", val => (a => a.CadNum == val), LandsHelper.NormalizeCadNum);

            AddMinMaxCondition<decimal>("Area", (min, max) => (a => a.Land.Area >= min && (!max.HasValue || a.Land.Area <= max)));

            AddMinMaxCondition<decimal>("Price", (min, max) => (a => a.PriceInRub >= min && (!max.HasValue || a.PriceInRub <= max)));

            AddCollectionCondition<string>("LandCategory", vals => (a => vals.Contains(a.Land.CategoryCode)));

            AddSingleParamCondition<string>("LandUtilization", val => (a => a.Land.Utilization.ToLower() == val.ToLower()));

        }
    }
}