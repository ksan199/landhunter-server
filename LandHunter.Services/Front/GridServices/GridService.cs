﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LandHunter.ApiModels;
using LandHunter.Common;
using SharpEngine.Web.Mvc.Controls;
using SharpEngine.Core.Sorting;

namespace LandHunter.Services
{
    public abstract class GridService<TEntity> : IGridService<TEntity>
        where TEntity : class
    {
        protected GridSortOptions Sorting;
        protected int? Page;
        protected int? PageSize;
        protected IEnumerable<FilterBindingModel> BindingFilters;
        protected IDictionary<string, Expression<Func<TEntity, bool>>> Filters;

        public IQueryable<TEntity> Query { get; private set; }
        public int? TotalItems { get; private set; }
        public int? TotalPages { get; private set; }
        public bool? HasPreviousPage { get; private set; }
        public bool? HasNextPage { get; private set; }

        protected GridService()
        {
            Filters = new Dictionary<string, Expression<Func<TEntity, bool>>>();
        }

        public IGridService<TEntity> Init(IQueryable<TEntity> query, GridSettingsBindingModel gridSettings)
        {
            if (query == null)
                throw new ArgumentNullException("query");

            Query = query;

            if (gridSettings != null)
            {
                Sorting = gridSettings.Sorting;
                Page = gridSettings.Page;
                PageSize = gridSettings.PageSize;

                BindingFilters = gridSettings.Filters;
            }
            
            InitFilterConditions();

            return this;
        }

        protected abstract void InitFilterConditions();

        protected void AddSingleParamCondition<TFilterValue>(string key, Func<TFilterValue, Expression<Func<TEntity, bool>>> condition,
            RefAction<TFilterValue> preProcessing = null)
        {
            var filter = TryGetFilter(key);

            if (filter != null && filter.Values != null && filter.Values.Any())
            {
                var value = (TFilterValue)Convert.ChangeType(filter.Values[0], typeof(TFilterValue));

                if (preProcessing != null)
                    preProcessing.Invoke(ref value);

                var predicate = condition.Invoke(value);

                AddSimpleCondition(key, predicate);
            }
        }

        protected void AddMinMaxCondition<TFilterValue>(string key, Func<TFilterValue, TFilterValue?, Expression<Func<TEntity, bool>>> condition,
            RefAction<TFilterValue, TFilterValue?> preProcessing = null)
            where TFilterValue : struct
        {
            var filter = TryGetFilter(key);

            if (filter != null && filter.Values != null && filter.Values.Any())
            {
                var minValue = (TFilterValue)Convert.ChangeType(filter.Values[0], typeof(TFilterValue));

                TFilterValue? maxValue = null;

                if (filter.Values.Length > 1)
                    maxValue = (TFilterValue?)Convert.ChangeType(filter.Values[1], typeof(TFilterValue));

                if (preProcessing != null)
                    preProcessing.Invoke(ref minValue, ref maxValue);

                var predicate = condition.Invoke(minValue, maxValue);

                AddSimpleCondition(key, predicate);
            }
        }

        protected void AddMinMaxCondition<TFilterValue>(string key, Func<TFilterValue, TFilterValue, Expression<Func<TEntity, bool>>> condition,
            RefAction<TFilterValue, TFilterValue> preProcessing = null)
            where TFilterValue : class
        {
            var filter = TryGetFilter(key);

            if (filter != null && filter.Values != null && filter.Values.Any())
            {
                var minValue = (TFilterValue)Convert.ChangeType(filter.Values[0], typeof(TFilterValue));

                TFilterValue maxValue = null;

                if (filter.Values.Length > 1)
                    maxValue = (TFilterValue)Convert.ChangeType(filter.Values[1], typeof(TFilterValue));

                if (preProcessing != null)
                    preProcessing.Invoke(ref minValue, ref maxValue);

                var predicate = condition.Invoke(minValue, maxValue);

                AddSimpleCondition(key, predicate);
            }
        }

        protected void AddCollectionCondition<TFilterValue>(string key, Func<TFilterValue[], Expression<Func<TEntity, bool>>> condition,
            RefAction<TFilterValue[]> preProcessing = null)
        {
            var filter = TryGetFilter(key);

            if (filter != null && filter.Values != null && filter.Values.Any())
            {
                var values = filter.Values.Select(v => (TFilterValue)Convert.ChangeType(v, typeof(TFilterValue))).ToArray();

                if (preProcessing != null)
                    preProcessing.Invoke(ref values);

                var predicate = condition.Invoke(values);

                AddSimpleCondition(key, predicate);
            }
        }

        protected void AddSimpleCondition(string key, Expression<Func<TEntity, bool>> condition)
        {
            Filters.Add(key, condition);
        }

        private FilterBindingModel TryGetFilter(string key)
        {
            if (BindingFilters == null || !BindingFilters.Any())
                return null;

            return BindingFilters.FirstOrDefault(f => f.Key.Equals(key, StringComparison.InvariantCultureIgnoreCase));
        }

        public IQueryable<TEntity> ApplyFilter(string key = null)
        {
            if (!Filters.Any())
                return Query;

            if (key != null)
            {
                if (Filters.ContainsKey(key))
                    Query = Query.Where(Filters[key]);
            }
            else
            {
                foreach (var condition in Filters)
                    Query = Query.Where(condition.Value);
            }

            return Query;
        }

        public IQueryable<TEntity> ApplySorting()
        {
            if (Sorting == null || Sorting.Column == null)
                return Query;

            Query = Query.OrderBy(Sorting.Column, Sorting.Direction);

            return Query;
        }

        public IQueryable<TEntity> ApplyPaging()
        {
            if (!PageSize.HasValue)
                return Query;

            Page = Page ?? 1;

            TotalItems = Query.Count();

            var totalPages = TotalItems / PageSize.Value;

            if (TotalItems % PageSize.Value > 0)
                totalPages++;

            TotalPages = totalPages;
            HasPreviousPage = Page.Value > 1;
            HasNextPage = Page.Value < TotalPages;

            var numberToSkip = (Page.Value - 1) * PageSize.Value;

            Query = Query.Skip(numberToSkip).Take(PageSize.Value);

            return Query;
        }

        public IQueryable<TEntity> Apply()
        {
            ApplyFilter();
            ApplySorting();
            ApplyPaging();

            return Query;
        }
    }
}