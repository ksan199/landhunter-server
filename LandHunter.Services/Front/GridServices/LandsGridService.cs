﻿using System.Linq;
using LandHunter.Models;

namespace LandHunter.Services
{
    public class LandsGridService : GridService<Land>
    {
        protected override void InitFilterConditions()
        {
            AddMinMaxCondition<decimal>("Area", (min, max) => (l => l.Area >= min && (!max.HasValue || l.Area <= max)));

            AddMinMaxCondition<decimal>("Price", (min, max) =>
                (l => l.AdvertisementsExtended.Any(a => a.PriceInRub >= min && (!max.HasValue || a.PriceInRub <= max))));

            AddCollectionCondition<string>("LandCategory", vals => (l => vals.Contains(l.CategoryCode)));

            AddSingleParamCondition<string>("LandUtilization", val => (l => l.Utilization.ToLower() == val.ToLower()));

            AddSingleParamCondition<string>("CadNum", val => (l => l.CadNum.Contains(val)));
        }
    }
}