﻿using System.Linq;
using LandHunter.Models;

namespace LandHunter.Services
{
    public class RealtorsGridService : GridService<RealtorExtended>
    {
        protected override void InitFilterConditions()
        {
            AddCollectionCondition<int>("Region",
                vals => (r => r.Advertisements.Any(a => !a.IsSoldOut && !a.IsDeleted && vals.Contains(a.Land.RegionId))));

            AddCollectionCondition<string>("FullName", vals => (r => vals.All(v => r.FullName.ToLower().Contains(v))), SplitFullName);

            AddMinMaxCondition<int>("AdvertisementsCount", (min, max) =>
                (r => r.AdvertisementsCount >= min && (!max.HasValue || r.AdvertisementsCount <= max)));

            AddMinMaxCondition<int>("Rating", (min, max) => (r => r.Rating >= min && (!max.HasValue || r.Rating <= max)));
        }

        private static void SplitFullName(ref string[] filterValues)
        {
            filterValues = filterValues.First().ToLower().Split(' ');
        }
    }
}