﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class LandsService : ILandsService
    {
        private readonly Lazy<IRepository<Land, string>> _landsRepository;
        private readonly Lazy<IRepository<RealtorFavouriteLand, int>> _favouritesRepository;
        private readonly Lazy<IGridService<Land>> _gridService;

        public LandsService(
            Lazy<IRepository<Land, string>> landsRepository,
            Lazy<IRepository<RealtorFavouriteLand, int>> favouritesRepository,
            Lazy<IGridService<Land>> gridService)
        {
            _landsRepository = landsRepository;
            _favouritesRepository = favouritesRepository;
            _gridService = gridService;
        }

        public LandApiModel Get(string cadNum, LandDataComposition dataComposition)
        {
            LandsHelper.NormalizeCadNum(ref cadNum);

            if (!string.IsNullOrEmpty(cadNum))
            {
                LandApiModel landModel = null;

                var landQuery = _landsRepository.Value.Where(l => l.CadNum == cadNum);

                switch (dataComposition)
                {
                    #region Short

                    case LandDataComposition.Short:
                    {
                        var land = landQuery
                            .Select(l => new
                                {
                                    l.CadNum,
                                    l.RegionId,
                                    l.Address,
                                    l.Utilization,
                                    l.CategoryCode,
                                    l.GasPipeline,
                                    l.WaterPipeline,
                                    l.PowerLine,
                                    l.Sand,
                                    l.CadastrePrice,
                                    l.Area,
                                    MinPrice = l.AdvertisementsExtended
                                        .Where(a => !a.IsDeleted && !a.IsSoldOut)
                                        .OrderBy(a => a.PriceInRub)
                                        .Select(a => new { a.Price, a.CurrencyId })
                                        .FirstOrDefault()
                                })
                            .FirstOrDefault();

                        if (land != null)
                        {
                            landModel = new LandApiModel
                                {
                                    CadNum = land.CadNum,
                                    RegionId = land.RegionId,
                                    Address = land.Address,
                                    Utilization = land.Utilization,
                                    CategoryCode = land.CategoryCode,
                                    GasPipeline = land.GasPipeline ? true : (bool?)null,
                                    WaterPipeline = land.WaterPipeline ? true : (bool?)null,
                                    PowerLine = land.PowerLine ? true : (bool?)null,
                                    Sand = land.Sand ? true : (bool?)null,
                                    Area = land.Area,
                                    CadastrePrice = land.CadastrePrice,
                                    Price = land.MinPrice != null ? land.MinPrice.Price : (decimal?)null,
                                    CurrencyId = land.MinPrice != null ? land.MinPrice.CurrencyId : (int?)null
                                };
                        }

                        break;
                    }

                    #endregion

                    #region Additional

                    case LandDataComposition.Additional:
                    {
                        var land = landQuery
                            .Select(l => new
                                {
                                    l.ViewsCount,
                                    l.Description,
                                    Images = l.Files.Where(f => f.Type == FileTypeKey.Image && !f.IsDeleted && !f.IsDeactivated)
                                        .OrderBy(f => f.SortOrder).ThenByDescending(f => f.UploadDate)
                                        .Select(f => new { f.FileName, f.Description, f.Path, f.ThumbnailPath }).ToList(),
                                    Advertisements = l.Advertisements
                                        .Where(a => !a.IsDeleted && !a.IsSoldOut)
                                        .Select(a => new
                                            {
                                                a.Id,
                                                a.Price,
                                                a.CurrencyId,
                                                a.Area,
                                                a.IsOnTop,
                                                a.CreateDate,
                                                a.ShowCreateDate,
                                                Realtor = new
                                                    {
                                                        a.RealtorId,
                                                        a.Realtor.Surname,
                                                        a.Realtor.Name,
                                                        a.Realtor.User.PhoneNumber,
                                                        a.Realtor.User.Email,
                                                        a.Realtor.Rating,
                                                        ImageThumbnailPath = a.Realtor.User.Files
                                                            .Where(f => !f.IsDeleted && !f.IsDeactivated)
                                                            .Select(f => f.ThumbnailPath)
                                                            .FirstOrDefault(),
                                                    }
                                            })
                                        .ToList()
                                })
                            .FirstOrDefault();

                        if (land != null)
                        {
                            landModel = new LandApiModel
                                {
                                    ViewsCount = land.ViewsCount,
                                    Description = land.Description,
                                    Images = land.Images.Any()
                                        ? land.Images.Select(i => new FileApiModel { FileName = i.FileName, Description = i.Description, Path = i.Path, ThumbnailPath = i.ThumbnailPath })
                                        : null,
                                    Advertisements = land.Advertisements.Any()
                                        ? land.Advertisements.Select(a => new AdvertisementApiModel
                                            {
                                                Id = a.Id,
                                                Price = a.Price,
                                                CurrencyId = a.CurrencyId,
                                                Area = a.Area,
                                                IsOnTop = a.IsOnTop ? true : (bool?)null,
                                                CreateDate = a.CreateDate,
                                                ShowCreateDate = a.ShowCreateDate,
                                                Realtor = new RealtorApiModel
                                                    {
                                                        Id = a.Realtor.RealtorId,
                                                        Surname = a.Realtor.Surname,
                                                        Name = a.Realtor.Name,
                                                        PhoneNumber = a.Realtor.PhoneNumber,
                                                        Email = a.Realtor.Email,
                                                        Rating = a.Realtor.Rating,
                                                        Image = a.Realtor.ImageThumbnailPath != null
                                                            ? new FileApiModel
                                                                {
                                                                    ThumbnailPath = a.Realtor.ImageThumbnailPath
                                                                }
                                                            : null
                                                    }
                                            })
                                        : null
                                };
                        }

                        break;
                    }

                    #endregion

                    #region Full

                    case LandDataComposition.Full:
                    {
                        var land = landQuery
                            .Select(l => new
                                {
                                    l.CadNum,
                                    l.RegionId,
                                    l.Address,
                                    l.Description,
                                    l.Utilization,
                                    l.CategoryCode,
                                    l.GasPipeline,
                                    l.WaterPipeline,
                                    l.PowerLine,
                                    l.Sand,
                                    l.Area,
                                    l.CadastrePrice,
                                    MinPrice = l.AdvertisementsExtended
                                        .Where(a => !a.IsDeleted && !a.IsSoldOut)
                                        .OrderBy(a => a.PriceInRub)
                                        .Select(a => new { a.Price, a.CurrencyId })
                                        .FirstOrDefault(),
                                    Images = l.Files.Where(f => f.Type == FileTypeKey.Image && !f.IsDeleted && !f.IsDeactivated)
                                        .OrderBy(f => f.SortOrder).ThenByDescending(f => f.UploadDate)
                                        .Select(f => new { f.FileName, f.Description, f.Path, f.ThumbnailPath }).ToList(),
                                    Advertisements = l.Advertisements
                                        .Where(a => !a.IsDeleted && !a.IsSoldOut)
                                        .Select(a => new
                                            {
                                                a.Id,
                                                a.Price,
                                                a.CurrencyId,
                                                a.Area,
                                                a.IsOnTop,
                                                Realtor = new
                                                    {
                                                        a.RealtorId,
                                                        a.Realtor.Surname,
                                                        a.Realtor.Name,
                                                        a.Realtor.User.PhoneNumber,
                                                        a.Realtor.User.Email,
                                                        ImageThumbnailPath = a.Realtor.User.Files
                                                            .Where(f => !f.IsDeleted && !f.IsDeactivated)
                                                            .Select(f => f.ThumbnailPath)
                                                            .FirstOrDefault()
                                                    }
                                            })
                                        .ToList()
                                })
                            .FirstOrDefault();

                        if (land != null)
                        {
                            landModel = new LandApiModel
                                {
                                    CadNum = land.CadNum,
                                    RegionId = land.RegionId,
                                    Address = land.Address,
                                    Description = land.Description,
                                    Utilization = land.Utilization,
                                    CategoryCode = land.CategoryCode,
                                    GasPipeline = land.GasPipeline ? true : (bool?)null,
                                    WaterPipeline = land.WaterPipeline ? true : (bool?)null,
                                    PowerLine = land.PowerLine ? true : (bool?)null,
                                    Sand = land.Sand ? true : (bool?)null,
                                    Area = land.Area,
                                    CadastrePrice = land.CadastrePrice,
                                    Price = land.MinPrice != null ? land.MinPrice.Price : (decimal?)null,
                                    CurrencyId = land.MinPrice != null ? land.MinPrice.CurrencyId : (int?)null,
                                    Images = land.Images.Any()
                                        ? land.Images.Select(i => new FileApiModel { FileName = i.FileName, Description = i.Description, Path = i.Path, ThumbnailPath = i.ThumbnailPath })
                                        : null,
                                    Advertisements = land.Advertisements.Any()
                                        ? land.Advertisements.Select(a => new AdvertisementApiModel
                                            {
                                                Id = a.Id,
                                                Price = a.Price,
                                                CurrencyId = a.CurrencyId,
                                                Area = a.Area,
                                                IsOnTop = a.IsOnTop ? true : (bool?)null,
                                                Realtor = new RealtorApiModel
                                                    {
                                                        Id = a.Realtor.RealtorId,
                                                        Surname = a.Realtor.Surname,
                                                        Name = a.Realtor.Name,
                                                        PhoneNumber = a.Realtor.PhoneNumber,
                                                        Email = a.Realtor.Email,
                                                        Image = a.Realtor.ImageThumbnailPath != null
                                                            ? new FileApiModel
                                                                {
                                                                    ThumbnailPath = a.Realtor.ImageThumbnailPath
                                                                }
                                                            : null
                                                    }
                                            })
                                        : null
                                };
                        }

                        break;
                    }

                    #endregion
                }

                if (landModel != null && (dataComposition == LandDataComposition.Additional || dataComposition == LandDataComposition.Full))
                    _landsRepository.Value.Update(l => l.CadNum == cadNum, l => new Land { ViewsCount = l.ViewsCount + 1 });

                return landModel;
            }

            return null;
        }

        public Dictionary<string, object> GetFilterSettings()
        {
            throw new NotImplementedException();
        }

        public IReadOnlyCollection<LandApiModel> Filter(LandsGridBindingModel landsGrid)
        {
            var cadNums = landsGrid.CadNums;
            var query = _landsRepository.Value.Where(l => cadNums.Contains(l.CadNum) && l.Advertisements.Any(a => !a.IsDeleted && !a.IsSoldOut));

            if (landsGrid.GridSettings != null)
            {
                var gridService = _gridService.Value.Init(query, landsGrid.GridSettings);

                query = gridService.ApplyFilter();
            }

            if (landsGrid.GetOnlyCadNums)
                return query.Select(l => new LandApiModel { CadNum = l.CadNum }).ToList();

            return query
                .Select(l => new
                    {
                        l.CadNum,
                        l.RegionId,
                        l.Address,
                        l.Utilization,
                        l.CategoryCode,
                        l.GasPipeline,
                        l.WaterPipeline,
                        l.PowerLine,
                        l.Sand,
                        l.Area,
                        MinPrice = l.AdvertisementsExtended
                            .Where(a => !a.IsDeleted && !a.IsSoldOut)
                            .OrderBy(a => a.PriceInRub)
                            .Select(a => new { a.Price, a.CurrencyId })
                            .FirstOrDefault()
                    })
                .ToList()
                .Select(l => new LandApiModel
                    {
                        CadNum = l.CadNum,
                        RegionId = l.RegionId,
                        Address = l.Address,
                        Utilization = l.Utilization,
                        CategoryCode = l.CategoryCode,
                        GasPipeline = l.GasPipeline ? true : (bool?)null,
                        WaterPipeline = l.WaterPipeline ? true : (bool?)null,
                        PowerLine = l.PowerLine ? true : (bool?)null,
                        Sand = l.Sand ? true : (bool?)null,
                        Area = l.Area,
                        Price = l.MinPrice != null ? l.MinPrice.Price : (decimal?)null,
                        CurrencyId = l.MinPrice != null ? l.MinPrice.CurrencyId : (int?)null
                    })
                .ToList();
        }

        public IReadOnlyCollection<string> GetFavourites(IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            return _favouritesRepository.Value.Where(f => f.RealtorId == currentUserId).Select(f => f.CadNum).ToList();
        }

        public void AddToFavourites(string cadNum, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();
            LandsHelper.NormalizeCadNum(ref cadNum);

            if (cadNum == null || _favouritesRepository.Value.GetQuery().Any(f => f.RealtorId == currentUserId && f.CadNum == cadNum))
                return;

            _favouritesRepository.Value.Add(new RealtorFavouriteLand { RealtorId = currentUserId, CadNum = cadNum });
            _favouritesRepository.Value.SaveChanges();
        }

        public void RemoveFromFavourites(string cadNum, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();
            LandsHelper.NormalizeCadNum(ref cadNum);

            if (cadNum == null)
                return;

            _favouritesRepository.Value.Delete(f => f.RealtorId == currentUserId && f.CadNum == cadNum);
        }
    }
}