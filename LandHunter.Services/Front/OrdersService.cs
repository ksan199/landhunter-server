﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Repository;
using System.Globalization;

namespace LandHunter.Services
{
    public class OrdersService : IOrdersService
    {
        private readonly Lazy<IRepository<Order, int>> _ordersRepository;
        private readonly Lazy<IRepository<OrderStatus, int>> _orderStatusesRepository;
        private readonly Lazy<IRepository<OrderType, int>> _orderTypesRepository;
        private readonly Lazy<IRepository<Land, string>> _landsRepository;
        private readonly Lazy<IRepository<Realtor, string>> _realtorsRepository;
        private readonly Lazy<IRepository<City, string>> _citiesRepository;
        private readonly Lazy<ISettingsService> _settingsService;
        private readonly Lazy<IServicesService> _servicesService;
        private readonly string _yaKassaLogDir;
        private readonly string _yaKassaLogFile;

        public OrdersService(
            Lazy<IRepository<Land, string>> landsRepository,
            Lazy<IRepository<Order, int>> ordersRepository,
            Lazy<IRepository<OrderStatus, int>> orderStatusesRepository,
            Lazy<IRepository<OrderType, int>> orderTypesRepository,
            Lazy<IRepository<Realtor, string>> realtorsRepository,
            Lazy<IRepository<City, string>> citiesRepository,
            Lazy<ISettingsService> settingsService,
            Lazy<IServicesService> servicesService)
        {
            _realtorsRepository = realtorsRepository;
            _landsRepository = landsRepository;
            _ordersRepository = ordersRepository;
            _orderStatusesRepository = orderStatusesRepository;
            _orderTypesRepository = orderTypesRepository;
            _citiesRepository = citiesRepository;
            _settingsService = settingsService;
            _servicesService = servicesService;

            var logFolder = AppConfigHelper.GetSetting("YaKassaLogFolder", "~/App_Data/YaKassaLogs");

            _yaKassaLogDir = System.Web.Hosting.HostingEnvironment.MapPath(logFolder) + "\\";
            _yaKassaLogFile = _yaKassaLogDir + "Error.txt";
        }

        public OrderApiModel Create(OrderBindingModel order, IPrincipal principal)
        {
            var response = new OrderApiModel();

            var currentUserId = principal.Identity.GetUserId();

            var newStatusId = _orderStatusesRepository.Value
                   .Where(s => s.Key == OrderStatusKey.New).Select(s => s.Id).Single();

            var now = DateTime.Now;

            if (order.OrderType == (int)OrderTypeKey.Recharging)
            {
                var newOrder = new Order
                    {
                        RealtorId = currentUserId,
                        UpdateDate = now,
                        CreateDate = now,
                        OrderSumAmount = order.OrderSumAmount,
                        OrderStatusId = newStatusId,
                        PaymentType = order.PaymentType,
                        OrderTypeId = order.OrderType
                    };

                _ordersRepository.Value.Add(newOrder);
                _ordersRepository.Value.SaveChanges();

                response.Id = newOrder.Id;
            }
            else
            {
                var realtor = _realtorsRepository.Value
                    .Where(r => r.Id == currentUserId)
                    .Single();

                var settings = _settingsService.Value.Get();

                var normalizedCadNum = order.CadNum;

                if (normalizedCadNum != null)
                    LandsHelper.NormalizeCadNum(ref normalizedCadNum);

                var existingOrder = _ordersRepository.Value
                    .FirstOrDefault(o => o.CadNum == normalizedCadNum && o.RealtorId == currentUserId && DateTime.Now >= o.AdStartDate && DateTime.Now <= o.AdEndDate);

                if (existingOrder != null)
                {
                    response.Message = string.Format(settings.OrderExistsMessage,
                        existingOrder.AdStartDate.GetValueOrDefault().ToShortDateString(),
                        existingOrder.AdEndDate.GetValueOrDefault().ToShortDateString());
                }
                else if (realtor.Balance < settings.LandOrderPublishSum)
                {
                    response.Message = string.Format(settings.OrderBalanceNotEnoughMessage,
                        settings.LandOrderPublishSum,
                        realtor.Balance,
                        settings.LandOrderPublishSum - realtor.Balance);
                }
                else
                {
                    var newOrder = new Order
                        {
                            CadNum = normalizedCadNum,
                            RealtorId = currentUserId,
                            UpdateDate = now,
                            CreateDate = now,
                            OrderSumAmount = settings.LandOrderPublishSum,
                            OrderStatusId = newStatusId,
                            PaymentType = order.PaymentType,
                            OrderTypeId = order.OrderType,
                            BalanceBefore = realtor.Balance,
                            AdStartDate = order.AdStartDate ?? now,
                            AdEndDate = order.AdEndDate ?? now.AddDays(settings.LandOrderPublishDays)
                        };

                    _ordersRepository.Value.Add(newOrder);
                    _ordersRepository.Value.SaveChanges();

                    realtor.Balance -= settings.LandOrderPublishSum;

                    _realtorsRepository.Value.Update(realtor);
                    _realtorsRepository.Value.SaveChanges();

                    response.Message = string.Format(settings.OrderSuccessMessage,
                        newOrder.AdStartDate.GetValueOrDefault().ToShortDateString(),
                        newOrder.AdEndDate.GetValueOrDefault().ToShortDateString());

                    response.IsCreated = true;
                }
            }

            return response;
        }

        public IReadOnlyCollection<OrderApiModel> GetLast(int? howMany, IPrincipal principal)
        {
            var howManyToTake = howMany ?? 10;
            var currentUserId = principal.Identity.GetUserId() ?? "debug";

            var orders = _ordersRepository.Value
                .Where(o => o.RealtorId == currentUserId && o.OrderStatusId == (int)OrderStatusKey.PaymentReceived)
                .OrderByDescending(o => o.UpdateDate)
                .ToList()
                .Select(o =>
                    new OrderApiModel
                        {
                            OrderTypeId = o.OrderTypeId,
                            OrderType = _orderTypesRepository.Value.Where(t => t.Id == o.OrderTypeId).Select(t => t.Name).Single(),
                            PaymentType = o.PaymentType,
                            BalanceBefore = o.BalanceBefore,
                            OrderSumAmount = o.OrderSumAmount,
                            UpdateDate = o.UpdateDate,
                            CadNum = o.CadNum
                        })
                .Take(howManyToTake)
                .ToList();

            return orders;
        }

        public YaKassaResponseApiModel CheckOrder(YaKassaRequestApiModel request)
        {
            var response = new YaKassaResponseApiModel(request);
            
            try
            {
                if (CheckMd5Hash(request))
                {                    
                    response.Code = (int)YaKassaResponseStatus.Success;

                    var orderNumber = int.Parse(request.OrderNumber);

                    var approvedStatusId = _orderStatusesRepository.Value
                        .Where(s => s.Key == OrderStatusKey.Approved).Select(s => s.Id).Single();

                    var existingOrder = _ordersRepository.Value
                        .Where(a => a.Id == orderNumber)
                        .SingleOrDefault();

                    var now = DateTime.Now;

                    if (existingOrder != null)
                    {
                        existingOrder.OrderStatusId = approvedStatusId;
                        existingOrder.UpdateDate = now;
                        existingOrder.InvoiceId = request.InvoiceId;

                        _ordersRepository.Value.Update(existingOrder);
                    }
                    else
                    {
                        var orderTypeId = _orderTypesRepository.Value
                            .Where(t => t.Key == OrderTypeKey.Recharging).Select(t => t.Id).Single();

                        var newOrder = new Order
                            {
                                RealtorId = request.CustomerNumber,
                                InvoiceId = request.InvoiceId,
                                UpdateDate = now,
                                CreateDate = now,
                                OrderSumAmount = request.OrderSumAmount,
                                OrderStatusId = approvedStatusId,
                                OrderTypeId = orderTypeId
                            };

                        _ordersRepository.Value.Add(newOrder);
                    }

                    _ordersRepository.Value.SaveChanges();
                }
                else 
                {
                    response.Code = (int)YaKassaResponseStatus.AuthError;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)YaKassaResponseStatus.ParsingError;

                try
                {
                    if (!Directory.Exists(_yaKassaLogDir))
                        Directory.CreateDirectory(_yaKassaLogDir);

                    using (var file = new StreamWriter(_yaKassaLogFile, true))
                    {
                        file.WriteLine(string.Format("{0} - CheckOrder", DateTime.Now.ToString("G")));
                        file.WriteLine(e.Message);
                        file.WriteLine(e.StackTrace);
                    }
                }
                catch
                {
                }
            }

            return response;
        }

        public YaKassaResponseApiModel PaymentAviso(YaKassaRequestApiModel request)
        {
            var response = new YaKassaResponseApiModel(request);
            
            try
            {
                if (CheckMd5Hash(request))
                {                    
                    response.Code = (int)YaKassaResponseStatus.Success;

                    var existingOrder = _ordersRepository.Value
                        .Where(a => a.InvoiceId == request.InvoiceId)
                        .SingleOrDefault();

                    if (existingOrder != null)
                    {
                        var realtor = _realtorsRepository.Value
                            .Where(r => r.Id == existingOrder.RealtorId)
                            .Single();

                        var paymentReceivedStatusId = _orderStatusesRepository.Value
                            .Where(s => s.Key == OrderStatusKey.PaymentReceived).Select(s => s.Id).Single();

                        existingOrder.OrderStatusId = paymentReceivedStatusId;
                        existingOrder.UpdateDate = DateTime.Now;
                        existingOrder.BalanceBefore = realtor.Balance;

                        realtor.Balance += existingOrder.OrderSumAmount;

                        _ordersRepository.Value.Update(existingOrder);
                        _realtorsRepository.Value.Update(realtor);

                        _realtorsRepository.Value.SaveChanges();

                        _servicesService.Value.ProcessSuspendedOrders(realtor.Id, existingOrder.OrderSumAmount);
                    }
                    else
                    {
                        var errorMessage = "Заказ с таким invoiceId не был найден в системе - " + request.InvoiceId;

                        try
                        {
                            if (!Directory.Exists(_yaKassaLogDir))
                                Directory.CreateDirectory(_yaKassaLogDir);

                            using (var file = new StreamWriter(_yaKassaLogFile, true))
                            {
                                file.WriteLine(string.Format("{0} - PaymentAviso", DateTime.Now.ToString("G")));
                                file.WriteLine(errorMessage);
                            }
                        }
                        catch
                        {
                        }

                        throw new Exception(errorMessage);                        
                    }
                }
                else
                {
                    response.Code = (int)YaKassaResponseStatus.AuthError;
                }
            }
            catch (Exception e)
            {
                response.Code = (int)YaKassaResponseStatus.ParsingError;

                try
                {
                    if (!Directory.Exists(_yaKassaLogDir))
                        Directory.CreateDirectory(_yaKassaLogDir);

                    using (var file = new StreamWriter(_yaKassaLogFile, true))
                    {
                        file.WriteLine(string.Format("{0} - PaymentAviso", DateTime.Now.ToString("G")));
                        file.WriteLine(e.Message);
                        file.WriteLine(e.StackTrace);
                    }
                }
                catch
                {
                }
            }

            return response;
        }
        
        public string GenerateAvitoAdvertisements(string webSitePath)
        {
            if (string.IsNullOrWhiteSpace(webSitePath))
                throw new ArgumentNullException("webSitePath");

            var activeAndPaidOrders = GetActiveAndPaid();

            var avitoAds = new List<AdvertisementAvitoModel>();

            activeAndPaidOrders.ForEach(o =>
                {
                    var ad = new AdvertisementAvitoModel();
                    var realtor = _realtorsRepository.Value.Where(r => r.Id == o.RealtorId).Single();
                    var land = _landsRepository.Value.SingleOrDefault(l => l.CadNum == o.CadNum);

                    if (land == null)
                        return;
                    
                    var filesUrl = land.Files.Select(f => f.Path).ToList();

                    decimal squareMetesToSotka = 0.01m;
                    var adId = string.Format("{0}_{1}", o.RealtorId, o.CadNum);

                    ad.Id = adId;
                    ad.Category = "Земельные участки";                    
                    ad.Region = "Москва";
                    ad.City = "";
                    ad.ContactPhone = realtor.User.PhoneNumber;
                    ad.EMail = realtor.User.Email;
                    ad.ManagerName = string.Format("{0} {1} {2}", realtor.Surname, realtor.Name, realtor.Patronymic);
                    ad.CompanyName = realtor.Agency != null ? realtor.Agency.Name : "";
                    ad.AdStatus = "Free";
                    ad.DateBegin = o.AdStartDate.ToString();
                    ad.DateEnd = o.AdEndDate.ToString();
                    ad.OperationType = "Продам";
                    ad.LandArea = land.Area * squareMetesToSotka ; 
                    ad.ObjectType = "Поселений (ИЖС)";
                    ad.DistanceToCity = 1;                                       
                    ad.Images = filesUrl.Select(x => new ImageAvitoModel(webSitePath + x)).ToArray();

                    avitoAds.Add(ad);
                });

            var avitoAd = new AdvertisementsAvitoModel(avitoAds.ToArray());
            
            return avitoAd.ToXmlString();
        }
        
        public string GenerateCianAdvertisements(string webSitePath)
        {
            if (string.IsNullOrWhiteSpace(webSitePath))
                throw new ArgumentNullException("webSitePath");

            var activeAndPaidOrders = this.GetActiveAndPaid();

            var cianAds = new List<CianOfferModel>();

            activeAndPaidOrders.ForEach(o =>
                {
                    var ad = new CianOfferModel();
                    var realtor = _realtorsRepository.Value.Where(r => r.Id == o.RealtorId).Single();

                    var land = _landsRepository.Value.SingleOrDefault(l => l.CadNum == o.CadNum);

                    if (land == null)
                        return;

                    var filesUrl = land.Files.Select(f => f.Path).ToList();
                    var realtorAd = land.Advertisements.FirstOrDefault(a => a.CadNum == land.CadNum && a.RealtorId == realtor.Id);

                    ad.Id = o.Id;
                    ad.RealtyType = "A";
                    ad.DealType = "S";
                    ad.LandType = 2;
                    ad.Note = string.Format("<![CDATA[ {0} ]]>", land.Description);
                    ad.Phone = realtor.User.PhoneNumber;
                    ad.Premium = 0;
                    ad.SetArea(land.Area);
                    ad.SetPrice(realtorAd.Price * (realtorAd.Area ?? 0));
                    ad.SetAddress(land.Region.CianId, realtorAd.Address ?? land.Region.Name, " ");
                    ad.Images = filesUrl.Select(x => webSitePath + x).ToArray();

                    cianAds.Add(ad);
                });

            var cianRoot = new CianRootModel(cianAds.ToArray());
            
            return cianRoot.ToXmlString();
        }

        public string GenerateIrrAdvertisements(string webSitePath, string userId)
        {
            if (string.IsNullOrWhiteSpace(webSitePath))
                throw new ArgumentNullException("webSitePath");

            var activeAndPaidOrders = this.GetActiveAndPaid();

            var offers = new List<IrrOffer>();

            activeAndPaidOrders.ForEach(o =>
            {
                var ad = new IrrOffer();
                var realtor = _realtorsRepository.Value.Where(r => r.Id == o.RealtorId).Single();
                var land = _landsRepository.Value.SingleOrDefault(l => l.CadNum == o.CadNum);

                if (land == null)
                    return;

                var filesUrl = land.Files.Select(f => f.Path).ToList();
                var realtorAd = land.Advertisements.FirstOrDefault(a => a.CadNum == land.CadNum && a.RealtorId == realtor.Id);

                var squareMetesToSotka =  0.01m;

                ad.Id = o.Id;
                ad.DateBegin = o.AdStartDate.ToString();
                ad.DateEnd = o.AdEndDate.ToString();
                ad.Title = land.Address;
                ad.Description = land.Description;
                ad.SetPrice(realtorAd.Price * (realtorAd.Area ?? 0), "RUR");
                ad.Photos = land.Files.Select(x => new IrrPhoto(webSitePath + x.Path, x.Md5Hash)).ToArray();

                var customFields = new Dictionary<string, string>();

                customFields.Add("region", land.Region.Name);//todo регионы в соответсвие
                customFields.Add("address_city", this.GetCityByCadNum(land.CadNum));
                customFields.Add("land", (land.Area * squareMetesToSotka).ToString() );
                customFields.Add("mail", realtor.User.Email);
                customFields.Add("phone", realtor.User.PhoneNumber);
                customFields.Add("contact", string.Format("{0} {1}" ,realtor.Surname, realtor.Name) );
                customFields.Add("private", "да");

                if (realtorAd.LandExtended.CentrPoint != null)
                {
                    var lat = realtorAd.LandExtended.CentrPoint.YCoordinate.Value;
                    var lng = realtorAd.LandExtended.CentrPoint.XCoordinate.Value;

                    ToGeographic(ref lng, ref lat);

                    customFields.Add("geo_lat", lat.ToString("0.000000", CultureInfo.InvariantCulture));
                    customFields.Add("geo_lng", lng.ToString("0.000000", CultureInfo.InvariantCulture));
                }

                ad.CustomFields = customFields.Select(f => new IrrCustomField(f.Key, f.Value)).ToArray();
                
                offers.Add(ad);
            });  
            
            var rootModel = new IrrRootModel();
            var users = new List<IrrUser>();
            var user = new IrrUser();

            user.Credentials.Id = userId;
            user.Offers = offers.ToArray();

            users.Add(user);

            rootModel.Users = users.ToArray();

            return rootModel.ToXmlString();
        }

        #region Helper Methods

        private static bool CheckMd5Hash(YaKassaRequestApiModel request)
        {
            var password = Uri.UnescapeDataString(AppConfigHelper.GetSetting("YaShopPassword", ""));

            var fields = new[]
                { 
                    request.Action,
                    string.Format(CultureInfo.InvariantCulture, "{0:0.00}", request.OrderSumAmount),
                    request.OrderSumCurrencyPaycash.ToString(),
                    request.OrderSumBankPaycash.ToString(),
                    request.ShopId.ToString(),
                    request.InvoiceId.ToString(),
                    request.CustomerNumber,
                    password
                };

            var calculatedHash = Md5Helper.CalculateMD5Hash(string.Join(";", fields));

            return calculatedHash.Equals(request.Md5);
        }

        private List<Order> GetActiveAndPaid()
        {
            var publication = _orderTypesRepository.Value
                .Where(t => t.Key == OrderTypeKey.Publication).Select(t => t.Id).Single();

            return _ordersRepository.Value
                .Where(o => (o.AdStartDate <= DateTime.Now && o.AdEndDate >= DateTime.Now) && o.OrderTypeId == publication)
                .ToList();
        }

        private void ToGeographic(ref double mercatorX_lon, ref double mercatorY_lat)
        {
            if (Math.Abs(mercatorX_lon) < 180 && Math.Abs(mercatorY_lat) < 90)
                return;

            if ((Math.Abs(mercatorX_lon) > 20037508.3427892) || (Math.Abs(mercatorY_lat) > 20037508.3427892))
                return;

            double x = mercatorX_lon;
            double y = mercatorY_lat;
            double num3 = x / 6378137.0;
            double num4 = num3 * 57.295779513082323;
            double num5 = Math.Floor((double)((num4 + 180.0) / 360.0));
            double num6 = num4 - (num5 * 360.0);
            double num7 = 1.5707963267948966 - (2.0 * Math.Atan(Math.Exp((-1.0 * y) / 6378137.0)));

            mercatorX_lon = num6;
            mercatorY_lat = num7 * 57.295779513082323;
        }

        private void ToWebMercator(ref double mercatorX_lon, ref double mercatorY_lat)
        {
            if ((Math.Abs(mercatorX_lon) > 180 || Math.Abs(mercatorY_lat) > 90))
                return;

            double num = mercatorX_lon * 0.017453292519943295;
            double x = 6378137.0 * num;
            double a = mercatorY_lat * 0.017453292519943295;

            mercatorX_lon = x;
            mercatorY_lat = 3189068.5 * Math.Log((1.0 + Math.Sin(a)) / (1.0 - Math.Sin(a)));
        }

        private string GetCityByCadNum(string cadNum)
        {
            string cityName = "";

            string[] cadNumParts = cadNum.Split(':');

            if (cadNumParts.Length > 0)
            {
                var regionCode = cadNumParts[0];

                var city = _citiesRepository.Value.FirstOrDefault(c => c.RegionId == regionCode);

                if (city != null)
                {
                    cityName = city.Name;
                }
            }

            return cityName;
        }

        #endregion
    }
}