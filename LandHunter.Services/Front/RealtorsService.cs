﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class RealtorsService : IRealtorsService
    {
        private readonly Lazy<IRepository<Realtor, string>> _realtorsRepository;
        private readonly Lazy<IRepository<RealtorExtended, string>> _realtorsExtendedRepository;
        private readonly Lazy<IRepository<File, int>> _filesRepository;
        private readonly Lazy<IGridService<RealtorExtended>> _gridService;
        private readonly Lazy<IFilesService> _filesService;

        public RealtorsService(
            Lazy<IRepository<Realtor, string>> realtorsRepository,
            Lazy<IRepository<RealtorExtended, string>> realtorsExtendedRepository,
            Lazy<IRepository<File, int>> filesRepository,
            Lazy<IGridService<RealtorExtended>> gridService,
            Lazy<IFilesService> filesService)
        {
            _realtorsRepository = realtorsRepository;
            _realtorsExtendedRepository = realtorsExtendedRepository;
            _filesRepository = filesRepository;
            _gridService = gridService;
            _filesService = filesService;
        }

        public RealtorsGridApiModel GetList(RealtorsGridBindingModel realtorsGrid, IPrincipal principal)
        {
            var grid = new RealtorsGridApiModel();

            var query = _realtorsExtendedRepository.Value
                .Where(r => !r.User.IsDeleted && !r.User.LockoutEnabled && r.AdvertisementsCount > 0)
                .OrderBy(r => r.Id).AsQueryable();

            if (realtorsGrid != null && realtorsGrid.GridSettings != null)
            {
                var gridService = _gridService.Value.Init(query, realtorsGrid.GridSettings);

                gridService.ApplyFilter();
                gridService.ApplySorting();

                grid.AllAdvertisementsCount = gridService.Query.Sum(r => (int?)r.AdvertisementsCount) ?? 0;

                query = gridService.ApplyPaging();

                grid.TotalItems = gridService.TotalItems;
                grid.TotalPages = gridService.TotalPages;
                grid.HasPreviousPage = gridService.HasPreviousPage;
                grid.HasNextPage = gridService.HasNextPage;
            }

            grid.List = query
                .Select(r => new
                    {
                        r.Id,
                        r.Name,
                        r.Surname,
                        r.Patronymic,
                        r.User.PhoneNumber,
                        r.User.Email,
                        r.Rating,
                        r.AdvertisementsCount,
                        r.Agency,
                        ImageThumbnailPath = r.User.Files
                            .Where(f => !f.IsDeleted && !f.IsDeactivated)
                            .Select(f => f.ThumbnailPath)
                            .FirstOrDefault(),
                        RegionsIds = r.Advertisements.Where(a => !a.IsSoldOut && !a.IsDeleted).Select(a => a.Land.RegionId).Distinct().ToList()
                    })
                .ToList()
                .Select(r => new RealtorApiModel
                    {
                        Id = r.Id,
                        Name = r.Name,
                        Surname = r.Surname,
                        Patronymic = r.Patronymic,
                        PhoneNumber = r.PhoneNumber,
                        Email = r.Email,
                        Agency = r.Agency != null
                            ? new AgencyApiModel
                                {
                                    Id = r.Agency.Id,
                                    Name = r.Agency.Name
                                }
                            : null,
                        Rating = r.Rating,
                        AdvertisementsCount = new Dictionary<AdvertisementState, int> { { AdvertisementState.Active, r.AdvertisementsCount } },
                        Image = r.ImageThumbnailPath != null
                            ? new FileApiModel
                                {
                                    ThumbnailPath = r.ImageThumbnailPath
                                }
                            : null,
                        RegionsIds = r.RegionsIds.Any() ? r.RegionsIds : null
                    })
                .ToList();

            return grid;
        }

        public RealtorApiModel Get(string id, IPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated && id == null)
                throw new Exception("Необходимо указать Id риелтора.");

            var currentUserId = principal.Identity.GetUserId();
            var realtorId = id ?? currentUserId;

            var realtor = _realtorsRepository.Value
                .Where(r => r.Id == realtorId)
                .Select(r => new
                    {
                        r.Id,
                        r.Surname,
                        r.Name,
                        r.Patronymic,
                        r.User.PhoneNumber,
                        r.User.Email,
                        r.Address,
                        r.Website,
                        r.Agency,
                        r.Balance,
                        AgencyImage = r.Agency.Files
                            .Where(f => !f.IsDeleted && !f.IsDeactivated)
                            .Select(f => new { f.Id, f.ThumbnailPath, f.Path })
                            .FirstOrDefault(),
                        r.Rating,
                        Image = r.User.Files
                            .Where(f => !f.IsDeleted && !f.IsDeactivated)
                            .Select(f => new { f.Id, f.ThumbnailPath, f.Path })
                            .FirstOrDefault(),
                        Advertisements = r.Advertisements
                            .Select(a => new { a.IsSoldOut, a.IsDeleted, a.Land.ViewsCount })
                            .ToList(),
                        Agencies = r.User.Agencies
                            .Where(a => !a.IsDeleted)
                            .Select(a => a.Id)
                            .ToList()
                    })
                .ToList()
                .Select(r => new RealtorApiModel
                    {
                        Id = r.Id,
                        Surname = r.Surname,
                        Name = r.Name,
                        Patronymic = r.Patronymic,
                        PhoneNumber = r.PhoneNumber,
                        Email = r.Email,
                        Address = r.Address,
                        Website = r.Website,
                        Agency = r.Agency != null
                            ? new AgencyApiModel
                                {
                                    Id = r.Agency.Id,
                                    Name = r.Agency.Name,
                                    OGRN = r.Agency.OGRN,
                                    Address = r.Agency.Address,
                                    Phone = r.Agency.Phone,
                                    Email = r.Agency.Email,
                                    Website = r.Agency.Website,
                                    Description = r.Agency.Description,
                                    Image = r.AgencyImage != null
                                        ? new FileApiModel { Id = r.AgencyImage.Id, ThumbnailPath = r.AgencyImage.ThumbnailPath, Path = r.AgencyImage.Path }
                                        : null
                                }
                            : null,
                        Rating = r.Rating,
                        Balance = r.Balance,
                        Image = r.Image != null
                            ? new FileApiModel
                                {
                                    Id = r.Image.Id,
                                    Path = r.Image.Path,
                                    ThumbnailPath = r.Image.ThumbnailPath
                                }
                            : null,
                        AdvertisementsCount = r.Advertisements.Any()
                            ? GetAdvertisementsCount(r.Id == currentUserId,
                                r.Advertisements.Select(a => new Tuple<bool, bool>(a.IsSoldOut, a.IsDeleted)).ToList())
                            : null,
                        // Убрал проверку на идентификатор риелтора
                        ViewsCount = r.Advertisements.Sum(a => a.ViewsCount),
                        OwnedAgenciesIds = r.Id == currentUserId ? r.Agencies : null
                    })
                .Single();

            if (id != null)
                _realtorsRepository.Value.Update(r => r.Id == id, r => new Realtor { ViewsCount = r.ViewsCount + 1 });

            return realtor;
        }
        /// <summary>
        /// Updates account values which are present(not null) in 'realtor' param
        /// </summary>
        /// <param name="realtor"></param>
        /// <param name="principal"></param>
        public void Update(RealtorBindingModel realtor, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            var existingRealtor = _realtorsRepository.Value
                .Where(r => r.Id == currentUserId)
                .Single();
            if (realtor.Surname != null)
                existingRealtor.Surname = realtor.Surname;
            if (realtor.Name != null)
                existingRealtor.Name = realtor.Name;
            if (realtor.Patronymic != null)
                existingRealtor.Patronymic = realtor.Patronymic;
            if (realtor.Email != null)
                existingRealtor.User.Email = realtor.Email;
            if (realtor.Address != null)
                existingRealtor.Address = realtor.Address;
            if (realtor.Website != null)
                existingRealtor.Website = realtor.Website;
            if (realtor.AgencyId != null)
                existingRealtor.AgencyId = realtor.AgencyId;

            _realtorsRepository.Value.Update(existingRealtor);
            _realtorsRepository.Value.SaveChanges();

            var currentImageId = existingRealtor.User.Files.Where(f => !f.IsDeleted && !f.IsDeactivated).Select(f => (int?)f.Id).FirstOrDefault();

            if (realtor.ImageId != currentImageId)
            {
                if (currentImageId.HasValue)
                    _filesService.Value.MarkAsDeleted(currentImageId.Value, principal);

                if (realtor.ImageId.HasValue)
                    _filesService.Value.AssignTempFile(realtor.ImageId.Value, principal, FileCategoryKey.Image, userId: currentUserId);
            }
        }

        public void CheckVisit(IPrincipal principal)
        {
            var realtorId = principal.Identity.GetUserId();

            var realtor = _realtorsRepository.Value
                .Where(r => r.Id == realtorId)
                .Select(r => new { r.ContinuedVisitsCount, r.TotalVisitsCount, r.LastVisitDate })
                .SingleOrDefault();

            if (realtor != null)
            {
                var now = DateTime.Now;

                if (now.Date > realtor.LastVisitDate.Date)
                {
                    var totalVisitsCount = realtor.TotalVisitsCount + 1;
                    var continuedVisitsCount = realtor.LastVisitDate.Date.AddDays(1) == now.Date ? realtor.ContinuedVisitsCount + 1 : 1;

                    _realtorsRepository.Value.Update(r => r.Id == realtorId, r => new Realtor
                        {
                            TotalVisitsCount = totalVisitsCount,
                            ContinuedVisitsCount = continuedVisitsCount,
                            LastVisitDate = now
                        });
                }
            }
        }

        private Dictionary<AdvertisementState, int> GetAdvertisementsCount(bool isCurrentUser, IReadOnlyCollection<Tuple<bool, bool>> advertisements)
        {
            var advertisementsCount = new Dictionary<AdvertisementState, int>
                {
                    { AdvertisementState.Active, advertisements.Count(a => !a.Item1 && !a.Item2) }
                };

            if (isCurrentUser)
            {
                advertisementsCount.Add(AdvertisementState.All, advertisements.Count());
                advertisementsCount.Add(AdvertisementState.SoldOut, advertisements.Count(a => a.Item1 && !a.Item2));
                advertisementsCount.Add(AdvertisementState.Deleted, advertisements.Count(a => a.Item2));
            }

            return advertisementsCount;
        }
    }
}