﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using LandHunter.ApiModels;
using LandHunter.AspNetIdentity;
using LandHunter.Common.Helpers;
using LandHunter.DataAccess.Context;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SharpEngine.Repository;
using SharpEngine.Security;
using SharpEngine.Web.Mvc.AspNetIdentity;

namespace LandHunter.Services
{
    public class SecurityService : ISecurityService
    {
        private readonly Lazy<IRepository<Realtor, string>> _repository;
        private readonly IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> _aspNetIdentity;
        private readonly Lazy<ISettingsService> _settingsService;

        public SecurityService(
            Lazy<IRepository<Realtor, string>> repository,
            IAspNetIdentity<LandHunterContext, ApplicationUser, ApplicationUserManager, RoleManager<IdentityRole>> aspNetIdentity,
            Lazy<ISettingsService> settingsService)
        {
            _repository = repository;
            _aspNetIdentity = aspNetIdentity;
            _settingsService = settingsService;
        }

        public async Task RegisterAsync(RegistrationBindingModel registration)
        {
            var userName = RealtorsHelper.GetUserNameFromPhone(registration.PhoneNumber);

            var password = ObviexPasswordGenerator.Generate(6, useUpperCase: false, useSpecialChars: false);

            await _aspNetIdentity.CreateUserAsync(
                new ApplicationUser(userName)
                    {
                        PhoneNumber = registration.PhoneNumber,
                        PhoneNumberConfirmed = true,
                        Email = registration.Email
                    },
                password);

            var userId = await _aspNetIdentity.GetUsersQuery().Where(u => u.UserName == userName).Select(u => u.Id).SingleAsync();

            await _aspNetIdentity.AddUserToRoleAsync(userId, "Realtor");

            var fullName = registration.FullName.Split(' ');

            _repository.Value.Add(new Realtor
                {
                    Id = userId,
                    Surname = fullName.Length > 1 ? fullName[0] : null,
                    Name = fullName.Length == 1 ? fullName[0] : fullName[1],
                    Patronymic = fullName.Length >= 3 ? fullName.Skip(2).Aggregate((p1, p2) => p1 + " " + p2) : null,
                    LastVisitDate = DateTime.Now
                });

            await _repository.Value.SaveChangesAsync();

            var message = string.Format(_settingsService.Value.Get().RegistrationMessage, password);

            await _aspNetIdentity.SendSmsAsync(userId, message);
        }

        public async Task ResetPasswordAsync(ResetPasswordBindingModel resetPassword)
        {
            var userName = RealtorsHelper.GetUserNameFromPhone(resetPassword.PhoneNumber);

            var user = await _aspNetIdentity.FindUserByNameAsync(userName);

            if (user == null || user.IsDeleted)
                throw new Exception(string.Format("Пользователь с номером телефона {0} не найден.", resetPassword.PhoneNumber));

            if (user.LockoutEnabled)
                throw new Exception(string.Format("Пользователь с номером телефона {0} заблокирован.", resetPassword.PhoneNumber));

            var newPassword = ObviexPasswordGenerator.Generate(6, useUpperCase: false, useSpecialChars: false);

            await _aspNetIdentity.ResetPasswordAsync(user.Id, newPassword);

            var message = string.Format(_settingsService.Value.Get().ResetMessage, newPassword);

            await _aspNetIdentity.SendSmsAsync(user.Id, message);
        }

        public async Task ChangePasswordAsync(ChangePasswordBindingModel changePassword, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            var user = await _aspNetIdentity.FindUserByIdAsync(currentUserId);

            if (user == null || user.IsDeleted)
                throw new Exception("Пользователь не найден.");

            if (user.LockoutEnabled)
                throw new Exception("Пользователь заблокирован.");

            await _aspNetIdentity.ChangePasswordAsync(user.Id, changePassword.OldPassword, changePassword.NewPassword);

            var message = string.Format(_settingsService.Value.Get().ResetMessage, changePassword.NewPassword);

            await _aspNetIdentity.SendSmsAsync(user.Id, message);
        }

        public async Task<bool> VerifyUser(string phoneNumber)
        {
            var userName = RealtorsHelper.GetUserNameFromPhone(phoneNumber);

            return await _aspNetIdentity.GetUsersQuery().AnyAsync(u => u.UserName == userName);
        }
    }
}