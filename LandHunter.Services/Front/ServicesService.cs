﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using LandHunter.ApiModels;
using LandHunter.Common.Enums;
using LandHunter.Common.Helpers;
using LandHunter.Models;
using Microsoft.AspNet.Identity;
using SharpEngine.Repository;
using Newtonsoft.Json;
using LandHunter.Common;

namespace LandHunter.Services
{
    public class ServicesService : IServicesService
    {
        private readonly Lazy<IRepository<Service, int>> _servicesRepository;
        private readonly Lazy<IRepository<ServiceCategory, int>> _serviceCategoriesRepository;
        private readonly Lazy<IRepository<ServiceOrder, int>> _serviceOrdersRepository;
        private readonly Lazy<IRepository<Order, int>> _ordersRepository;
        private readonly Lazy<IRepository<OrderStatus, int>> _orderStatusesRepository;
        private readonly Lazy<IRepository<OrderType, int>> _orderTypesRepository;
        private readonly Lazy<IRepository<Realtor, string>> _realtorsRepository;
        private readonly Lazy<IMailService> _mailService;

        public ServicesService(
            Lazy<IRepository<Service, int>> servicesRepository,
            Lazy<IRepository<ServiceCategory, int>> serviceCategoriesRepository,
            Lazy<IRepository<ServiceOrder, int>> serviceOrdersRepository,
            Lazy<IRepository<Order, int>> ordersRepository,
            Lazy<IRepository<OrderStatus, int>> orderStatusesRepository,
            Lazy<IRepository<OrderType, int>> orderTypesRepository,
            Lazy<IRepository<Realtor, string>> realtorsRepository,
            Lazy<IMailService> mailService)
        {
            _servicesRepository = servicesRepository;
            _serviceCategoriesRepository = serviceCategoriesRepository;
            _serviceOrdersRepository = serviceOrdersRepository;
            _ordersRepository = ordersRepository;
            _orderStatusesRepository = orderStatusesRepository;
            _orderTypesRepository = orderTypesRepository;
            _realtorsRepository = realtorsRepository;
            _mailService = mailService;
        }

        public IReadOnlyCollection<ServiceCategoryApiModel> GetList()
        {
            return _serviceCategoriesRepository.Value.GetQuery()
                .OrderBy(c => c.SortOrder)
                .Select(c => new { c.Id, c.Name, Services = c.Services.OrderBy(s => s.SortOrder).Select(s => new { s.Id, s.Name, s.Price }) })
                .ToList()
                .Select(c => new ServiceCategoryApiModel
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Services = c.Services.Select(s => new ServiceApiModel { Id = s.Id, Name = s.Name, Price = s.Price }).ToList()
                    })
                .ToList();
        }

        public void Create(ServiceOrderBindingModel order, IPrincipal principal)
        {
            var currentUserId = principal.Identity.GetUserId();

            var realtorBalance = _realtorsRepository.Value
                .Where(r => r.Id == currentUserId)
                .Select(r => r.Balance)
                .Single();

            var service = _servicesRepository.Value
                .Where(s => s.Id == order.ServiceId)
                .Select(s => new { s.Price, ModeratorsEmails = s.ModeratorsService.Select(ms => ms.Moderator.Email )})
                .Single();

            var newStatusId = _orderStatusesRepository.Value
                .Where(s => s.Key == OrderStatusKey.New).Select(s => s.Id).Single();

            var orderTypeId = _orderTypesRepository.Value
                .Where(t => t.Key == OrderTypeKey.Service).Select(t => t.Id).Single();

            var now = DateTime.Now;

            var newOrder = new Order
                {
                    RealtorId = currentUserId,
                    UpdateDate = now,
                    CreateDate = now,
                    OrderSumAmount = service.Price ?? 0,
                    OrderStatusId = newStatusId,
                    OrderTypeId = orderTypeId,
                    BalanceBefore = realtorBalance,
                    ServiceOrder = new ServiceOrder
                        {
                            ServiceId = order.ServiceId,
                            UpdateDate = now,
                            CreateDate = now,
                            Status = service.Price.HasValue && realtorBalance < service.Price.Value ? ServiceOrderStatus.Suspended : ServiceOrderStatus.New,
                            UserData = JsonHelper.Serialize(order.UserData),
                            Email = order.Email,
                            ReceptionType = order.ReceptionType
                        }
                };


            _ordersRepository.Value.Add(newOrder);
            _ordersRepository.Value.SaveChanges();

            if (service.Price.HasValue)
                _realtorsRepository.Value.Update(r => r.Id == currentUserId, r => new Realtor { Balance = (realtorBalance - service.Price.Value) });

            try
            {
                if (!service.Price.HasValue)
                    _mailService.Value.SendServiceOrderNotices(newOrder.ServiceOrder, service.ModeratorsEmails.ToArray());
            }
            catch
            {
            }
        }

        public void ProcessSuspendedOrders(string realtorId, decimal orderSumAmount)
        {
            var orders = _serviceOrdersRepository.Value
                .Where(o => o.Order.RealtorId == realtorId && o.Status == ServiceOrderStatus.Suspended)
                .OrderBy(o => o.CreateDate)
                .ToList();

            if (!orders.Any())
                return;

            foreach (var order in orders)
            {
                var price = order.Service.Price;
                if (price.HasValue && orderSumAmount >= price.Value)
                {
                    order.Status = ServiceOrderStatus.New;
                    order.UpdateDate = DateTime.Now;

                    _serviceOrdersRepository.Value.Update(order);

                    orderSumAmount -= price.Value;

                    
                    try
                    {
                        if (order.ServiceId == 20)
                        {
                            var userData = order.UserData;
                            ServiceOrderUserData deserializedProduct = JsonConvert.DeserializeObject<ServiceOrderUserData>(userData);
                            System.Diagnostics.Process process = new System.Diagnostics.Process();
                            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            startInfo.FileName = "cmd.exe";
                            startInfo.Arguments = "/user:Administrator \"cmd /K C:\\inetpub\\egrp\\egrp.py " + deserializedProduct.CadNum + " " + order.Email;
                            process.StartInfo = startInfo;
                            process.Start();
                        }
                    }
                    catch
                    {
                    }

                    try
                    {
                        var moderatorsEmails = order.Service.ModeratorsService.Select(ms => ms.Moderator.Email).ToArray();

                        _mailService.Value.SendServiceOrderPaidUpNotices(order, moderatorsEmails);
                    }
                    catch
                    {
                    }
                }
            }

            _serviceOrdersRepository.Value.SaveChanges();
        }
    }
}
