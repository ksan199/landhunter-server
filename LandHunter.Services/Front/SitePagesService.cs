﻿using System;
using System.Collections.Generic;
using System.Linq;
using LandHunter.ApiModels;
using LandHunter.Models;
using SharpEngine.Repository;

namespace LandHunter.Services
{
    public class SitePagesService : ISitePagesService
    {
        private readonly Lazy<IRepository<SitePage, int>> _pagesRepository;

        public SitePagesService(Lazy<IRepository<SitePage, int>> pagesRepository)
        {
            _pagesRepository = pagesRepository;
        }

        public SitePageApiModel Get(string pageUrl)
        {
            return _pagesRepository.Value
                .Where(p => p.Url.ToLower() == pageUrl.ToLower())
                .Select(p => new SitePageApiModel
                    {
                        Title = p.Title,
                        Content = p.Content
                    })
                .FirstOrDefault();
        }

        public IReadOnlyCollection<SitePageApiModel> GetList()
        {
            return _pagesRepository.Value
                .Where(p => p.ShowInMenu)
                .OrderBy(p => p.SortOrder)
                .Select(p => new SitePageApiModel
                    {
                        Title = p.Title,
                        Url = p.Url
                    })
                .ToList();
        }
    }
}