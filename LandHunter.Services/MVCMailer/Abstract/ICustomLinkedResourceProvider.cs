﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace LandHunter.Services.MVCMailer
{
    public interface ICustomLinkedResourceProvider
    {
        List<LinkedResource> GetAll(Dictionary<string, MemoryStream> resources);
        LinkedResource Get(string contentId, MemoryStream stream);
    }
}