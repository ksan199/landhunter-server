﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using Mvc.Mailer;

namespace LandHunter.Services.MVCMailer
{
    public class CustomLinkedResourceProvider : LinkedResourceProvider, ICustomLinkedResourceProvider
    {
        public virtual List<LinkedResource> GetAll(Dictionary<string, MemoryStream> resources)
        {
            var linkedResources = new List<LinkedResource>();

            foreach (var resource in resources)
            {
                linkedResources.Add(Get(resource.Key, resource.Value));
            }

            return linkedResources;
        }

        public virtual LinkedResource Get(string contentId, MemoryStream stream)
        {
            var resource = new LinkedResource(stream) { ContentId = contentId };

            return resource;
        }
    }
}