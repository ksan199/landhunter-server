﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using Mvc.Mailer;

namespace LandHunter.Services.MVCMailer
{
    public class CustomMailerBase : MailerBase
    {
        private ICustomLinkedResourceProvider _customLinkedResourceProvider = (ICustomLinkedResourceProvider)new CustomLinkedResourceProvider();

        public virtual ICustomLinkedResourceProvider CustomLinkedResourceProvider
        {
            get
            {
                return this._customLinkedResourceProvider;
            }
            set
            {
                this._customLinkedResourceProvider = value;
            }
        }

        public virtual void PopulateBody(MailMessage mailMessage, string viewName, Dictionary<string, MemoryStream> linkedResources)
        {
            PopulateBody(mailMessage, viewName, null, linkedResources);
        }

        public virtual void PopulateBody(MailMessage mailMessage, string viewName, string masterName = null, Dictionary<string, MemoryStream> linkedResources = null)
        {
            if (mailMessage == null)
                throw new ArgumentNullException("mailMessage", "mailMessage cannot be null");

            masterName = masterName ?? MasterName;

            var linkedResourcesPresent = linkedResources != null && linkedResources.Count > 0;
            var textExists = TextViewExists(viewName, masterName);

            if (textExists)
                PopulateTextBody(mailMessage, viewName, masterName);

            if (HtmlViewExists(viewName, masterName))
            {
                if (textExists || linkedResourcesPresent)
                {
                    PopulateHtmlPart(mailMessage, viewName, masterName, linkedResources);
                }
                else
                {
                    PopulateHtmlBody(mailMessage, viewName, masterName);
                }
            }
        }

        public virtual AlternateView PopulateHtmlPart(MailMessage mailMessage, string viewName, string masterName, Dictionary<string, MemoryStream> linkedResources)
        {
            var htmlPart = PopulatePart(mailMessage, viewName, "text/html", masterName);

            if (htmlPart != null)
                PopulateLinkedResources(htmlPart, linkedResources);

            return htmlPart;
        }

        public virtual List<LinkedResource> PopulateLinkedResources(AlternateView mailPart, Dictionary<string, MemoryStream> resources)
        {
            if (resources == null || resources.Count == 0)
                return new List<LinkedResource>();

            var linkedResources = CustomLinkedResourceProvider.GetAll(resources);

            linkedResources.ForEach(resource => mailPart.LinkedResources.Add(resource));

            return linkedResources;
        }
    }
}