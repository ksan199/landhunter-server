﻿using System;
using Autofac;
using LandHunter.Common.Helpers;
using LandHunter.DataAccess.Context;
using LandHunter.DataAccess.Repository;
using LandHunter.Services;
using SharpEngine.Repository;

namespace LandHunter.TempFilesCleaner
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            RegisterDependencies();

            try
            {
                var webSitePath = AppConfigHelper.GetSetting<string>("WebSitePath");
                var daysBeforeNow = AppConfigHelper.GetSetting<int>("DaysBeforeNow", 2);

                DeleteAllTempFiles(webSitePath, daysBeforeNow);

                Console.WriteLine("Temp files was successfully deleted.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while deleting temp files: " + e.Message + " - " + e.StackTrace);
            }
#if DEBUG
            Console.WriteLine("Press any key to close console.");
            Console.ReadKey();
#endif
        }

        private static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<LandHunterContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(LandHunterRepository<,>)).As(typeof(IRepository<,>)).InstancePerLifetimeScope();
            builder.RegisterType<FilesService>().As<IFilesService>().InstancePerLifetimeScope();

            Container = builder.Build();
        }

        private static void DeleteAllTempFiles(string webSitePath, int daysBeforeNow)
        {
            using (var scope = Container.BeginLifetimeScope())
            {
                var filesService = scope.Resolve<IFilesService>();

                filesService.DeleteAllTempFiles(webSitePath, daysBeforeNow);
            }
        }
    }
}