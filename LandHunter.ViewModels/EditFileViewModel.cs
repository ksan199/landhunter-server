﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ViewModels
{
    public class EditFileViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Название")]
        public string FileName { get; set; }

        [UIHint("TextArea")]
        [StringLength(1000, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        public override string ToString()
        {
            return "Файл";
        }
    }
}