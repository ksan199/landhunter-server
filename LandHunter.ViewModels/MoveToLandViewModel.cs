﻿namespace LandHunter.ViewModels
{
    public class MoveToLandViewModel
    {
        public MoveToLandViewModel()
        {
            ImagesIdsToMove = new int[0];
        }

        public int AdvertisementId { get; set; }
        public bool MoveAddress { get; set; }
        public bool MoveDescription { get; set; }
        public bool MoveUtilization { get; set; }
        public bool MoveCategoryCode { get; set; }
        public bool MoveArea { get; set; }
        public bool MoveCommunications { get; set; }
        public int[] ImagesIdsToMove { get; set; }
    }
}