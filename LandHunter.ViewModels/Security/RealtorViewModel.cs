﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Attributes;
using SharpEngine.Web.Mvc.Resources;
using SharpEngine.Web.Mvc.Validation;

namespace LandHunter.ViewModels
{
    public class RealtorViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public string Id { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [AdditionalMetadata("CssClass", "phone-input")]
        [Display(Name = "Телефон", Order = 0)]
        public string PhoneNumber { get; set; }

        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Фамилия", Order = 3)]
        public string Surname { get; set; }

        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Имя", Order = 4)]
        public string Name { get; set; }

        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Отчество", Order = 5)]
        public string Patronymic { get; set; }

        [EmailAddress(ErrorMessage = null, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "EmailIncorrect")]
        [StringLength(100, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "E-mail", Order = 6)]
        public string Email { get; set; }

        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Адрес", Order = 7)]
        public string Address { get; set; }

        [RegularExpression(ValidationRegex.Url, ErrorMessage = "Url адрес введен некорректно.")]
        [StringLength(255, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Сайт", Order = 8)]
        public string Website { get; set; }

        [DropDownList("AgenciesDictionary")]
        [Display(Name = "Агенство", Order = 9)]
        public int? AgencyId { get; set; }

        [ScaffoldColumn(false)]
        public IEnumerable<SelectListItem> AgenciesDictionary { get; set; }

        [HttpPostedFileExtensions("jpeg,jpg,png", ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "AllowedFileExtensions")]
        [HttpPostedFileSize(15360, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "UploadedFilesSizeIncorrect")]
        [UIHint("File")]
        [Display(Name = "Фотография", Order = 10, Description = "Максимальный объем файла - 15 мб.")]
        public HttpPostedFileBase HttpPostedFile { get; set; }

        public override string ToString()
        {
            return "Риелтор";
        }
    }

    public class CreateRealtorViewModel : RealtorViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "PropertyValueRequired")]
        [StringLength(100, ErrorMessage = "Длина поля не должна быть минимум {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль", Order = 1)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Пароль еще раз", Order = 2)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class EditRealtorViewModel : RealtorViewModel
    {
        [Display(Name = "Заблокировать пользователя", Order = 11)]
        public bool LockoutEnabled { get; set; }

        [UIHint("Text")]
        [Display(Name = "Баланс", Order = 12)]
        public decimal Balance { get; set; }

        [UIHint("Text")]
        [Display(Name = "Кол-во просмотров", Order = 13)]
        public int ViewsCount { get; set; }

        [UIHint("Text")]
        [Display(Name = "Рейтинг", Order = 14)]
        public int Rating { get; set; }

        [Display(Name = "Бонус рейтинга", Description = "Учитывается при каждом расчете рейтинга. Допускаются отрицательные значения.", Order = 15)]
        public int RatingBonus { get; set; }

        [UIHint("BlockHeader")]
        [Display(Name = "", Order = 16)]
        public string BlockHeader1 { get { return "Статистика посещений"; } }

        [UIHint("Text")]
        [Display(Name = "Общее кол-во дней посещений", Order = 17)]
        public int TotalVisitsCount { get; set; }

        [UIHint("Text")]
        [Display(Name = "Кол-во дней посещений подряд", Description = "Параметр не актуален, если дата последнего посещения старше вчерашнего дня.", Order = 18)]
        public int ContinuedVisitsCount { get; set; }

        [UIHint("Text")]
        [Display(Name = "Дата последнего посещения", Order = 19)]
        public DateTime LastVisitDate { get; set; }

        [UIHint("BlockHeader")]
        [Display(Name = "", Order = 20)]
        public string BlockHeader2 { get { return "Статистика предложений"; } }

        [UIHint("Text")]
        [Display(Name = "Кол-во всех предложений", Order = 21)]
        public int AllAdvertisementsCount { get; set; }

        [UIHint("Text")]
        [Display(Name = "Кол-во активных предложений", Order = 22)]
        public int ActiveAdvertisementsCount { get; set; }

        [UIHint("Text")]
        [Display(Name = "Кол-во проданных предложений", Order = 23)]
        public int SoldOutAdvertisementsCount { get; set; }

        [UIHint("Text")]
        [Display(Name = "Кол-во удаленных предложений", Order = 24)]
        public int DeletedAdvertisementsCount { get; set; }
    }
}
