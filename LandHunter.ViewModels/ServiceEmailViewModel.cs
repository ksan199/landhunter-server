﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Resources;

namespace LandHunter.ViewModels
{
    public class ServiceEmailViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int ServiceOrderId { get; set; }

        [UIHint("Text")]
        [Display(Name = "Получатель")]
        public string Recipient { get; set; }

        [StringLength(500, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "StringLengthIncorrect")]
        [Display(Name = "Тема")]
        public string Title { get; set; }

        [UIHint("TextArea")]
        [Display(Name = "Текст")]
        public string Text { get; set; }

        [HttpPostedFileSize(15360, ErrorMessageResourceType = typeof(ValidationMessages), ErrorMessageResourceName = "UploadedFilesSizeIncorrect")]
        [UIHint("Files")]
        [Display(Name = "Вложение", Description = "Можно выбрать несколько файлов. Максимальный объем - 15 мб.")]
        public IEnumerable<HttpPostedFileBase> HttpPostedFile { get; set; }

        public override string ToString()
        {
            return "E-mail";
        }
    }
}
