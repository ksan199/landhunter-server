﻿using System.Web.Optimization;

namespace LandHunter
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Scripts

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*",
                "~/Scripts/globalize.js",
                "~/Scripts/globalize.culture.ru-RU.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/moment-with-locales.js",
                "~/Scripts/bootstrap-multiselect.js",
                "~/Scripts/bootstrap-datetimepicker.js",
                "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/noty").Include(
                "~/Scripts/noty/jquery.noty.js",
                "~/Scripts/noty/themes/default.js",
                "~/Scripts/noty/layouts/top.js",
                "~/Scripts/noty/layouts/center.js"));

            bundles.Add(new ScriptBundle("~/bundles/ckeditor").Include(
                "~/Scripts/ckeditor/ckeditor.js",
                "~/Scripts/ckeditor/adapters/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                "~/Scripts/jquery.maskedinput.js",
                "~/Scripts/admin/jquery.blockUI.js",
                "~/Scripts/admin/jquery.form.js",
                "~/Scripts/admin/jquery.tablednd.js",
                "~/Scripts/admin/sharpengine.js",
                "~/Scripts/admin/sharpengine.grid.js"));

            bundles.Add(new ScriptBundle("~/bundles/lands").Include(
                "~/Scripts/admin/Lands/lands.js"));

            bundles.Add(new ScriptBundle("~/bundles/advertisements").Include(
                "~/Scripts/admin/Advertisements/advertisements.js"));

            bundles.Add(new ScriptBundle("~/bundles/exchange-rates").Include(
                "~/Scripts/admin/ExchangeRates/index.js"));

            bundles.Add(new ScriptBundle("~/bundles/uploaded-files").Include(
                "~/Scripts/admin/uploaded-files.js"));

            bundles.Add(new ScriptBundle("~/bundles/service-orders").Include(
                "~/Scripts/admin/ServiceOrders/service-orders.js"));

            #endregion

            #region Styles

            bundles.Add(new StyleBundle("~/Styles/site").Include(
                "~/Styles/bootstrap.css",
                "~/Styles/site.css"));

            bundles.Add(new StyleBundle("~/Styles/admin").Include(
                "~/Styles/bootstrap.css",
                "~/Styles/bootstrap-multiselect.css",
                "~/Styles/bootstrap-datetimepicker.css",
                "~/Styles/admin.css",
                "~/Styles/validation.css"));

            bundles.Add(new StyleBundle("~/Styles/signin").Include(
                "~/Styles/bootstrap.css",
                "~/Styles/signin.css",
                "~/Styles/validation.css"));

            #endregion

            BundleTable.EnableOptimizations = false;
        }
    }
}