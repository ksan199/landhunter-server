﻿using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using WebApiContrib.Formatting.Jsonp;

namespace LandHunter
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

            config.AddJsonpFormatter();

            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            // Web API routes

            config.MapHttpAttributeRoutes();

            // Для контроллера Advertisements создан свой роут, чтобы не было проблем с AdBlock

            config.Routes.MapHttpRoute(
                "LandvertisementsApi",
                "api/Landvertisements/{action}/{id}",
                new { controller = "Advertisements", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional }
            );
        }
    }
}
