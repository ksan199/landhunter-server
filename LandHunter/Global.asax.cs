﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LandHunter.Infrastructure;
using SharpEngine.Web.Mvc;
using SharpEngine.Web.Mvc.Controls;

namespace LandHunter
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            BootStrapper.ConfigureApplication();

            ModelBinders.Binders.DefaultBinder = new SmartBinder(new[] { new GridOptionsModelBinder() });

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
