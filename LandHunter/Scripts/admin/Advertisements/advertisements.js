﻿$(function () {
    $('body').on('click', '.move-to-land', function () {
        var saveToLandBtn = $('#MoveToLand');

        $(this).toggleClass('btn-default btn-info');
        $(this).find('.glyphicon').toggleClass('glyphicon-unchecked glyphicon-check');

        if (!$(this).hasClass('active')) {
            saveToLandBtn.removeClass('disabled').css('pointer-events', '');
        } else if (!$('.move-to-land').not(this).is('.active')) {
            saveToLandBtn.addClass('disabled').css('pointer-events', 'all');
        }
    });

    $('body').on('click', '#MoveToLand', function (event) {
        var self = $(this),
            form = self.closest('form'),
            imagesIds = $('#AdvertisementFilesGrid').find('.move-to-land.active').closest('tr')
                .map(function () { return $(this).attr('data-item-id'); });

        event.preventDefault();

        if (self.hasClass('disabled'))
            return false;

        $.ajax({
            type: 'POST',
            url: self.attr('href'),
            data: {
                AdvertisementId: form.find('#Id').val(),
                MoveAddress: moveToLand(form.find('#Address')),
                MoveDescription: moveToLand(form.find('#Description')),
                MoveUtilization: moveToLand(form.find('#Utilization')),
                MoveCategoryCode: moveToLand(form.find('#CategoryCode')),
                MoveArea: moveToLand(form.find('#Area')),
                MoveCommunications: moveToLand(form.find('#GasPipeline')),
                ImagesIdsToMove: $.makeArray(imagesIds),
            },
            traditional: true,
            dataType: 'json',
            success: function (json) {
                if (json.Successful) {
                    showNoty('Данные перенесены в участок.', 'success');

                    form.find('.move-to-land.active').click();
                    reloadPanel('#AdvertisementFilesGrid');
                } else {
                    showGlobalErrors(json);
                }
            }
        });

        return false;
    });

    function moveToLand(element) {
        return element.closest('.form-group').find('.move-to-land').hasClass('active');
    }
})