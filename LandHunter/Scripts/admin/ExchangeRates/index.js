﻿$(function() {
    $('body').on('click', '#ExchangeRatesRefreshDailyRates', function (event) {
        var self = $(this),
            targetUpdate = $(this).attr('data-target-update'),
            antiForgeryToken = $(this).siblings('input[name=__RequestVerificationToken]').val();

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: self.attr('href'),
            data: { __RequestVerificationToken: antiForgeryToken },
            traditional: true,
            dataType: 'json',
            success: function (json) {
                if (json.Successful)
                    reloadPanel(targetUpdate);
                else
                    showGlobalErrors(json);
            }
        });

        return false;
    });
})