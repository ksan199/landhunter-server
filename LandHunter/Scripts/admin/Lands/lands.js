﻿$(function () {
    var addressChanged = $('#AddressChanged').prop('checked'),
        utilizationChanged = $('#UtilizationChanged').prop('checked'),
        categoryCodeChanged = $('#CategoryCodeChanged').prop('checked'),
        areaChanged = $('#AreaChanged').prop('checked');

    $('body').on('change', '.prop-changed-flag', function () {
        var updateBtn = $('#UpdateCadastreInfo'),
            disableUpdateBtn = $('#AddressChanged').prop('checked') !== addressChanged || $('#UtilizationChanged').prop('checked') !== utilizationChanged
                || $('#CategoryCodeChanged').prop('checked') !== categoryCodeChanged || $('#AreaChanged').prop('checked') !== areaChanged;

        updateBtn.toggleClass('disabled', disableUpdateBtn);

        if (disableUpdateBtn)
            updateBtn.attr('title', 'Сохраните участок перед обновлением.').css('pointer-events', 'all');
        else
            updateBtn.removeAttr('title').css('pointer-events', '');
    });

    $('body').on('click', '#UpdateCadastreInfo', function(event) {
        var self = $(this),
            cadNum = $(this).closest('form').find('#CadNum').val(),
            antiForgeryToken = $(this).siblings('input[name=__RequestVerificationToken]').val();

        event.preventDefault();

        if (self.hasClass('disabled'))
            return false;

        $.ajax({
            type: 'POST',
            url: self.attr('href'),
            data: { cadNums: [cadNum], __RequestVerificationToken: antiForgeryToken },
            traditional: true,
            dataType: 'json',
            success: function (json) {
                if (json.Successful) {
                    if (json.Data === 1) {
                        showNoty('Участок обновлен.', 'success');

                        setTimeout('location.reload()', 500);
                    } else {
                        showNoty('Не удалось обновить участок.', 'error');
                    }
                } else {
                    showGlobalErrors(json);
                }
            }
        });

        return false;
    });

    $('body').on('click', '#LandsUpdateCadastreInfo', function (event) {
        var self = $(this),
            targetUpdate = $(this).attr('data-target-update'),
            rowCheckboxes = $(targetUpdate).find('.row-checkbox:checked'),
            antiForgeryToken = $(this).siblings('input[name=__RequestVerificationToken]').val();

        if (rowCheckboxes.length === 0) {
            showNoty('Не выбрано ни одного участка.', 'alert');
            return false;
        }

        noty({
            layout: 'center',
            text: 'Вы уверены, что хотите обновить кадастровую информацию по выбранным участкам?',
            type: 'warning',
            modal: true,
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
                        $noty.close();

                        var selectedValues = rowCheckboxes.map(function () { return $(this).val(); });

                        $.ajax({
                            type: 'POST',
                            url: self.attr('href'),
                            data: { cadNums: $.makeArray(selectedValues), __RequestVerificationToken: antiForgeryToken },
                            traditional: true,
                            dataType: 'json',
                            success: function (json) {
                                if (json.Successful) {
                                    reloadPanel(targetUpdate);

                                    showNoty('Обновлено участков: ' + json.Data, 'success');
                                } else {
                                    showGlobalErrors(json);
                                }
                            }
                        });
                    }
                }, {
                    addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) {
                        $noty.close();
                    }
                }
            ]
        });

        return false;
    });
})