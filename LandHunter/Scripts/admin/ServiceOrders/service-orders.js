﻿$(function() {
    $('body').on('click', '.take-btn', function (event) {
        var self = $(this),
            targetUpdate = $(this).closest('.updatepanel.mvcgrid-panel');

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: self.attr('href'),
            traditional: true,
            dataType: 'json',
            success: function (json) {
                if (json.Successful) {
                    if (targetUpdate.length > 0) {
                        reloadPanel(targetUpdate);
                    } else {
                        window.location.reload();
                    }
                } else {
                    showGlobalErrors(json);
                }
            }
        });

        return false;
    });
})